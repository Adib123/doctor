-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Jeu 22 Juin 2017 à 10:39
-- Version du serveur :  5.7.9
-- Version de PHP :  5.6.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `doctorbusinessology`
--

-- --------------------------------------------------------

--
-- Structure de la table `appointment`
--

DROP TABLE IF EXISTS `appointment`;
CREATE TABLE IF NOT EXISTS `appointment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `isActiveAppointment` tinyint(1) NOT NULL,
  `user_id` int(11) NOT NULL,
  `doctor_id` int(11) NOT NULL,
  `createdAtAppointment` datetime NOT NULL,
  `appointment` datetime NOT NULL,
  `eval` tinyint(1) NOT NULL,
  `reason` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `IDX_FE38F844A76ED395` (`user_id`),
  KEY `IDX_FE38F84487F4FB17` (`doctor_id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `appointment`
--

INSERT INTO `appointment` (`id`, `status`, `isActiveAppointment`, `user_id`, `doctor_id`, `createdAtAppointment`, `appointment`, `eval`, `reason`) VALUES
(1, 'مرفوض', 1, 3, 1, '2017-04-17 08:09:55', '2017-04-17 08:09:55', 1, 'cdjsdgf sdjcgusdc vkcvgdb'),
(2, 'مرفوض', 1, 4, 2, '2017-04-17 08:20:34', '2017-04-17 20:13:00', 0, ''),
(3, 'معلق', 0, 1, 5, '2017-04-17 09:52:47', '2012-01-01 06:01:00', 0, ''),
(4, 'مؤكد', 1, 5, 2, '2017-04-17 09:53:44', '2012-01-01 15:02:00', 0, ''),
(5, 'معلق', 1, 5, 2, '2017-04-21 16:22:31', '2017-04-24 00:00:00', 0, ''),
(6, 'مرفوض', 1, 8, 1, '2017-04-21 16:23:17', '2017-12-04 00:00:00', 0, ''),
(7, 'منته', 1, 1, 5, '2017-04-21 16:23:46', '2012-01-01 00:00:00', 0, ''),
(8, 'مؤكد', 1, 9, 7, '2017-04-21 16:24:27', '2017-04-27 00:00:00', 0, ''),
(9, 'معلق', 1, 3, 1, '2017-06-13 00:28:17', '2017-07-10 08:00:00', 0, NULL),
(10, 'معلق', 1, 3, 1, '2017-06-13 01:01:11', '2017-07-02 09:00:00', 0, NULL),
(11, 'معلق', 1, 3, 1, '2017-06-13 01:02:46', '2017-07-02 13:00:00', 0, NULL),
(12, 'مرفوض', 1, 10, 9, '2017-06-13 07:24:30', '2017-07-10 17:00:00', 0, '\r\n                        ùdplgmrgrmkgr'),
(13, 'معلق', 1, 13, 13, '2017-06-17 10:29:45', '2017-06-17 09:00:00', 0, NULL),
(14, 'معلق', 1, 3, 13, '2017-06-17 10:30:18', '2017-06-17 10:00:00', 0, NULL),
(15, 'معلق', 1, 3, 9, '2017-06-17 10:32:18', '2017-06-17 11:00:00', 0, NULL),
(16, 'معلق', 1, 13, 9, '2017-06-17 10:32:49', '2017-06-17 12:00:00', 0, NULL),
(17, 'معلق', 1, 3, 13, '2017-06-17 03:18:39', '2017-06-17 16:00:00', 0, NULL),
(18, 'معلق', 1, 13, 13, '2017-06-21 14:05:49', '2017-07-01 10:30:00', 0, NULL),
(19, 'معلق', 1, 13, 13, '2017-06-21 19:35:12', '2017-07-03 10:30:00', 0, NULL),
(20, 'معلق', 1, 13, 13, '2017-06-21 19:36:15', '2017-07-04 10:30:00', 0, NULL),
(21, 'معلق', 1, 13, 13, '2017-06-21 19:40:18', '2017-07-05 10:30:00', 0, NULL),
(22, 'معلق', 1, 13, 13, '2017-06-21 19:41:23', '2017-07-06 10:30:00', 0, NULL),
(23, 'مرفوض', 1, 13, 13, '2017-06-21 19:42:37', '2017-07-07 10:30:00', 0, 'cdjsdgf sdjcgusdc vkcvgdb');

-- --------------------------------------------------------

--
-- Structure de la table `code`
--

DROP TABLE IF EXISTS `code`;
CREATE TABLE IF NOT EXISTS `code` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `number` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `code`
--

INSERT INTO `code` (`id`, `number`) VALUES
(1, 'e10adc3949ba59abbe56e057f20f883e');

-- --------------------------------------------------------

--
-- Structure de la table `doctor`
--

DROP TABLE IF EXISTS `doctor`;
CREATE TABLE IF NOT EXISTS `doctor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nameDoctor` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nationality` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phoneNumberDoctor` bigint(20) DEFAULT NULL,
  `countryDoctor` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `townDoctor` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cityDoctor` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `workplace` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `workplaceName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `emailDoctor` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `passwordDoctor` longtext COLLATE utf8_unicode_ci,
  `photoDoctor` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `createdAtDoctor` datetime NOT NULL,
  `longitude` longtext COLLATE utf8_unicode_ci,
  `isActiveDoctor` tinyint(1) NOT NULL,
  `lastLoginDoctor` datetime DEFAULT NULL,
  `isCompleted` tinyint(1) NOT NULL,
  `speciality_id` int(11) DEFAULT NULL,
  `startTimeWork` time DEFAULT NULL,
  `endTimeWork` time DEFAULT NULL,
  `lattitude` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_1FC0F36AEFDC9C03` (`phoneNumberDoctor`),
  UNIQUE KEY `UNIQ_1FC0F36AF195B4F` (`emailDoctor`),
  KEY `IDX_1FC0F36A3B5A08D7` (`speciality_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `doctor`
--

INSERT INTO `doctor` (`id`, `nameDoctor`, `nationality`, `phoneNumberDoctor`, `countryDoctor`, `townDoctor`, `cityDoctor`, `workplace`, `workplaceName`, `emailDoctor`, `passwordDoctor`, `photoDoctor`, `createdAtDoctor`, `longitude`, `isActiveDoctor`, `lastLoginDoctor`, `isCompleted`, `speciality_id`, `startTimeWork`, `endTimeWork`, `lattitude`) VALUES
(1, 'عماد سلطاني', 'تونسية', 70654321, 'تونس', 'جندوبة', 'السواني', 'شارع الطيب المهيري', 'BUSINESSOLOGY PLUS', 'akrem.boussaha@gmail.com', '123456789', '4629d7cfea51478b976a7a713095e774.jpeg', '2017-04-14 14:54:32', NULL, 1, NULL, 0, NULL, '08:00:00', '12:00:00', NULL),
(2, 'محمد', 'تونسية', 32659812, 'المملكة العربية السعودية', 'مكة المكرمة', 'النور', 'مكة المكرمة', 'مستشفى النور', 'med@gmail.com', '123456789', 'f0a254ed923ccd732720db90f6a385e8.jpeg', '2017-04-14 15:45:09', NULL, 1, NULL, 0, NULL, NULL, NULL, NULL),
(5, 'Mongi', 'Algérienne', 51257896, 'الجزائر', 'وهران', 'النور', 'مكة المكرمة', 'مستشفى النور', 'woowerktgl.tt@.com', '1234567889', '872a974808d895845b0665b3030d839a.jpeg', '2017-04-14 15:48:35', NULL, 1, NULL, 0, NULL, NULL, NULL, NULL),
(6, 'Test', 'testinien', 58796542, 'ldldldl', 'ftghfthftg', 'dgdrgdtrghtr', 'grergerger', 'ergergrg', 'kfgkf@gthtrl.com', '123456789', '94c380d00d4c5889ad06a7cd040a3939.jpeg', '2017-04-14 15:51:03', NULL, 0, NULL, 0, NULL, NULL, NULL, NULL),
(7, 'صالح', 'جزائرية', 21365475, 'الجزائر', 'وهران', 'الفردوس', 'جندوبة', 'المستشفى الجهوي بجندوبة', 'saleh@gmail.com', '123456789', 'ecf3b7e46e52ec622f7d53f1a42e5c04.jpeg', '2017-04-21 14:00:34', NULL, 1, NULL, 0, 3, NULL, NULL, NULL),
(8, 'عبد العزيز', 'تونسية', 20081529, 'السعودية', 'مكة المكرمة', 'حي الفردوس', 'مكة الكرمة', 'مستشفى النور', 'admin@admin.com', '123456789', '36061cdcc541750c50c6bcb0cf219eba.jpeg', '2017-05-07 09:54:33', NULL, 1, NULL, 0, 4, NULL, NULL, NULL),
(9, 'أيمن البهلي', 'تونسية', 200541321, 'المملكة العربية السعودية', 'مكة المكرمة', 'حي السلامة', 'مكة المكرمة', 'مستشفى النور', 'aymen.bohli@gmail.com', '123456789', 'f72db0a89b2579cc9a238a28c0a2fd32.jpg', '2017-06-13 07:19:35', NULL, 1, NULL, 1, 2, '08:00:00', '17:30:00', NULL),
(10, NULL, NULL, 78325452, NULL, NULL, NULL, NULL, NULL, NULL, '123', NULL, '2017-06-15 07:31:28', NULL, 1, NULL, 0, NULL, NULL, NULL, NULL),
(11, NULL, NULL, 58147963, NULL, NULL, NULL, NULL, NULL, NULL, '123', 'avatar.jpg', '2017-06-15 07:34:58', NULL, 1, NULL, 0, NULL, NULL, NULL, NULL),
(12, NULL, NULL, NULL, 'المملكة العربية السعودية', NULL, NULL, NULL, NULL, NULL, NULL, 'avatar.jpg', '2017-06-15 08:33:51', NULL, 1, NULL, 0, 1, NULL, NULL, NULL),
(13, 'Ali', 'Tunisienne', 99654789, 'المملكة العربية السعودية', 'مكة المكرمة', 'حي الفردوس', 'مكة الكرمة', 'مستشفى النور', 'admin@aaadddmin.com', '123', '9b30f7c709360e651bd107de69c675a6.jpg', '2017-06-17 10:26:10', NULL, 1, NULL, 1, 5, '08:30:00', '16:00:00', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `evaluation`
--

DROP TABLE IF EXISTS `evaluation`;
CREATE TABLE IF NOT EXISTS `evaluation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `evaluation` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `feedback` longtext COLLATE utf8_unicode_ci NOT NULL,
  `createdAtEvaluation` datetime NOT NULL,
  `isActiveEvaluation` tinyint(1) NOT NULL,
  `doctor_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `appointment_id` int(11) DEFAULT NULL,
  `statusEvaluation` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_1323A575E5B533F9` (`appointment_id`),
  KEY `IDX_1323A57587F4FB17` (`doctor_id`),
  KEY `IDX_1323A575A76ED395` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `evaluation`
--

INSERT INTO `evaluation` (`id`, `evaluation`, `feedback`, `createdAtEvaluation`, `isActiveEvaluation`, `doctor_id`, `user_id`, `appointment_id`, `statusEvaluation`) VALUES
(6, 'ممتاز', 'hehe hdhhhehhhh', '2017-06-13 11:24:14', 1, 1, 3, 1, 0);

-- --------------------------------------------------------

--
-- Structure de la table `fos_user`
--

DROP TABLE IF EXISTS `fos_user`;
CREATE TABLE IF NOT EXISTS `fos_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `username_canonical` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `email_canonical` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `confirmation_token` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_957A647992FC23A8` (`username_canonical`),
  UNIQUE KEY `UNIQ_957A6479A0D96FBF` (`email_canonical`),
  UNIQUE KEY `UNIQ_957A6479C05FB297` (`confirmation_token`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `fos_user`
--

INSERT INTO `fos_user` (`id`, `username`, `username_canonical`, `email`, `email_canonical`, `enabled`, `salt`, `password`, `last_login`, `confirmation_token`, `password_requested_at`, `roles`) VALUES
(1, 'Admin', 'admin', 'akrem.boussaha@gmail.com', 'akrem.boussaha@gmail.com', 1, NULL, '$2y$13$l4tg6CtSiW8VeViv4MGe3O7HxtfUPKC06ah8N0140eCxh.iGNPUK2', '2017-06-21 12:28:32', NULL, NULL, 'a:1:{i:0;s:10:"ROLE_ADMIN";}');

-- --------------------------------------------------------

--
-- Structure de la table `notification`
--

DROP TABLE IF EXISTS `notification`;
CREATE TABLE IF NOT EXISTS `notification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `appointment_id` int(11) DEFAULT NULL,
  `message` longtext COLLATE utf8_unicode_ci,
  `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `seen` tinyint(1) DEFAULT NULL,
  `isDoctor` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_BF5476CAE5B533F9` (`appointment_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `notification`
--

INSERT INTO `notification` (`id`, `appointment_id`, `message`, `type`, `date`, `seen`, `isDoctor`) VALUES
(1, 23, 'قام المستخدمfat7iبحجز موعد جديد', 'appointment', '2017-06-21 19:42:37', 0, 1),
(2, 23, 'تم رفض الموعد مع الطبيب', 'appointment', '2017-06-21 19:56:01', 0, 0),
(3, 23, 'تم رفض الموعد مع الطبيب', 'appointment', '2017-06-22 07:12:29', 0, 0),
(4, 22, 'sdlkfjsd usdhfusdfh ksdfh ksdhf', 'appt', '2017-06-22 07:00:00', 0, 1);

-- --------------------------------------------------------

--
-- Structure de la table `speciality`
--

DROP TABLE IF EXISTS `speciality`;
CREATE TABLE IF NOT EXISTS `speciality` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nameSpeciality` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `isActiveSpeciality` tinyint(1) NOT NULL,
  `createdAtSpeciality` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `speciality`
--

INSERT INTO `speciality` (`id`, `nameSpeciality`, `isActiveSpeciality`, `createdAtSpeciality`) VALUES
(1, 'العيون', 1, '2017-04-21 09:04:31'),
(2, 'القلب', 1, '2017-04-21 09:46:48'),
(3, 'الأسنان', 1, '2017-04-21 09:47:37'),
(4, 'الكرش', 0, '2017-04-21 10:02:57'),
(5, 'العظام', 1, '2017-05-07 16:32:41');

-- --------------------------------------------------------

--
-- Structure de la table `token_doctor`
--

DROP TABLE IF EXISTS `token_doctor`;
CREATE TABLE IF NOT EXISTS `token_doctor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `doctor_id` int(11) DEFAULT NULL,
  `tokenDoctor` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `IDX_1EB85C6887F4FB17` (`doctor_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `token_doctor`
--

INSERT INTO `token_doctor` (`id`, `doctor_id`, `tokenDoctor`) VALUES
(1, 13, 'dzes54ze68f46zef46e54f6z8ef49ze8f4'),
(2, 12, 'z35ef4z6ef46z+ef46ze4f6ze4f5ze65fez6');

-- --------------------------------------------------------

--
-- Structure de la table `token_user`
--

DROP TABLE IF EXISTS `token_user`;
CREATE TABLE IF NOT EXISTS `token_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `token` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `IDX_EF97E32BA76ED395` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `token_user`
--

INSERT INTO `token_user` (`id`, `user_id`, `token`) VALUES
(1, 13, 'd6f4e6f4e6f4e6f4e6f4e6r846erf46er4f'),
(2, 12, 'ef6e6f4z35f4ez6f4ze5f4z6e4fe6f4er6f4');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `phone_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `isActive` tinyint(1) NOT NULL,
  `code` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `verified` tinyint(1) NOT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `town` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `lastLogin` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `user`
--

INSERT INTO `user` (`id`, `phone_number`, `password`, `isActive`, `code`, `name`, `verified`, `country`, `town`, `city`, `photo`, `email`, `createdAt`, `lastLogin`) VALUES
(1, '55956566', '1234567889', 0, 9171, 'Akrem', 0, 'Tunisia', 'Jendouba', 'Ferdaous', '6448a630205ddec1f67d971b97ef92e6.png', 'akrem.boussaha@gmail.com', '2017-04-13 13:09:17', NULL),
(3, '26656697', '213546789', 1, 5992, 'أكرم بوساحة', 1, 'تونس', 'جندوبة', 'الفردوس', '098f9c10983e76585aadc370d9b855e1.png', 'akrem.boussaha@live.com', '2017-04-13 13:38:52', NULL),
(4, '65963214', '321456987', 1, 3687, 'أديب', 0, 'تونس', 'جندوبة', 'الفردوس', '4d68bf562048edf3e4f7ca7dc00185b1.jpeg', 'adib@gmail.com', '2017-04-13 13:43:16', NULL),
(5, '545625512', '1234569874455878', 1, 6060, 'أيمن', 0, 'تونس', 'بوسالم', 'الورد', '77885be86abe6dfe383bdaf0c5e92707.png', 'aymen@gmail.com', '2017-04-13 15:05:50', NULL),
(6, '111111', '123456', 0, 5909, 'adib', 0, 'tunis', 'جندوبة', 'بثقيضخعس', '65c67f49123f5a4236c812bd360fab2a.png', 'qkrem@gmail.com', '2017-04-13 15:12:22', NULL),
(7, '99888968', '123456', 1, 6256, 'Akrse', 0, 'saudi', 'jeddah', 'ennour', '3c724baf84b25fc5722a489af4ed0aac.jpeg', 'live@gmail.com', '2017-04-14 07:47:15', NULL),
(8, '251456415', '4568799', 1, 6948, 'عبد العزيز', 0, 'المملكة العربية السعودية', 'جدة', 'النور', '3be44ffcf8b89a08265f89bfbf0734a3.png', 'med@gmail.com', '2017-04-14 07:53:32', NULL),
(9, '55669988', '123456789', 0, 4391, 'محمود عبيدي', 0, 'دبي', 'دبي', 'النور', '6afc3a8e0f4b21337d52734f832b97e0.jpeg', 'ikram@gmail.com', '2017-04-14 10:48:06', NULL),
(10, '97654123', '123', 1, 6896, 'Akrem Boussaha', 0, 'Tunisia', 'Jendouba', 'Elferdaous', '86c2b946551e4ff42704a551d0722c64.jpeg', 'akkrreem@gmmaim.com', '2017-06-13 07:22:01', NULL),
(11, '98654021', '123', 1, 5539, 'ضصمصم ممبم', 0, 'ميمي', 'ميمي ينين', 'نيننتن يني', 'a33878104702b41f6a1cce14ec9afa26.jpeg', 'akaj@djdj.com', '2017-06-13 08:26:39', NULL),
(12, '53177831', '123', 1, 8975, 'كريم مازني', 0, 'تونس', 'جندوبة', 'السعايدية', 'avatar.jpg', 'akrem.boussaha@gmail.com', '2017-06-16 08:13:55', NULL),
(13, '55321456', '123', 1, 4455, 'fat7i', 1, 'المملكة العربية السعودية', 'مكة المكرمة', 'سلامة', '536500c8a0121d71815bac21a9eaa089.jpeg', 'kjdi@telecom.com', '2017-06-17 10:27:47', NULL);

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `appointment`
--
ALTER TABLE `appointment`
  ADD CONSTRAINT `FK_FE38F84487F4FB17` FOREIGN KEY (`doctor_id`) REFERENCES `doctor` (`id`),
  ADD CONSTRAINT `FK_FE38F844A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

--
-- Contraintes pour la table `doctor`
--
ALTER TABLE `doctor`
  ADD CONSTRAINT `FK_1FC0F36A3B5A08D7` FOREIGN KEY (`speciality_id`) REFERENCES `speciality` (`id`);

--
-- Contraintes pour la table `evaluation`
--
ALTER TABLE `evaluation`
  ADD CONSTRAINT `FK_1323A57587F4FB17` FOREIGN KEY (`doctor_id`) REFERENCES `doctor` (`id`),
  ADD CONSTRAINT `FK_1323A575A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `FK_1323A575E5B533F9` FOREIGN KEY (`appointment_id`) REFERENCES `appointment` (`id`);

--
-- Contraintes pour la table `notification`
--
ALTER TABLE `notification`
  ADD CONSTRAINT `FK_BF5476CAE5B533F9` FOREIGN KEY (`appointment_id`) REFERENCES `appointment` (`id`);

--
-- Contraintes pour la table `token_doctor`
--
ALTER TABLE `token_doctor`
  ADD CONSTRAINT `FK_1EB85C6887F4FB17` FOREIGN KEY (`doctor_id`) REFERENCES `doctor` (`id`);

--
-- Contraintes pour la table `token_user`
--
ALTER TABLE `token_user`
  ADD CONSTRAINT `FK_EF97E32BA76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
