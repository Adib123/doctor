<?php

namespace Proxies\__CG__\Doctors\AdminBundle\Entity;

/**
 * DO NOT EDIT THIS FILE - IT WAS CREATED BY DOCTRINE'S PROXY GENERATOR
 */
class Appointment extends \Doctors\AdminBundle\Entity\Appointment implements \Doctrine\ORM\Proxy\Proxy
{
    /**
     * @var \Closure the callback responsible for loading properties in the proxy object. This callback is called with
     *      three parameters, being respectively the proxy object to be initialized, the method that triggered the
     *      initialization process and an array of ordered parameters that were passed to that method.
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setInitializer
     */
    public $__initializer__;

    /**
     * @var \Closure the callback responsible of loading properties that need to be copied in the cloned object
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setCloner
     */
    public $__cloner__;

    /**
     * @var boolean flag indicating if this object was already initialized
     *
     * @see \Doctrine\Common\Persistence\Proxy::__isInitialized
     */
    public $__isInitialized__ = false;

    /**
     * @var array properties to be lazy loaded, with keys being the property
     *            names and values being their default values
     *
     * @see \Doctrine\Common\Persistence\Proxy::__getLazyProperties
     */
    public static $lazyPropertiesDefaults = [];



    /**
     * @param \Closure $initializer
     * @param \Closure $cloner
     */
    public function __construct($initializer = null, $cloner = null)
    {

        $this->__initializer__ = $initializer;
        $this->__cloner__      = $cloner;
    }







    /**
     * 
     * @return array
     */
    public function __sleep()
    {
        if ($this->__isInitialized__) {
            return ['__isInitialized__', '' . "\0" . 'Doctors\\AdminBundle\\Entity\\Appointment' . "\0" . 'id', '' . "\0" . 'Doctors\\AdminBundle\\Entity\\Appointment' . "\0" . 'appointment', '' . "\0" . 'Doctors\\AdminBundle\\Entity\\Appointment' . "\0" . 'status', '' . "\0" . 'Doctors\\AdminBundle\\Entity\\Appointment' . "\0" . 'createdAtAppointment', '' . "\0" . 'Doctors\\AdminBundle\\Entity\\Appointment' . "\0" . 'isActiveAppointment', '' . "\0" . 'Doctors\\AdminBundle\\Entity\\Appointment' . "\0" . 'eval', '' . "\0" . 'Doctors\\AdminBundle\\Entity\\Appointment' . "\0" . 'reason', '' . "\0" . 'Doctors\\AdminBundle\\Entity\\Appointment' . "\0" . 'user', '' . "\0" . 'Doctors\\AdminBundle\\Entity\\Appointment' . "\0" . 'doctor'];
        }

        return ['__isInitialized__', '' . "\0" . 'Doctors\\AdminBundle\\Entity\\Appointment' . "\0" . 'id', '' . "\0" . 'Doctors\\AdminBundle\\Entity\\Appointment' . "\0" . 'appointment', '' . "\0" . 'Doctors\\AdminBundle\\Entity\\Appointment' . "\0" . 'status', '' . "\0" . 'Doctors\\AdminBundle\\Entity\\Appointment' . "\0" . 'createdAtAppointment', '' . "\0" . 'Doctors\\AdminBundle\\Entity\\Appointment' . "\0" . 'isActiveAppointment', '' . "\0" . 'Doctors\\AdminBundle\\Entity\\Appointment' . "\0" . 'eval', '' . "\0" . 'Doctors\\AdminBundle\\Entity\\Appointment' . "\0" . 'reason', '' . "\0" . 'Doctors\\AdminBundle\\Entity\\Appointment' . "\0" . 'user', '' . "\0" . 'Doctors\\AdminBundle\\Entity\\Appointment' . "\0" . 'doctor'];
    }

    /**
     * 
     */
    public function __wakeup()
    {
        if ( ! $this->__isInitialized__) {
            $this->__initializer__ = function (Appointment $proxy) {
                $proxy->__setInitializer(null);
                $proxy->__setCloner(null);

                $existingProperties = get_object_vars($proxy);

                foreach ($proxy->__getLazyProperties() as $property => $defaultValue) {
                    if ( ! array_key_exists($property, $existingProperties)) {
                        $proxy->$property = $defaultValue;
                    }
                }
            };

        }
    }

    /**
     * 
     */
    public function __clone()
    {
        $this->__cloner__ && $this->__cloner__->__invoke($this, '__clone', []);
    }

    /**
     * Forces initialization of the proxy
     */
    public function __load()
    {
        $this->__initializer__ && $this->__initializer__->__invoke($this, '__load', []);
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __isInitialized()
    {
        return $this->__isInitialized__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitialized($initialized)
    {
        $this->__isInitialized__ = $initialized;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitializer(\Closure $initializer = null)
    {
        $this->__initializer__ = $initializer;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __getInitializer()
    {
        return $this->__initializer__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setCloner(\Closure $cloner = null)
    {
        $this->__cloner__ = $cloner;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific cloning logic
     */
    public function __getCloner()
    {
        return $this->__cloner__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     * @static
     */
    public function __getLazyProperties()
    {
        return self::$lazyPropertiesDefaults;
    }

    
    /**
     * {@inheritDoc}
     */
    public function getId()
    {
        if ($this->__isInitialized__ === false) {
            return (int)  parent::getId();
        }


        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getId', []);

        return parent::getId();
    }

    /**
     * {@inheritDoc}
     */
    public function setStatus($status)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setStatus', [$status]);

        return parent::setStatus($status);
    }

    /**
     * {@inheritDoc}
     */
    public function getStatus()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getStatus', []);

        return parent::getStatus();
    }

    /**
     * {@inheritDoc}
     */
    public function setCreatedAtAppointment($createdAtAppointment)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCreatedAtAppointment', [$createdAtAppointment]);

        return parent::setCreatedAtAppointment($createdAtAppointment);
    }

    /**
     * {@inheritDoc}
     */
    public function getCreatedAtAppointment()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCreatedAtAppointment', []);

        return parent::getCreatedAtAppointment();
    }

    /**
     * {@inheritDoc}
     */
    public function setIsActiveAppointment($isActiveAppointment)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setIsActiveAppointment', [$isActiveAppointment]);

        return parent::setIsActiveAppointment($isActiveAppointment);
    }

    /**
     * {@inheritDoc}
     */
    public function getIsActiveAppointment()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getIsActiveAppointment', []);

        return parent::getIsActiveAppointment();
    }

    /**
     * {@inheritDoc}
     */
    public function setUser(\Doctors\AdminBundle\Entity\User $user)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setUser', [$user]);

        return parent::setUser($user);
    }

    /**
     * {@inheritDoc}
     */
    public function getUser()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getUser', []);

        return parent::getUser();
    }

    /**
     * {@inheritDoc}
     */
    public function setAppointment($appointment)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setAppointment', [$appointment]);

        return parent::setAppointment($appointment);
    }

    /**
     * {@inheritDoc}
     */
    public function getAppointment()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getAppointment', []);

        return parent::getAppointment();
    }

    /**
     * {@inheritDoc}
     */
    public function setDoctor(\Doctors\AdminBundle\Entity\Doctor $doctor)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setDoctor', [$doctor]);

        return parent::setDoctor($doctor);
    }

    /**
     * {@inheritDoc}
     */
    public function getDoctor()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getDoctor', []);

        return parent::getDoctor();
    }

    /**
     * {@inheritDoc}
     */
    public function setEval($eval)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setEval', [$eval]);

        return parent::setEval($eval);
    }

    /**
     * {@inheritDoc}
     */
    public function getEval()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getEval', []);

        return parent::getEval();
    }

    /**
     * {@inheritDoc}
     */
    public function setReason($reason)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setReason', [$reason]);

        return parent::setReason($reason);
    }

    /**
     * {@inheritDoc}
     */
    public function getReason()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getReason', []);

        return parent::getReason();
    }

}
