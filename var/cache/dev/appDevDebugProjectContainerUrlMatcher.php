<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appDevDebugProjectContainerUrlMatcher.
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appDevDebugProjectContainerUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($pathinfo);
        $context = $this->context;
        $request = $this->request;

        if (0 === strpos($pathinfo, '/_')) {
            // _wdt
            if (0 === strpos($pathinfo, '/_wdt') && preg_match('#^/_wdt/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_wdt')), array (  '_controller' => 'web_profiler.controller.profiler:toolbarAction',));
            }

            if (0 === strpos($pathinfo, '/_profiler')) {
                // _profiler_home
                if (rtrim($pathinfo, '/') === '/_profiler') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', '_profiler_home');
                    }

                    return array (  '_controller' => 'web_profiler.controller.profiler:homeAction',  '_route' => '_profiler_home',);
                }

                if (0 === strpos($pathinfo, '/_profiler/search')) {
                    // _profiler_search
                    if ($pathinfo === '/_profiler/search') {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchAction',  '_route' => '_profiler_search',);
                    }

                    // _profiler_search_bar
                    if ($pathinfo === '/_profiler/search_bar') {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchBarAction',  '_route' => '_profiler_search_bar',);
                    }

                }

                // _profiler_info
                if (0 === strpos($pathinfo, '/_profiler/info') && preg_match('#^/_profiler/info/(?P<about>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_info')), array (  '_controller' => 'web_profiler.controller.profiler:infoAction',));
                }

                // _profiler_phpinfo
                if ($pathinfo === '/_profiler/phpinfo') {
                    return array (  '_controller' => 'web_profiler.controller.profiler:phpinfoAction',  '_route' => '_profiler_phpinfo',);
                }

                // _profiler_search_results
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/search/results$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_search_results')), array (  '_controller' => 'web_profiler.controller.profiler:searchResultsAction',));
                }

                // _profiler_open_file
                if ($pathinfo === '/_profiler/open') {
                    return array (  '_controller' => 'web_profiler.controller.profiler:openAction',  '_route' => '_profiler_open_file',);
                }

                // _profiler
                if (preg_match('#^/_profiler/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler')), array (  '_controller' => 'web_profiler.controller.profiler:panelAction',));
                }

                // _profiler_router
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/router$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_router')), array (  '_controller' => 'web_profiler.controller.router:panelAction',));
                }

                // _profiler_exception
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception')), array (  '_controller' => 'web_profiler.controller.exception:showAction',));
                }

                // _profiler_exception_css
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception\\.css$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception_css')), array (  '_controller' => 'web_profiler.controller.exception:cssAction',));
                }

            }

            // _twig_error_test
            if (0 === strpos($pathinfo, '/_error') && preg_match('#^/_error/(?P<code>\\d+)(?:\\.(?P<_format>[^/]++))?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_twig_error_test')), array (  '_controller' => 'twig.controller.preview_error:previewErrorPageAction',  '_format' => 'html',));
            }

        }

        // homepage
        if (rtrim($pathinfo, '/') === '') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'homepage');
            }

            return array (  '_controller' => 'Doctors\\EndpointBundle\\Controller\\DefaultController::indexAction',  '_route' => 'homepage',);
        }

        if (0 === strpos($pathinfo, '/user_')) {
            // user_api_add
            if ($pathinfo === '/user_api_add') {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_user_api_add;
                }

                return array (  '_controller' => 'Doctors\\EndpointBundle\\Controller\\UserApiController::api_register_userAction',  '_route' => 'user_api_add',);
            }
            not_user_api_add:

            // user_api_verify
            if (0 === strpos($pathinfo, '/user_verify_api') && preg_match('#^/user_verify_api/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_user_api_verify;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'user_api_verify')), array (  '_controller' => 'Doctors\\EndpointBundle\\Controller\\UserApiController::verifyAction',));
            }
            not_user_api_verify:

            // user_api_profile_complete
            if (0 === strpos($pathinfo, '/user_profile_complete_api') && preg_match('#^/user_profile_complete_api/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_user_api_profile_complete;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'user_api_profile_complete')), array (  '_controller' => 'Doctors\\EndpointBundle\\Controller\\UserApiController::profile_completeAction',));
            }
            not_user_api_profile_complete:

            // user_api_show_profile
            if (0 === strpos($pathinfo, '/user_show_profile_api') && preg_match('#^/user_show_profile_api/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_user_api_show_profile;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'user_api_show_profile')), array (  '_controller' => 'Doctors\\EndpointBundle\\Controller\\UserApiController::show_profile_userAction',));
            }
            not_user_api_show_profile:

        }

        // user_api_edit_user
        if (0 === strpos($pathinfo, '/edit_user_api') && preg_match('#^/edit_user_api/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_user_api_edit_user;
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'user_api_edit_user')), array (  '_controller' => 'Doctors\\EndpointBundle\\Controller\\UserApiController::api_edit_userAction',));
        }
        not_user_api_edit_user:

        if (0 === strpos($pathinfo, '/user_')) {
            // user_api_reset_password
            if ($pathinfo === '/user_reset_password_api') {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_user_api_reset_password;
                }

                return array (  '_controller' => 'Doctors\\EndpointBundle\\Controller\\UserApiController::resetAction',  '_route' => 'user_api_reset_password',);
            }
            not_user_api_reset_password:

            // user_api_login
            if ($pathinfo === '/user_login_api') {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_user_api_login;
                }

                return array (  '_controller' => 'Doctors\\EndpointBundle\\Controller\\UserApiController::loginAction',  '_route' => 'user_api_login',);
            }
            not_user_api_login:

            // user_api_update_picture
            if (0 === strpos($pathinfo, '/user_update_picture_api') && preg_match('#^/user_update_picture_api/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_user_api_update_picture;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'user_api_update_picture')), array (  '_controller' => 'Doctors\\EndpointBundle\\Controller\\UserApiController::updatePictureAction',));
            }
            not_user_api_update_picture:

            // user_api_add_picture
            if (0 === strpos($pathinfo, '/user_add_picture_api') && preg_match('#^/user_add_picture_api/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_user_api_add_picture;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'user_api_add_picture')), array (  '_controller' => 'Doctors\\EndpointBundle\\Controller\\UserApiController::addPictureAction',));
            }
            not_user_api_add_picture:

        }

        if (0 === strpos($pathinfo, '/a')) {
            // user_api_delete
            if (0 === strpos($pathinfo, '/api_delete_user') && preg_match('#^/api_delete_user/(?P<user_id>[^/]++)$#s', $pathinfo, $matches)) {
                if ($this->context->getMethod() != 'DELETE') {
                    $allow[] = 'DELETE';
                    goto not_user_api_delete;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'user_api_delete')), array (  '_controller' => 'Doctors\\EndpointBundle\\Controller\\UserApiController::api_delete_userAction',));
            }
            not_user_api_delete:

            // user_api_add_token
            if ($pathinfo === '/add_user_token') {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_user_api_add_token;
                }

                return array (  '_controller' => 'Doctors\\EndpointBundle\\Controller\\UserApiController::api_add_token_userAction',  '_route' => 'user_api_add_token',);
            }
            not_user_api_add_token:

        }

        if (0 === strpos($pathinfo, '/list_doctor')) {
            // doctor_api_listDoctors
            if ($pathinfo === '/list_doctors') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_doctor_api_listDoctors;
                }

                return array (  '_controller' => 'Doctors\\EndpointBundle\\Controller\\DoctorApiController::api_doctor_listAction',  '_route' => 'doctor_api_listDoctors',);
            }
            not_doctor_api_listDoctors:

            // doctor_api_listDoctorsSpeciality
            if (0 === strpos($pathinfo, '/list_doctor_speciality') && preg_match('#^/list_doctor_speciality/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_doctor_api_listDoctorsSpeciality;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'doctor_api_listDoctorsSpeciality')), array (  '_controller' => 'Doctors\\EndpointBundle\\Controller\\DoctorApiController::api_doctor_listDoctor_SpecialityAction',));
            }
            not_doctor_api_listDoctorsSpeciality:

        }

        // doctor_api_profile_doctor
        if (0 === strpos($pathinfo, '/profile_doctor') && preg_match('#^/profile_doctor/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                $allow = array_merge($allow, array('GET', 'HEAD'));
                goto not_doctor_api_profile_doctor;
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'doctor_api_profile_doctor')), array (  '_controller' => 'Doctors\\EndpointBundle\\Controller\\DoctorApiController::showDoctorProfileAction',));
        }
        not_doctor_api_profile_doctor:

        // doctor_api_profile_complete
        if (0 === strpos($pathinfo, '/doctor_profile_complete_api') && preg_match('#^/doctor_profile_complete_api/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_doctor_api_profile_complete;
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'doctor_api_profile_complete')), array (  '_controller' => 'Doctors\\EndpointBundle\\Controller\\DoctorApiController::profile_completeDoctorAction',));
        }
        not_doctor_api_profile_complete:

        // doctor_api_search
        if ($pathinfo === '/search_doctor') {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_doctor_api_search;
            }

            return array (  '_controller' => 'Doctors\\EndpointBundle\\Controller\\DoctorApiController::searchDoctorAction',  '_route' => 'doctor_api_search',);
        }
        not_doctor_api_search:

        if (0 === strpos($pathinfo, '/doctor_')) {
            // doctor_api_distance
            if ($pathinfo === '/doctor_distance') {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_doctor_api_distance;
                }

                return array (  '_controller' => 'Doctors\\EndpointBundle\\Controller\\DoctorApiController::calculDistanceAction',  '_route' => 'doctor_api_distance',);
            }
            not_doctor_api_distance:

            // doctor_api_update
            if (0 === strpos($pathinfo, '/doctor_update_api') && preg_match('#^/doctor_update_api/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_doctor_api_update;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'doctor_api_update')), array (  '_controller' => 'Doctors\\EndpointBundle\\Controller\\DoctorApiController::api_edit_doctorAction',));
            }
            not_doctor_api_update:

            // doctor_api_reset_password
            if ($pathinfo === '/doctor_reset_password_api') {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_doctor_api_reset_password;
                }

                return array (  '_controller' => 'Doctors\\EndpointBundle\\Controller\\DoctorApiController::api_resetAction',  '_route' => 'doctor_api_reset_password',);
            }
            not_doctor_api_reset_password:

        }

        // api_login
        if ($pathinfo === '/login_api') {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_api_login;
            }

            return array (  '_controller' => 'Doctors\\EndpointBundle\\Controller\\DoctorApiController::api_loginAction',  '_route' => 'api_login',);
        }
        not_api_login:

        if (0 === strpos($pathinfo, '/doctor_')) {
            // api_update_picture
            if (0 === strpos($pathinfo, '/doctor_update_picture_api') && preg_match('#^/doctor_update_picture_api/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_api_update_picture;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'api_update_picture')), array (  '_controller' => 'Doctors\\EndpointBundle\\Controller\\DoctorApiController::updateDoctorPictureAction',));
            }
            not_api_update_picture:

            // api_add_picture
            if (0 === strpos($pathinfo, '/doctor_add_picture_api') && preg_match('#^/doctor_add_picture_api/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_api_add_picture;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'api_add_picture')), array (  '_controller' => 'Doctors\\EndpointBundle\\Controller\\DoctorApiController::addDoctorPictureAction',));
            }
            not_api_add_picture:

        }

        if (0 === strpos($pathinfo, '/a')) {
            // doctor_api_delete
            if (0 === strpos($pathinfo, '/api_delete_doctor') && preg_match('#^/api_delete_doctor/(?P<doctor_id>[^/]++)$#s', $pathinfo, $matches)) {
                if ($this->context->getMethod() != 'DELETE') {
                    $allow[] = 'DELETE';
                    goto not_doctor_api_delete;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'doctor_api_delete')), array (  '_controller' => 'Doctors\\EndpointBundle\\Controller\\DoctorApiController::api_delete_doctorAction',));
            }
            not_doctor_api_delete:

            // doctor_api_add_token
            if ($pathinfo === '/add_doctor_token') {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_doctor_api_add_token;
                }

                return array (  '_controller' => 'Doctors\\EndpointBundle\\Controller\\DoctorApiController::api_add_token_doctorAction',  '_route' => 'doctor_api_add_token',);
            }
            not_doctor_api_add_token:

        }

        // loadDataTimeAppointment
        if ($pathinfo === '/load_hour_work') {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_loadDataTimeAppointment;
            }

            return array (  '_controller' => 'Doctors\\EndpointBundle\\Controller\\AppointmentApiController::loadDataHourWorkAction',  '_route' => 'loadDataTimeAppointment',);
        }
        not_loadDataTimeAppointment:

        // add_api_appointment
        if ($pathinfo === '/add_appointment') {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_add_api_appointment;
            }

            return array (  '_controller' => 'Doctors\\EndpointBundle\\Controller\\AppointmentApiController::addappointmentAction',  '_route' => 'add_api_appointment',);
        }
        not_add_api_appointment:

        // appointment_api_update_status
        if (0 === strpos($pathinfo, '/update_status_appointment') && preg_match('#^/update_status_appointment/(?P<appointment_id>[^/]++)$#s', $pathinfo, $matches)) {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_appointment_api_update_status;
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'appointment_api_update_status')), array (  '_controller' => 'Doctors\\EndpointBundle\\Controller\\AppointmentApiController::api_update_status_appointmentAction',));
        }
        not_appointment_api_update_status:

        if (0 === strpos($pathinfo, '/list_appointment_')) {
            // appointment_api_listConfirmed
            if ($pathinfo === '/list_appointment_confirmed') {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_appointment_api_listConfirmed;
                }

                return array (  '_controller' => 'Doctors\\EndpointBundle\\Controller\\AppointmentApiController::api_appointment_list_confirmedAction',  '_route' => 'appointment_api_listConfirmed',);
            }
            not_appointment_api_listConfirmed:

            if (0 === strpos($pathinfo, '/list_appointment_by_')) {
                // appointment_api_list_appointment_by_doctor
                if (0 === strpos($pathinfo, '/list_appointment_by_doctor') && preg_match('#^/list_appointment_by_doctor/(?P<doctor_id>[^/]++)$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_appointment_api_list_appointment_by_doctor;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'appointment_api_list_appointment_by_doctor')), array (  '_controller' => 'Doctors\\EndpointBundle\\Controller\\AppointmentApiController::api_get_appointment_by_doctorAction',));
                }
                not_appointment_api_list_appointment_by_doctor:

                // appointment_api_list_appointment_by_user
                if (0 === strpos($pathinfo, '/list_appointment_by_user') && preg_match('#^/list_appointment_by_user/(?P<user_id>[^/]++)$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_appointment_api_list_appointment_by_user;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'appointment_api_list_appointment_by_user')), array (  '_controller' => 'Doctors\\EndpointBundle\\Controller\\AppointmentApiController::api_get_appointment_by_userAction',));
                }
                not_appointment_api_list_appointment_by_user:

            }

        }

        // appointment_api_send_notification
        if ($pathinfo === '/send_notification') {
            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                $allow = array_merge($allow, array('GET', 'HEAD'));
                goto not_appointment_api_send_notification;
            }

            return array (  '_controller' => 'Doctors\\EndpointBundle\\Controller\\AppointmentApiController::sendNotificationAppointmentAction',  '_route' => 'appointment_api_send_notification',);
        }
        not_appointment_api_send_notification:

        // appointment_api_get_notification
        if ($pathinfo === '/get_notification') {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_appointment_api_get_notification;
            }

            return array (  '_controller' => 'Doctors\\EndpointBundle\\Controller\\AppointmentApiController::api_notificationListAction',  '_route' => 'appointment_api_get_notification',);
        }
        not_appointment_api_get_notification:

        // appointment_api_update_notification
        if ($pathinfo === '/update_notification') {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_appointment_api_update_notification;
            }

            return array (  '_controller' => 'Doctors\\EndpointBundle\\Controller\\AppointmentApiController::api_update_notificationListAction',  '_route' => 'appointment_api_update_notification',);
        }
        not_appointment_api_update_notification:

        if (0 === strpos($pathinfo, '/a')) {
            if (0 === strpos($pathinfo, '/api_')) {
                // appointment_api_delete
                if (0 === strpos($pathinfo, '/api_delete_appointment') && preg_match('#^/api_delete_appointment/(?P<appointment_id>[^/]++)$#s', $pathinfo, $matches)) {
                    if ($this->context->getMethod() != 'DELETE') {
                        $allow[] = 'DELETE';
                        goto not_appointment_api_delete;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'appointment_api_delete')), array (  '_controller' => 'Doctors\\EndpointBundle\\Controller\\AppointmentApiController::api_delete_appointmentAction',));
                }
                not_appointment_api_delete:

                if (0 === strpos($pathinfo, '/api_send_')) {
                    // appointment_cornJobs
                    if ($pathinfo === '/api_send_notification_corn_jobs') {
                        return array (  '_controller' => 'Doctors\\EndpointBundle\\Controller\\AppointmentApiController::api_send_notification_corn_jobsAction',  '_route' => 'appointment_cornJobs',);
                    }

                    // appointment_cornJobs_recall_notification
                    if ($pathinfo === '/api_send_recall_notification_corn_jobs') {
                        return array (  '_controller' => 'Doctors\\EndpointBundle\\Controller\\AppointmentApiController::cornJobsNotificationRecallAction',  '_route' => 'appointment_cornJobs_recall_notification',);
                    }

                }

            }

            // evaluation_api_add
            if ($pathinfo === '/add_evaluation') {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_evaluation_api_add;
                }

                return array (  '_controller' => 'Doctors\\EndpointBundle\\Controller\\EvaluationApiController::addEvaluationAction',  '_route' => 'evaluation_api_add',);
            }
            not_evaluation_api_add:

        }

        if (0 === strpos($pathinfo, '/get_evaluation_')) {
            // evaluation_api_doctors
            if (0 === strpos($pathinfo, '/get_evaluation_doctor') && preg_match('#^/get_evaluation_doctor/(?P<id_doctor>[^/]++)$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_evaluation_api_doctors;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'evaluation_api_doctors')), array (  '_controller' => 'Doctors\\EndpointBundle\\Controller\\EvaluationApiController::api_list_evaluationByDoctorAction',));
            }
            not_evaluation_api_doctors:

            // evaluation_api_users
            if (0 === strpos($pathinfo, '/get_evaluation_user') && preg_match('#^/get_evaluation_user/(?P<id_user>[^/]++)$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_evaluation_api_users;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'evaluation_api_users')), array (  '_controller' => 'Doctors\\EndpointBundle\\Controller\\EvaluationApiController::api_list_evaluationByUserAction',));
            }
            not_evaluation_api_users:

        }

        // evaluation_api_update
        if (0 === strpos($pathinfo, '/evaluation_update_api') && preg_match('#^/evaluation_update_api/(?P<evaluation_id>[^/]++)$#s', $pathinfo, $matches)) {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_evaluation_api_update;
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'evaluation_api_update')), array (  '_controller' => 'Doctors\\EndpointBundle\\Controller\\EvaluationApiController::api_edit_evaluationAction',));
        }
        not_evaluation_api_update:

        // api_evaluation_delete
        if (0 === strpos($pathinfo, '/delete_evaluation_api') && preg_match('#^/delete_evaluation_api/(?P<evaluation_id>[^/]++)$#s', $pathinfo, $matches)) {
            if ($this->context->getMethod() != 'DELETE') {
                $allow[] = 'DELETE';
                goto not_api_evaluation_delete;
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'api_evaluation_delete')), array (  '_controller' => 'Doctors\\EndpointBundle\\Controller\\EvaluationApiController::api_delete_evaluationAction',));
        }
        not_api_evaluation_delete:

        // speciality_api_add
        if ($pathinfo === '/speciality_api_add') {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_speciality_api_add;
            }

            return array (  '_controller' => 'Doctors\\EndpointBundle\\Controller\\SpecialityApiController::api_register_specialityAction',  '_route' => 'speciality_api_add',);
        }
        not_speciality_api_add:

        // speciality_api_edit
        if (0 === strpos($pathinfo, '/edit_speciality_api') && preg_match('#^/edit_speciality_api/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_speciality_api_edit;
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'speciality_api_edit')), array (  '_controller' => 'Doctors\\EndpointBundle\\Controller\\SpecialityApiController::api_edit_specialityAction',));
        }
        not_speciality_api_edit:

        // speciality_api_list
        if ($pathinfo === '/list_speciality') {
            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                $allow = array_merge($allow, array('GET', 'HEAD'));
                goto not_speciality_api_list;
            }

            return array (  '_controller' => 'Doctors\\EndpointBundle\\Controller\\SpecialityApiController::api_list_SpecialityAction',  '_route' => 'speciality_api_list',);
        }
        not_speciality_api_list:

        // speciality_api_delete
        if (0 === strpos($pathinfo, '/api_delete_speciality') && preg_match('#^/api_delete_speciality/(?P<speciality_id>[^/]++)$#s', $pathinfo, $matches)) {
            if ($this->context->getMethod() != 'DELETE') {
                $allow[] = 'DELETE';
                goto not_speciality_api_delete;
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'speciality_api_delete')), array (  '_controller' => 'Doctors\\EndpointBundle\\Controller\\SpecialityApiController::api_delete_specialityAction',));
        }
        not_speciality_api_delete:

        // endpoint_contact
        if ($pathinfo === '/contact') {
            if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                goto not_endpoint_contact;
            }

            return array (  '_controller' => 'Doctors\\EndpointBundle\\Controller\\DefaultController::contactAction',  '_route' => 'endpoint_contact',);
        }
        not_endpoint_contact:

        if (0 === strpos($pathinfo, '/admin')) {
            // doctors_admin_homepage
            if (rtrim($pathinfo, '/') === '/admin') {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'doctors_admin_homepage');
                }

                return array (  '_controller' => 'Doctors\\AdminBundle\\Controller\\AdminController::indexAction',  '_route' => 'doctors_admin_homepage',);
            }

            // doctors_admin_doctors
            if ($pathinfo === '/admin/doctors') {
                return array (  '_controller' => 'Doctors\\AdminBundle\\Controller\\DoctorController::doctorsAction',  '_route' => 'doctors_admin_doctors',);
            }

            // doctors_admin_showDoctor
            if (0 === strpos($pathinfo, '/admin/showDoctor') && preg_match('#^/admin/showDoctor/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'doctors_admin_showDoctor')), array (  '_controller' => 'Doctors\\AdminBundle\\Controller\\DoctorController::showDoctorAction',));
            }

            if (0 === strpos($pathinfo, '/admin/a')) {
                // bonjou
                if (0 === strpos($pathinfo, '/admin/akrem') && preg_match('#^/admin/akrem/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'bonjou')), array (  '_controller' => 'Doctors\\AdminBundle\\Controller\\DoctorController::akremAction',));
                }

                // doctors_admin_addDoctors
                if ($pathinfo === '/admin/addDoctor') {
                    return array (  '_controller' => 'Doctors\\AdminBundle\\Controller\\DoctorController::addDoctorAction',  '_route' => 'doctors_admin_addDoctors',);
                }

            }

            // doctors_admin_editDoctor
            if (0 === strpos($pathinfo, '/admin/editDoctor') && preg_match('#^/admin/editDoctor/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'doctors_admin_editDoctor')), array (  '_controller' => 'Doctors\\AdminBundle\\Controller\\DoctorController::editDoctorAction',));
            }

            // doctor_admin_addFastDoctor
            if ($pathinfo === '/admin/addFastDoctor') {
                return array (  '_controller' => 'Doctors\\AdminBundle\\Controller\\DoctorController::addFastDoctorAction',  '_route' => 'doctor_admin_addFastDoctor',);
            }

            // doctors_admin_editPhotoDoctor
            if (0 === strpos($pathinfo, '/admin/editPhotoDoctor') && preg_match('#^/admin/editPhotoDoctor/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'doctors_admin_editPhotoDoctor')), array (  '_controller' => 'Doctors\\AdminBundle\\Controller\\DoctorController::editPhotoDoctorAction',));
            }

            // doctors_admin_deleteDoctor
            if (0 === strpos($pathinfo, '/admin/deleteDoctor') && preg_match('#^/admin/deleteDoctor/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'doctors_admin_deleteDoctor')), array (  '_controller' => 'Doctors\\AdminBundle\\Controller\\DoctorController::deleteDoctorAction',));
            }

            // doctors_admin_users
            if ($pathinfo === '/admin/users') {
                return array (  '_controller' => 'Doctors\\AdminBundle\\Controller\\UserController::usersAction',  '_route' => 'doctors_admin_users',);
            }

            // doctors_admin_showUser
            if (0 === strpos($pathinfo, '/admin/showUser') && preg_match('#^/admin/showUser/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'doctors_admin_showUser')), array (  '_controller' => 'Doctors\\AdminBundle\\Controller\\UserController::showUserAction',));
            }

            // doctors_admin_addusers
            if ($pathinfo === '/admin/addUser') {
                return array (  '_controller' => 'Doctors\\AdminBundle\\Controller\\UserController::addUserAction',  '_route' => 'doctors_admin_addusers',);
            }

            if (0 === strpos($pathinfo, '/admin/edit')) {
                // doctors_admin_editUser
                if (0 === strpos($pathinfo, '/admin/editUser') && preg_match('#^/admin/editUser/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'doctors_admin_editUser')), array (  '_controller' => 'Doctors\\AdminBundle\\Controller\\UserController::editUserAction',));
                }

                // doctors_admin_editPhotoUser
                if (0 === strpos($pathinfo, '/admin/editPhotoUser') && preg_match('#^/admin/editPhotoUser/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'doctors_admin_editPhotoUser')), array (  '_controller' => 'Doctors\\AdminBundle\\Controller\\UserController::editPhotoUserAction',));
                }

            }

            // doctors_admin_deleteUser
            if (0 === strpos($pathinfo, '/admin/deleteUser') && preg_match('#^/admin/deleteUser/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'doctors_admin_deleteUser')), array (  '_controller' => 'Doctors\\AdminBundle\\Controller\\UserController::deleteUserAction',));
            }

            // doctors_admin_addEvaluation
            if ($pathinfo === '/admin/addEvaluation') {
                return array (  '_controller' => 'Doctors\\AdminBundle\\Controller\\EvaluationController::addEvaluationAction',  '_route' => 'doctors_admin_addEvaluation',);
            }

            // doctors_admin_showEvaluation
            if (0 === strpos($pathinfo, '/admin/showEvaluation') && preg_match('#^/admin/showEvaluation/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'doctors_admin_showEvaluation')), array (  '_controller' => 'Doctors\\AdminBundle\\Controller\\EvaluationController::showEvaluationAction',));
            }

            if (0 === strpos($pathinfo, '/admin/e')) {
                // doctors_admin_evaluation
                if ($pathinfo === '/admin/evaluations') {
                    return array (  '_controller' => 'Doctors\\AdminBundle\\Controller\\EvaluationController::evaluationsAction',  '_route' => 'doctors_admin_evaluation',);
                }

                // doctors_admin_editEvaluation
                if (0 === strpos($pathinfo, '/admin/editEvaluation') && preg_match('#^/admin/editEvaluation/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'doctors_admin_editEvaluation')), array (  '_controller' => 'Doctors\\AdminBundle\\Controller\\EvaluationController::editEvaluationAction',));
                }

            }

            // doctors_admin_deleteEvaluation
            if (0 === strpos($pathinfo, '/admin/deleteEvaluation') && preg_match('#^/admin/deleteEvaluation/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'doctors_admin_deleteEvaluation')), array (  '_controller' => 'Doctors\\AdminBundle\\Controller\\EvaluationController::deleteEvaluationAction',));
            }

            // doctors_admin_confirmEvaluation
            if (0 === strpos($pathinfo, '/admin/confirmEvaluation') && preg_match('#^/admin/confirmEvaluation/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'doctors_admin_confirmEvaluation')), array (  '_controller' => 'Doctors\\AdminBundle\\Controller\\EvaluationController::confirmEvaluationAction',));
            }

            // doctors_admin_unconfirmEvaluation
            if (0 === strpos($pathinfo, '/admin/unconfirmEvaluation') && preg_match('#^/admin/unconfirmEvaluation/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'doctors_admin_unconfirmEvaluation')), array (  '_controller' => 'Doctors\\AdminBundle\\Controller\\EvaluationController::unconfirmEvaluationAction',));
            }

            // doctors_admin_appointment
            if ($pathinfo === '/admin/appointments') {
                return array (  '_controller' => 'Doctors\\AdminBundle\\Controller\\AppointmentController::appointmentsAction',  '_route' => 'doctors_admin_appointment',);
            }

            // doctors_admin_showAppointment
            if (0 === strpos($pathinfo, '/admin/showAppointment') && preg_match('#^/admin/showAppointment/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'doctors_admin_showAppointment')), array (  '_controller' => 'Doctors\\AdminBundle\\Controller\\AppointmentController::showAppointmentAction',));
            }

            // doctors_admin_addAppointment
            if ($pathinfo === '/admin/addAppointment') {
                return array (  '_controller' => 'Doctors\\AdminBundle\\Controller\\AppointmentController::addAppointmentAction',  '_route' => 'doctors_admin_addAppointment',);
            }

            // doctors_admin_editAppointment
            if (0 === strpos($pathinfo, '/admin/editAppointment') && preg_match('#^/admin/editAppointment/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'doctors_admin_editAppointment')), array (  '_controller' => 'Doctors\\AdminBundle\\Controller\\AppointmentController::editAppointmentAction',));
            }

            // doctors_admin_deleteAppointment
            if (0 === strpos($pathinfo, '/admin/deleteAppointment') && preg_match('#^/admin/deleteAppointment/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'doctors_admin_deleteAppointment')), array (  '_controller' => 'Doctors\\AdminBundle\\Controller\\AppointmentController::deleteAppointmentAction',));
            }

            // loadDataTimeDoctor
            if ($pathinfo === '/admin/loadDataHourWork') {
                return array (  '_controller' => 'Doctors\\AdminBundle\\Controller\\AppointmentController::loadDataHourWorkAction',  '_route' => 'loadDataTimeDoctor',);
            }

            // doctorSpeciality_index
            if ($pathinfo === '/admin/specialities') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_doctorSpeciality_index;
                }

                return array (  '_controller' => 'Doctors\\AdminBundle\\Controller\\SpecialityController::indexSpecialityAction',  '_route' => 'doctorSpeciality_index',);
            }
            not_doctorSpeciality_index:

            // doctorSpeciality_show
            if (preg_match('#^/admin/(?P<id>[^/]++)/showSpeciality$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_doctorSpeciality_show;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'doctorSpeciality_show')), array (  '_controller' => 'Doctors\\AdminBundle\\Controller\\SpecialityController::showSpecialityAction',));
            }
            not_doctorSpeciality_show:

            // doctorSpeciality_new
            if ($pathinfo === '/admin/newSpeciality') {
                if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                    goto not_doctorSpeciality_new;
                }

                return array (  '_controller' => 'Doctors\\AdminBundle\\Controller\\SpecialityController::newSpecialityAction',  '_route' => 'doctorSpeciality_new',);
            }
            not_doctorSpeciality_new:

            // doctorSpeciality_edit
            if (preg_match('#^/admin/(?P<id>[^/]++)/editSpeciality$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                    goto not_doctorSpeciality_edit;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'doctorSpeciality_edit')), array (  '_controller' => 'Doctors\\AdminBundle\\Controller\\SpecialityController::editSpecialityAction',));
            }
            not_doctorSpeciality_edit:

            // doctorSpeciality_delete
            if (preg_match('#^/admin/(?P<id>[^/]++)/deleteSpeciality$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'doctorSpeciality_delete')), array (  '_controller' => 'Doctors\\AdminBundle\\Controller\\SpecialityController::deleteSpecialityAction',));
            }

        }

        if (0 === strpos($pathinfo, '/log')) {
            if (0 === strpos($pathinfo, '/login')) {
                // fos_user_security_login
                if ($pathinfo === '/login') {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_fos_user_security_login;
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::loginAction',  '_route' => 'fos_user_security_login',);
                }
                not_fos_user_security_login:

                // fos_user_security_check
                if ($pathinfo === '/login_check') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_fos_user_security_check;
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::checkAction',  '_route' => 'fos_user_security_check',);
                }
                not_fos_user_security_check:

            }

            // fos_user_security_logout
            if ($pathinfo === '/logout') {
                if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                    goto not_fos_user_security_logout;
                }

                return array (  '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::logoutAction',  '_route' => 'fos_user_security_logout',);
            }
            not_fos_user_security_logout:

        }

        if (0 === strpos($pathinfo, '/profile')) {
            // fos_user_profile_show
            if (rtrim($pathinfo, '/') === '/profile') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_fos_user_profile_show;
                }

                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'fos_user_profile_show');
                }

                return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ProfileController::showAction',  '_route' => 'fos_user_profile_show',);
            }
            not_fos_user_profile_show:

            // fos_user_profile_edit
            if ($pathinfo === '/profile/edit') {
                if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                    goto not_fos_user_profile_edit;
                }

                return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ProfileController::editAction',  '_route' => 'fos_user_profile_edit',);
            }
            not_fos_user_profile_edit:

        }

        if (0 === strpos($pathinfo, '/re')) {
            if (0 === strpos($pathinfo, '/register')) {
                // fos_user_registration_register
                if (rtrim($pathinfo, '/') === '/register') {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_fos_user_registration_register;
                    }

                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'fos_user_registration_register');
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::registerAction',  '_route' => 'fos_user_registration_register',);
                }
                not_fos_user_registration_register:

                if (0 === strpos($pathinfo, '/register/c')) {
                    // fos_user_registration_check_email
                    if ($pathinfo === '/register/check-email') {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_fos_user_registration_check_email;
                        }

                        return array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::checkEmailAction',  '_route' => 'fos_user_registration_check_email',);
                    }
                    not_fos_user_registration_check_email:

                    if (0 === strpos($pathinfo, '/register/confirm')) {
                        // fos_user_registration_confirm
                        if (preg_match('#^/register/confirm/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_fos_user_registration_confirm;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_user_registration_confirm')), array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::confirmAction',));
                        }
                        not_fos_user_registration_confirm:

                        // fos_user_registration_confirmed
                        if ($pathinfo === '/register/confirmed') {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_fos_user_registration_confirmed;
                            }

                            return array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::confirmedAction',  '_route' => 'fos_user_registration_confirmed',);
                        }
                        not_fos_user_registration_confirmed:

                    }

                }

            }

            if (0 === strpos($pathinfo, '/resetting')) {
                // fos_user_resetting_request
                if ($pathinfo === '/resetting/request') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_fos_user_resetting_request;
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::requestAction',  '_route' => 'fos_user_resetting_request',);
                }
                not_fos_user_resetting_request:

                // fos_user_resetting_send_email
                if ($pathinfo === '/resetting/send-email') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_fos_user_resetting_send_email;
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::sendEmailAction',  '_route' => 'fos_user_resetting_send_email',);
                }
                not_fos_user_resetting_send_email:

                // fos_user_resetting_check_email
                if ($pathinfo === '/resetting/check-email') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_fos_user_resetting_check_email;
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::checkEmailAction',  '_route' => 'fos_user_resetting_check_email',);
                }
                not_fos_user_resetting_check_email:

                // fos_user_resetting_reset
                if (0 === strpos($pathinfo, '/resetting/reset') && preg_match('#^/resetting/reset/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_fos_user_resetting_reset;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_user_resetting_reset')), array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::resetAction',));
                }
                not_fos_user_resetting_reset:

            }

        }

        // fos_user_change_password
        if ($pathinfo === '/profile/change-password') {
            if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                goto not_fos_user_change_password;
            }

            return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ChangePasswordController::changePasswordAction',  '_route' => 'fos_user_change_password',);
        }
        not_fos_user_change_password:

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
