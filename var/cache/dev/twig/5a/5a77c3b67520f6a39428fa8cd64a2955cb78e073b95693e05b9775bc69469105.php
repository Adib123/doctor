<?php

/* @FOSUser/Security/login_content.html.twig */
class __TwigTemplate_352384729e96cb7857ea90f87e5845f4ced2c0df1bcc489a44f499a3f421a9c7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3447bcf8c2e6ad91dc87987b515e4a9b0df93b9bb64f37661e3ae8f19c4f4024 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3447bcf8c2e6ad91dc87987b515e4a9b0df93b9bb64f37661e3ae8f19c4f4024->enter($__internal_3447bcf8c2e6ad91dc87987b515e4a9b0df93b9bb64f37661e3ae8f19c4f4024_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Security/login_content.html.twig"));

        $__internal_b1eed269be9db8b2799f02e6ffd21a8161f09e9dc6837bec710a071b57c15f7f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b1eed269be9db8b2799f02e6ffd21a8161f09e9dc6837bec710a071b57c15f7f->enter($__internal_b1eed269be9db8b2799f02e6ffd21a8161f09e9dc6837bec710a071b57c15f7f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Security/login_content.html.twig"));

        // line 2
        echo "

<div class=\"preloader\">
    <div class=\"cssload-speeding-wheel\"></div>
</div>
<section id=\"wrapper\" class=\"login-register\" style=\"direction: rtl;\">
    <div class=\"login-box\">
        <div class=\"white-box\">
<form action=\"";
        // line 10
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_security_check");
        echo "\" class=\"form-horizontal form-material\" id=\"loginform\" method=\"post\">
    ";
        // line 11
        if ((isset($context["csrf_token"]) ? $context["csrf_token"] : $this->getContext($context, "csrf_token"))) {
            // line 12
            echo "        <input type=\"hidden\" name=\"_csrf_token\" value=\"";
            echo twig_escape_filter($this->env, (isset($context["csrf_token"]) ? $context["csrf_token"] : $this->getContext($context, "csrf_token")), "html", null, true);
            echo "\" />
    ";
        }
        // line 14
        echo "    <h3 class=\"box-title m-b-20 arabe text-center authentification\"><i class=\"fa fa-lock\" aria-hidden=\"true\"></i>
        لوحة التحكم</h3>
    <div class=\"form-group \">
        <div class=\"col-xs-12\">
            <label for=\"username\" class=\"arabe\"><i class=\"fa fa-user\" aria-hidden=\"true\"></i>
                اسم الأدمين</label>
    <input type=\"text\" id=\"username\" name=\"_username\" value=\"";
        // line 20
        echo twig_escape_filter($this->env, (isset($context["last_username"]) ? $context["last_username"] : $this->getContext($context, "last_username")), "html", null, true);
        echo "\" required=\"required\"  class=\"form-control arabe\" />
        </div></div>


    <div class=\"form-group\">
        <div class=\"col-xs-12\">
            <label for=\"password\" class=\"arabe\"><i class=\"fa fa-unlock-alt\" aria-hidden=\"true\"></i>
                كلمة المرور</label>
    <input type=\"password\" id=\"password\" name=\"_password\" required=\"required\" class=\"form-control arabe\" />
        </div></div>
    <div class=\"form-group\">
        <div class=\"col-md-12\">
            <div class=\"checkbox checkbox-primary pull-left p-t-0\">
    <input type=\"checkbox\" id=\"remember_me\" name=\"_remember_me\" value=\"on\" />
    <label for=\"remember_me\" class=\"arabe\"> تذكرني </label>
            </div>
            ";
        // line 36
        echo "</div>
    </div>
    <div class=\"form-group text-center m-t-20\">
        <div class=\"col-xs-12\">
    <input class=\"btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light arabe   \" type=\"submit\" id=\"_submit\" name=\"_submit\" value=\"تسجيل الدخول\" />
        </div>
    </div>
    ";
        // line 43
        if ((isset($context["error"]) ? $context["error"] : $this->getContext($context, "error"))) {
            // line 44
            echo "        <div class=\"alert alert-danger\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans($this->getAttribute((isset($context["error"]) ? $context["error"] : $this->getContext($context, "error")), "messageKey", array()), $this->getAttribute((isset($context["error"]) ? $context["error"] : $this->getContext($context, "error")), "messageData", array()), "security"), "html", null, true);
            echo "</div>
    ";
        }
        // line 46
        echo "</form></div></div></section>
";
        
        $__internal_3447bcf8c2e6ad91dc87987b515e4a9b0df93b9bb64f37661e3ae8f19c4f4024->leave($__internal_3447bcf8c2e6ad91dc87987b515e4a9b0df93b9bb64f37661e3ae8f19c4f4024_prof);

        
        $__internal_b1eed269be9db8b2799f02e6ffd21a8161f09e9dc6837bec710a071b57c15f7f->leave($__internal_b1eed269be9db8b2799f02e6ffd21a8161f09e9dc6837bec710a071b57c15f7f_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Security/login_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  91 => 46,  85 => 44,  83 => 43,  74 => 36,  55 => 20,  47 => 14,  41 => 12,  39 => 11,  35 => 10,  25 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% trans_default_domain 'FOSUserBundle' %}


<div class=\"preloader\">
    <div class=\"cssload-speeding-wheel\"></div>
</div>
<section id=\"wrapper\" class=\"login-register\" style=\"direction: rtl;\">
    <div class=\"login-box\">
        <div class=\"white-box\">
<form action=\"{{ path(\"fos_user_security_check\") }}\" class=\"form-horizontal form-material\" id=\"loginform\" method=\"post\">
    {% if csrf_token %}
        <input type=\"hidden\" name=\"_csrf_token\" value=\"{{ csrf_token }}\" />
    {% endif %}
    <h3 class=\"box-title m-b-20 arabe text-center authentification\"><i class=\"fa fa-lock\" aria-hidden=\"true\"></i>
        لوحة التحكم</h3>
    <div class=\"form-group \">
        <div class=\"col-xs-12\">
            <label for=\"username\" class=\"arabe\"><i class=\"fa fa-user\" aria-hidden=\"true\"></i>
                اسم الأدمين</label>
    <input type=\"text\" id=\"username\" name=\"_username\" value=\"{{ last_username }}\" required=\"required\"  class=\"form-control arabe\" />
        </div></div>


    <div class=\"form-group\">
        <div class=\"col-xs-12\">
            <label for=\"password\" class=\"arabe\"><i class=\"fa fa-unlock-alt\" aria-hidden=\"true\"></i>
                كلمة المرور</label>
    <input type=\"password\" id=\"password\" name=\"_password\" required=\"required\" class=\"form-control arabe\" />
        </div></div>
    <div class=\"form-group\">
        <div class=\"col-md-12\">
            <div class=\"checkbox checkbox-primary pull-left p-t-0\">
    <input type=\"checkbox\" id=\"remember_me\" name=\"_remember_me\" value=\"on\" />
    <label for=\"remember_me\" class=\"arabe\"> تذكرني </label>
            </div>
            {#<a href=\"javascript:void(0)\" id=\"to-recover\" class=\"text-dark pull-right\"><i class=\"fa fa-lock m-r-5\"></i> Forgot pwd?</a> #}</div>
    </div>
    <div class=\"form-group text-center m-t-20\">
        <div class=\"col-xs-12\">
    <input class=\"btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light arabe   \" type=\"submit\" id=\"_submit\" name=\"_submit\" value=\"تسجيل الدخول\" />
        </div>
    </div>
    {% if error %}
        <div class=\"alert alert-danger\">{{ error.messageKey|trans(error.messageData, 'security') }}</div>
    {% endif %}
</form></div></div></section>
", "@FOSUser/Security/login_content.html.twig", "C:\\wamp64\\www\\doctorAdmin\\app\\Resources\\FOSUserBundle\\views\\Security\\login_content.html.twig");
    }
}
