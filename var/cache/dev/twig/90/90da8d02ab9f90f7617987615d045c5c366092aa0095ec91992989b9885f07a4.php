<?php

/* @WebProfiler/Collector/router.html.twig */
class __TwigTemplate_0fd258d5cd90e6569d1ab61f7a8b5064101977573920a7e0c68993aed9a371c4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_81723ea090774a38a143504b24dcdfbca96a6e6a88fd4ef0ea3104eb789e31fc = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_81723ea090774a38a143504b24dcdfbca96a6e6a88fd4ef0ea3104eb789e31fc->enter($__internal_81723ea090774a38a143504b24dcdfbca96a6e6a88fd4ef0ea3104eb789e31fc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $__internal_1b499e3cf7d259b6b75ae11a89a29e6050762e4896f313ea8fdee37a1944213c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1b499e3cf7d259b6b75ae11a89a29e6050762e4896f313ea8fdee37a1944213c->enter($__internal_1b499e3cf7d259b6b75ae11a89a29e6050762e4896f313ea8fdee37a1944213c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_81723ea090774a38a143504b24dcdfbca96a6e6a88fd4ef0ea3104eb789e31fc->leave($__internal_81723ea090774a38a143504b24dcdfbca96a6e6a88fd4ef0ea3104eb789e31fc_prof);

        
        $__internal_1b499e3cf7d259b6b75ae11a89a29e6050762e4896f313ea8fdee37a1944213c->leave($__internal_1b499e3cf7d259b6b75ae11a89a29e6050762e4896f313ea8fdee37a1944213c_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_b0f032dcdf363738061df890cec53119ee5accb3ea295f75b555f4dccc7400e1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b0f032dcdf363738061df890cec53119ee5accb3ea295f75b555f4dccc7400e1->enter($__internal_b0f032dcdf363738061df890cec53119ee5accb3ea295f75b555f4dccc7400e1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        $__internal_0a1847c773ad501f94959232f53ebc70275a3247204be9f122b4c08343c965c5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0a1847c773ad501f94959232f53ebc70275a3247204be9f122b4c08343c965c5->enter($__internal_0a1847c773ad501f94959232f53ebc70275a3247204be9f122b4c08343c965c5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_0a1847c773ad501f94959232f53ebc70275a3247204be9f122b4c08343c965c5->leave($__internal_0a1847c773ad501f94959232f53ebc70275a3247204be9f122b4c08343c965c5_prof);

        
        $__internal_b0f032dcdf363738061df890cec53119ee5accb3ea295f75b555f4dccc7400e1->leave($__internal_b0f032dcdf363738061df890cec53119ee5accb3ea295f75b555f4dccc7400e1_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_37b2d9ec0fb08e753b4b4c9417e987a7c84d4a618e185d237161474fed984709 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_37b2d9ec0fb08e753b4b4c9417e987a7c84d4a618e185d237161474fed984709->enter($__internal_37b2d9ec0fb08e753b4b4c9417e987a7c84d4a618e185d237161474fed984709_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_12737bb5e9701e84945e4b3c01bffc7f05c3c23f8d3fc0911ab5204d5ac1d941 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_12737bb5e9701e84945e4b3c01bffc7f05c3c23f8d3fc0911ab5204d5ac1d941->enter($__internal_12737bb5e9701e84945e4b3c01bffc7f05c3c23f8d3fc0911ab5204d5ac1d941_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_12737bb5e9701e84945e4b3c01bffc7f05c3c23f8d3fc0911ab5204d5ac1d941->leave($__internal_12737bb5e9701e84945e4b3c01bffc7f05c3c23f8d3fc0911ab5204d5ac1d941_prof);

        
        $__internal_37b2d9ec0fb08e753b4b4c9417e987a7c84d4a618e185d237161474fed984709->leave($__internal_37b2d9ec0fb08e753b4b4c9417e987a7c84d4a618e185d237161474fed984709_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_670199a52cc30b7721784d8525ab1cce4b94f52f7b5b7853d9c2fbe770ffe354 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_670199a52cc30b7721784d8525ab1cce4b94f52f7b5b7853d9c2fbe770ffe354->enter($__internal_670199a52cc30b7721784d8525ab1cce4b94f52f7b5b7853d9c2fbe770ffe354_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_f5bb741849e9a80fa274fcc7bf1792b319127a64e34edee3a9fb5231401e5a26 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f5bb741849e9a80fa274fcc7bf1792b319127a64e34edee3a9fb5231401e5a26->enter($__internal_f5bb741849e9a80fa274fcc7bf1792b319127a64e34edee3a9fb5231401e5a26_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_router", array("token" => (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_f5bb741849e9a80fa274fcc7bf1792b319127a64e34edee3a9fb5231401e5a26->leave($__internal_f5bb741849e9a80fa274fcc7bf1792b319127a64e34edee3a9fb5231401e5a26_prof);

        
        $__internal_670199a52cc30b7721784d8525ab1cce4b94f52f7b5b7853d9c2fbe770ffe354->leave($__internal_670199a52cc30b7721784d8525ab1cce4b94f52f7b5b7853d9c2fbe770ffe354_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  94 => 13,  85 => 12,  71 => 7,  68 => 6,  59 => 5,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}{% endblock %}

{% block menu %}
<span class=\"label\">
    <span class=\"icon\">{{ include('@WebProfiler/Icon/router.svg') }}</span>
    <strong>Routing</strong>
</span>
{% endblock %}

{% block panel %}
    {{ render(path('_profiler_router', { token: token })) }}
{% endblock %}
", "@WebProfiler/Collector/router.html.twig", "C:\\wamp64\\www\\doctorAdmin\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle\\Resources\\views\\Collector\\router.html.twig");
    }
}
