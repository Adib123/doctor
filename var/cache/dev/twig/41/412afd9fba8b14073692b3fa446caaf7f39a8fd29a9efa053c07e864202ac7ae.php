<?php

/* DoctorsAdminBundle:Doctor:showDoctor.html.twig */
class __TwigTemplate_7f2e52dadb0ac679e251e0b348e1bd756210751808966edda0b6ea907351e920 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::layoutAdmin.html.twig", "DoctorsAdminBundle:Doctor:showDoctor.html.twig", 1);
        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::layoutAdmin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_315c295afd4cb63ff75ea95900af87a553e7c34779e935cf02b4a22909a4775d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_315c295afd4cb63ff75ea95900af87a553e7c34779e935cf02b4a22909a4775d->enter($__internal_315c295afd4cb63ff75ea95900af87a553e7c34779e935cf02b4a22909a4775d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "DoctorsAdminBundle:Doctor:showDoctor.html.twig"));

        $__internal_5e4123e6d114289eb6b5bfd3384aad0430baee3e11054311c86245aeceedaf8d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5e4123e6d114289eb6b5bfd3384aad0430baee3e11054311c86245aeceedaf8d->enter($__internal_5e4123e6d114289eb6b5bfd3384aad0430baee3e11054311c86245aeceedaf8d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "DoctorsAdminBundle:Doctor:showDoctor.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_315c295afd4cb63ff75ea95900af87a553e7c34779e935cf02b4a22909a4775d->leave($__internal_315c295afd4cb63ff75ea95900af87a553e7c34779e935cf02b4a22909a4775d_prof);

        
        $__internal_5e4123e6d114289eb6b5bfd3384aad0430baee3e11054311c86245aeceedaf8d->leave($__internal_5e4123e6d114289eb6b5bfd3384aad0430baee3e11054311c86245aeceedaf8d_prof);

    }

    // line 2
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_7d72d9eef9b4332451eac4a7aed747a34b730521457b112891cf3a0df70302ff = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7d72d9eef9b4332451eac4a7aed747a34b730521457b112891cf3a0df70302ff->enter($__internal_7d72d9eef9b4332451eac4a7aed747a34b730521457b112891cf3a0df70302ff_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_6d4864bee47e1bc8076d3735093efbce9d09ec64827824a6e52e6729c21fd2f7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6d4864bee47e1bc8076d3735093efbce9d09ec64827824a6e52e6729c21fd2f7->enter($__internal_6d4864bee47e1bc8076d3735093efbce9d09ec64827824a6e52e6729c21fd2f7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 3
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    ";
        // line 5
        echo "    <!-- Integral core stylesheet -->
    ";
        // line 7
        echo "    <!-- /integral core stylesheet -->

    <!--Jvector Map-->
    ";
        // line 11
        echo "
    ";
        // line 13
        echo "
    <!-- Bootstrap RTL stylesheet min version -->
    ";
        // line 16
        echo "    <!-- /bootstrap rtl stylesheet min version -->

    <!-- Integral RTL core stylesheet -->
    ";
        // line 20
        echo "
    ";
        // line 22
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("datatable/jquery.dataTables.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
    <link href=\"";
        // line 23
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("datatable/buttons.dataTables.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">

    <link href=\"";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("datatable/select2.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">

    <link href=\"";
        // line 27
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("datatable/bootstrap-toggle.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
";
        
        $__internal_6d4864bee47e1bc8076d3735093efbce9d09ec64827824a6e52e6729c21fd2f7->leave($__internal_6d4864bee47e1bc8076d3735093efbce9d09ec64827824a6e52e6729c21fd2f7_prof);

        
        $__internal_7d72d9eef9b4332451eac4a7aed747a34b730521457b112891cf3a0df70302ff->leave($__internal_7d72d9eef9b4332451eac4a7aed747a34b730521457b112891cf3a0df70302ff_prof);

    }

    // line 30
    public function block_body($context, array $blocks = array())
    {
        $__internal_ba33369d74a2463eea11cc3d14894fa5cb0bc405100acc071e34df37c68e58ba = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ba33369d74a2463eea11cc3d14894fa5cb0bc405100acc071e34df37c68e58ba->enter($__internal_ba33369d74a2463eea11cc3d14894fa5cb0bc405100acc071e34df37c68e58ba_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_25384984af376c8aeadbc04ad7f177fc427aa1dd1b18af95c9ed1ec6f69af924 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_25384984af376c8aeadbc04ad7f177fc427aa1dd1b18af95c9ed1ec6f69af924->enter($__internal_25384984af376c8aeadbc04ad7f177fc427aa1dd1b18af95c9ed1ec6f69af924_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 31
        echo "    <!-- /main header -->

    <div class=\"preloader\">
        <div class=\"cssload-speeding-wheel\"></div>
    </div>
    <div id=\"page-wrapper\">
        <div class=\"container-fluid\">
            <div class=\"row bg-title\">
                <div class=\"col-lg-3 col-md-4 col-sm-4 col-xs-12\">
                    <h4 class=\"page-title arabe\">المعلومات التي تخص الطبيب \"<strong>";
        // line 40
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["doctor"]) ? $context["doctor"] : $this->getContext($context, "doctor")), "nameDoctor", array()), "html", null, true);
        echo "</strong>\"</h4>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <div class=\"row\">
                <div class=\"col-lg-12\">


                    <div class=\"panel panel-default\">
                        <div class=\"panel-heading clearfix\">
                            <h4 class=\"panel-title arabe\">عرض المعلومات</h4>
                        </div>


                        <div class=\"panel-body\">
                            <div class=\"table-responsive\">
                                <ul class=\"list-group\">
                                    <li class=\"list-group-item\">
                                        <div class=\"row\">
                                            <div class=\"col-lg-2\">
                                                <strong class=\"arabe\">اسم الطبيب </strong>
                                            </div>
                                            <div class=\"col-lg-4 arabe\">
                                                ";
        // line 63
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["doctor"]) ? $context["doctor"] : $this->getContext($context, "doctor")), "nameDoctor", array()), "html", null, true);
        echo "
                                            </div>
                                        </div>
                                    </li>
                                    <li class=\"list-group-item\">
                                        <div class=\"row\">
                                            <div class=\"col-lg-2 arabe\">
                                                <strong>رقم التواصل  </strong>
                                            </div>
                                            <div class=\"col-lg-4 arabe\">
                                                ";
        // line 73
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["doctor"]) ? $context["doctor"] : $this->getContext($context, "doctor")), "phoneNumberDoctor", array()), "html", null, true);
        echo "
                                            </div>
                                        </div>
                                    </li>
                                    <li class=\"list-group-item\">
                                        <div class=\"row\">
                                            <div class=\"col-lg-2 arabe\">
                                                <strong>تاريخ الاضافة  </strong>
                                            </div>
                                            <div class=\"col-lg-4 arabe\">
                                                ";
        // line 83
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["doctor"]) ? $context["doctor"] : $this->getContext($context, "doctor")), "createdAtDoctor", array()), "d/m/Y"), "html", null, true);
        echo "
                                            </div>
                                        </div>
                                    </li>
                                    <li class=\"list-group-item\">
                                        <div class=\"row\">
                                            <div class=\"col-lg-2 arabe\">
                                                <strong>الاختصاص </strong>
                                            </div>
                                            <div class=\"col-lg-4 arabe\">
                                                ";
        // line 93
        if ( !$this->getAttribute($this->getAttribute((isset($context["doctor"]) ? $context["doctor"] : null), "speciality", array(), "any", false, true), "nameSpeciality", array(), "any", true, true)) {
            // line 94
            echo "                                                    طبيب عام
                                                ";
        } else {
            // line 96
            echo "                                                    ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["doctor"]) ? $context["doctor"] : $this->getContext($context, "doctor")), "speciality", array()), "nameSpeciality", array()), "html", null, true);
            echo "
                                                ";
        }
        // line 98
        echo "                                            </div>
                                        </div>
                                    </li>
                                    <li class=\"list-group-item\">
                                        <div class=\"row\">
                                            <div class=\"col-lg-2 arabe\">
                                                <strong>الجنسية </strong>
                                            </div>
                                            <div class=\"col-lg-4 arabe\">
                                                ";
        // line 107
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["doctor"]) ? $context["doctor"] : $this->getContext($context, "doctor")), "nationality", array()), "html", null, true);
        echo "
                                            </div>
                                        </div>
                                    </li>
                                    <li class=\"list-group-item\">
                                        <div class=\"row\">
                                            <div class=\"col-lg-2 arabe\">
                                                <strong>البلد </strong>
                                            </div>
                                            <div class=\"col-lg-4 arabe\">
                                                ";
        // line 117
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["doctor"]) ? $context["doctor"] : $this->getContext($context, "doctor")), "countryDoctor", array()), "html", null, true);
        echo "
                                            </div>
                                        </div>
                                    </li>
                                    <li class=\"list-group-item\">
                                        <div class=\"row\">
                                            <div class=\"col-lg-2 arabe\">
                                                <strong>المدينة </strong>
                                            </div>
                                            <div class=\"col-lg-4 arabe\">
                                                ";
        // line 127
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["doctor"]) ? $context["doctor"] : $this->getContext($context, "doctor")), "townDoctor", array()), "html", null, true);
        echo "
                                            </div>
                                        </div>
                                    </li>

                                    <li class=\"list-group-item\">
                                        <div class=\"row\">
                                            <div class=\"col-lg-2 arabe\">
                                                <strong>الحي </strong>
                                            </div>
                                            <div class=\"col-lg-4 arabe\">
                                                ";
        // line 138
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["doctor"]) ? $context["doctor"] : $this->getContext($context, "doctor")), "cityDoctor", array()), "html", null, true);
        echo "
                                            </div>
                                        </div>
                                    </li>
                                    <li class=\"list-group-item\">
                                        <div class=\"row\">
                                            <div class=\"col-lg-2 arabe\">
                                                <strong>مكان العمل </strong>
                                            </div>
                                            <div class=\"col-lg-4 arabe\">
                                                ";
        // line 148
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["doctor"]) ? $context["doctor"] : $this->getContext($context, "doctor")), "workplace", array()), "html", null, true);
        echo "
                                            </div>
                                        </div>
                                    </li>
                                    <li class=\"list-group-item\">
                                        <div class=\"row\">
                                            <div class=\"col-lg-2 arabe\">
                                                <strong>اسم جهة العمل </strong>
                                            </div>
                                            <div class=\"col-lg-4 arabe\">
                                                ";
        // line 158
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["doctor"]) ? $context["doctor"] : $this->getContext($context, "doctor")), "workplaceName", array()), "html", null, true);
        echo "
                                            </div>
                                        </div>
                                    </li>
                                    <li class=\"list-group-item\">
                                        <div class=\"row\">
                                            <div class=\"col-lg-2 arabe\">
                                                <strong>بداية الدوام </strong>
                                            </div>
                                            <div class=\"col-lg-4 arabe\">
                                                على الساعة";
        // line 168
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["doctor"]) ? $context["doctor"] : $this->getContext($context, "doctor")), "startTimeWork", array()), "H"), "html", null, true);
        echo "  و
                                                ";
        // line 169
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["doctor"]) ? $context["doctor"] : $this->getContext($context, "doctor")), "startTimeWork", array()), "i"), "html", null, true);
        echo "  دقيقة
                                            </div>
                                        </div>
                                    </li>
                                    <li class=\"list-group-item\">
                                        <div class=\"row\">
                                            <div class=\"col-lg-2 arabe\">
                                                <strong>نهاية الدوام </strong>
                                            </div>
                                            <div class=\"col-lg-4 arabe\">
                                                على الساعة";
        // line 179
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["doctor"]) ? $context["doctor"] : $this->getContext($context, "doctor")), "endTimeWork", array()), "H"), "html", null, true);
        echo " و
                                                ";
        // line 180
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["doctor"]) ? $context["doctor"] : $this->getContext($context, "doctor")), "endTimeWork", array()), "i"), "html", null, true);
        echo "    دقيقة
                                            </div>
                                        </div>
                                    </li>
                                    <li class=\"list-group-item\">
                                        <div class=\"row\">
                                            <div class=\"col-lg-2 arabe\">
                                                <strong>الايميل </strong>
                                            </div>
                                            <div class=\"col-lg-4 arabe\">
                                                ";
        // line 190
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["doctor"]) ? $context["doctor"] : $this->getContext($context, "doctor")), "emailDoctor", array()), "html", null, true);
        echo "
                                            </div>
                                        </div>
                                    </li>

                                    <li class=\"list-group-item\">
                                        <div class=\"row\">
                                            <div class=\"col-lg-2 arabe\">
                                                <strong class=\"arabe\">الصورة</strong>
                                            </div>
                                            <div class=\"col-lg-10\">
                                                <div class=\"row\">


                                                    <div class=\"col-lg-3\">
                                                        <a href=\"#\" id=\"show-";
        // line 205
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["doctor"]) ? $context["doctor"] : $this->getContext($context, "doctor")), "id", array()), "html", null, true);
        echo "\" data-toggle=\"modal\"
                                                           data-target=\"#show-modal-";
        // line 206
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["doctor"]) ? $context["doctor"] : $this->getContext($context, "doctor")), "id", array()), "html", null, true);
        echo "\">

                                                            <img src=\"";
        // line 208
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl(("uploads/doctors/" . $this->getAttribute((isset($context["doctor"]) ? $context["doctor"] : $this->getContext($context, "doctor")), "photoDoctor", array()))), "html", null, true);
        echo "\"
                                                                 width=\"180\" height=\"150\" id=\"small-img-";
        // line 209
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["doctor"]) ? $context["doctor"] : $this->getContext($context, "doctor")), "id", array()), "html", null, true);
        echo "\" class=\"thumbnail\">

                                                        </a>

                                                    </div>


                                                    <div class=\"modal fade\" id=\"show-modal-";
        // line 216
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["doctor"]) ? $context["doctor"] : $this->getContext($context, "doctor")), "id", array()), "html", null, true);
        echo "\" tabindex=\"-1\"
                                                         role=\"dialog\" aria-labelledby=\"myModalLabel\"
                                                         aria-hidden=\"true\">
                                                        <div class=\"modal-dialog\">
                                                            <div class=\"modal-content\">

                                                                <div class=\"modal-header\">
                                                                    <button type=\"button\" class=\"close\"
                                                                            data-dismiss=\"modal\" aria-hidden=\"true\">
                                                                        &times;
                                                                    </button>
                                                                    <h4 class=\"modal-title arabe\" id=\"myModalLabel\"> اظهار
                                                                        الصورة</h4>
                                                                </div>

                                                                <div class=\"modal-body\">
                                                                    <center>
                                                                        <img src=\"";
        // line 233
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl(("uploads/doctors/" . $this->getAttribute((isset($context["doctor"]) ? $context["doctor"] : $this->getContext($context, "doctor")), "photoDoctor", array()))), "html", null, true);
        echo "\"
                                                                             width=\"500\" height=\"300\" id=\"large-img-7\">
                                                                    </center>
                                                                </div>

                                                                <div class=\"modal-footer\">
                                                                    <button type=\"button\" class=\"btn btn-default arabe\"
                                                                            data-dismiss=\"modal\"> رجوع
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class=\"list-group-item\">
                                        <div class=\"row\">
                                            <div class=\"col-lg-2 arabe\"><strong> قائمة المواعيد  </strong></div>
                                            <div class=\"col-lg-10\">
                                                ";
        // line 254
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["appointments"]) ? $context["appointments"] : $this->getContext($context, "appointments")));
        foreach ($context['_seq'] as $context["_key"] => $context["appointment"]) {
            // line 255
            echo "                                                    يوم
                                                    ";
            // line 256
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["appointment"], "appointment", array()), "d-m-Y"), "html", null, true);
            echo " على الساعة ";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["appointment"], "appointment", array()), "H"), "html", null, true);
            echo " و ";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["appointment"], "appointment", array()), "i"), "html", null, true);
            echo "  دقيقة
| <strong>حالة الموعد</strong> : ";
            // line 257
            echo twig_escape_filter($this->env, $this->getAttribute($context["appointment"], "status", array()), "html", null, true);
            echo "                                                    <br>
                                                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['appointment'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 259
        echo "                                            </div>
                                        </div>
                                    </li>
                                    ";
        // line 272
        echo "
                                </ul>


                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>

        <!-- /main content -->
        ";
        // line 289
        echo "    </div>
    <!-- /page-wrapper -->
";
        
        $__internal_25384984af376c8aeadbc04ad7f177fc427aa1dd1b18af95c9ed1ec6f69af924->leave($__internal_25384984af376c8aeadbc04ad7f177fc427aa1dd1b18af95c9ed1ec6f69af924_prof);

        
        $__internal_ba33369d74a2463eea11cc3d14894fa5cb0bc405100acc071e34df37c68e58ba->leave($__internal_ba33369d74a2463eea11cc3d14894fa5cb0bc405100acc071e34df37c68e58ba_prof);

    }

    // line 292
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_63b2b25396106e9c543a25057da052c453839808271aace1e831fffd96394db7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_63b2b25396106e9c543a25057da052c453839808271aace1e831fffd96394db7->enter($__internal_63b2b25396106e9c543a25057da052c453839808271aace1e831fffd96394db7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_6783d8ed8e424baed8d771719a6ade4362e444b13276710da69bfc5deadd86cf = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6783d8ed8e424baed8d771719a6ade4362e444b13276710da69bfc5deadd86cf->enter($__internal_6783d8ed8e424baed8d771719a6ade4362e444b13276710da69bfc5deadd86cf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 293
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    <script src=\"";
        // line 294
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("datatable/js/jquery.metisMenu.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 295
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("datatable/js/jquery-ui.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 296
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("datatable/js/jquery.blockUI.js"), "html", null, true);
        echo "\"></script>

    <!--Functions Js-->
    <script src=\"";
        // line 299
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("datatable/js/functions.js"), "html", null, true);
        echo "\"></script>

    <script src=\"";
        // line 301
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("datatable/js/loader.js"), "html", null, true);
        echo "\"></script>

    <script src=\"";
        // line 303
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("datatable/js/jquery.dataTables.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 304
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("datatable/js/dataTables.bootstrap.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 305
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("datatable/js/jszip.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 306
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("datatable/js/pdfmake.min.js"), "html", null, true);
        echo "\"></script>
    ";
        // line 308
        echo "    <script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("datatable/js/buttons.html5.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 309
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("datatable/js/buttons.colVis.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 310
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("datatable/js/dataTables-script.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 311
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("plugins/bower_components/bootstrap-rtl-master/dist/js/bootstrap-rtl.min.js"), "html", null, true);
        echo "\"></script>
";
        
        $__internal_6783d8ed8e424baed8d771719a6ade4362e444b13276710da69bfc5deadd86cf->leave($__internal_6783d8ed8e424baed8d771719a6ade4362e444b13276710da69bfc5deadd86cf_prof);

        
        $__internal_63b2b25396106e9c543a25057da052c453839808271aace1e831fffd96394db7->leave($__internal_63b2b25396106e9c543a25057da052c453839808271aace1e831fffd96394db7_prof);

    }

    public function getTemplateName()
    {
        return "DoctorsAdminBundle:Doctor:showDoctor.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  535 => 311,  531 => 310,  527 => 309,  522 => 308,  518 => 306,  514 => 305,  510 => 304,  506 => 303,  501 => 301,  496 => 299,  490 => 296,  486 => 295,  482 => 294,  477 => 293,  468 => 292,  456 => 289,  440 => 272,  435 => 259,  427 => 257,  419 => 256,  416 => 255,  412 => 254,  388 => 233,  368 => 216,  358 => 209,  354 => 208,  349 => 206,  345 => 205,  327 => 190,  314 => 180,  310 => 179,  297 => 169,  293 => 168,  280 => 158,  267 => 148,  254 => 138,  240 => 127,  227 => 117,  214 => 107,  203 => 98,  197 => 96,  193 => 94,  191 => 93,  178 => 83,  165 => 73,  152 => 63,  126 => 40,  115 => 31,  106 => 30,  94 => 27,  89 => 25,  84 => 23,  79 => 22,  76 => 20,  71 => 16,  67 => 13,  64 => 11,  59 => 7,  56 => 5,  51 => 3,  42 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"::layoutAdmin.html.twig\" %}
{% block stylesheets %}
    {{ parent() }}
    {#<link rel=\"stylesheet\" href=\"{{ asset('bootstrap/dist/css/bootstrap.min.css') }}\">#}
    <!-- Integral core stylesheet -->
    {#<link href=\"{{ asset('datatable/integral-core.css') }}\" rel=\"stylesheet\">#}
    <!-- /integral core stylesheet -->

    <!--Jvector Map-->
    {#<link href=\"{{ asset('datatable/jquery-jvectormap-2.0.3.css') }}\" rel=\"stylesheet\">#}

    {#<link href=\"{{ asset('datatable/integral-forms.css') }}\" rel=\"stylesheet\">#}

    <!-- Bootstrap RTL stylesheet min version -->
    {# <link href=\"{{ asset('datatable/bootstrap-rtl.min.css') }}\" rel=\"stylesheet\">#}
    <!-- /bootstrap rtl stylesheet min version -->

    <!-- Integral RTL core stylesheet -->
    {#<link href=\"{{ asset('datatable/integral-rtl-core.css') }}\" rel=\"stylesheet\">#}

    {#<link href=\"{{ asset('datatable/integral-forms.css') }}\" rel=\"stylesheet\">#}
    <link href=\"{{ asset('datatable/jquery.dataTables.css') }}\" rel=\"stylesheet\">
    <link href=\"{{ asset('datatable/buttons.dataTables.css') }}\" rel=\"stylesheet\">

    <link href=\"{{ asset('datatable/select2.css') }}\" rel=\"stylesheet\">

    <link href=\"{{ asset('datatable/bootstrap-toggle.min.css') }}\" rel=\"stylesheet\">
{% endblock %}

{% block body %}
    <!-- /main header -->

    <div class=\"preloader\">
        <div class=\"cssload-speeding-wheel\"></div>
    </div>
    <div id=\"page-wrapper\">
        <div class=\"container-fluid\">
            <div class=\"row bg-title\">
                <div class=\"col-lg-3 col-md-4 col-sm-4 col-xs-12\">
                    <h4 class=\"page-title arabe\">المعلومات التي تخص الطبيب \"<strong>{{ doctor.nameDoctor }}</strong>\"</h4>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <div class=\"row\">
                <div class=\"col-lg-12\">


                    <div class=\"panel panel-default\">
                        <div class=\"panel-heading clearfix\">
                            <h4 class=\"panel-title arabe\">عرض المعلومات</h4>
                        </div>


                        <div class=\"panel-body\">
                            <div class=\"table-responsive\">
                                <ul class=\"list-group\">
                                    <li class=\"list-group-item\">
                                        <div class=\"row\">
                                            <div class=\"col-lg-2\">
                                                <strong class=\"arabe\">اسم الطبيب </strong>
                                            </div>
                                            <div class=\"col-lg-4 arabe\">
                                                {{ doctor.nameDoctor }}
                                            </div>
                                        </div>
                                    </li>
                                    <li class=\"list-group-item\">
                                        <div class=\"row\">
                                            <div class=\"col-lg-2 arabe\">
                                                <strong>رقم التواصل  </strong>
                                            </div>
                                            <div class=\"col-lg-4 arabe\">
                                                {{ doctor.phoneNumberDoctor }}
                                            </div>
                                        </div>
                                    </li>
                                    <li class=\"list-group-item\">
                                        <div class=\"row\">
                                            <div class=\"col-lg-2 arabe\">
                                                <strong>تاريخ الاضافة  </strong>
                                            </div>
                                            <div class=\"col-lg-4 arabe\">
                                                {{ doctor.createdAtDoctor|date('d/m/Y') }}
                                            </div>
                                        </div>
                                    </li>
                                    <li class=\"list-group-item\">
                                        <div class=\"row\">
                                            <div class=\"col-lg-2 arabe\">
                                                <strong>الاختصاص </strong>
                                            </div>
                                            <div class=\"col-lg-4 arabe\">
                                                {% if  doctor.speciality.nameSpeciality is not defined  %}
                                                    طبيب عام
                                                {% else %}
                                                    {{ doctor.speciality.nameSpeciality }}
                                                {% endif %}
                                            </div>
                                        </div>
                                    </li>
                                    <li class=\"list-group-item\">
                                        <div class=\"row\">
                                            <div class=\"col-lg-2 arabe\">
                                                <strong>الجنسية </strong>
                                            </div>
                                            <div class=\"col-lg-4 arabe\">
                                                {{ doctor.nationality }}
                                            </div>
                                        </div>
                                    </li>
                                    <li class=\"list-group-item\">
                                        <div class=\"row\">
                                            <div class=\"col-lg-2 arabe\">
                                                <strong>البلد </strong>
                                            </div>
                                            <div class=\"col-lg-4 arabe\">
                                                {{ doctor.countryDoctor }}
                                            </div>
                                        </div>
                                    </li>
                                    <li class=\"list-group-item\">
                                        <div class=\"row\">
                                            <div class=\"col-lg-2 arabe\">
                                                <strong>المدينة </strong>
                                            </div>
                                            <div class=\"col-lg-4 arabe\">
                                                {{ doctor.townDoctor }}
                                            </div>
                                        </div>
                                    </li>

                                    <li class=\"list-group-item\">
                                        <div class=\"row\">
                                            <div class=\"col-lg-2 arabe\">
                                                <strong>الحي </strong>
                                            </div>
                                            <div class=\"col-lg-4 arabe\">
                                                {{ doctor.cityDoctor }}
                                            </div>
                                        </div>
                                    </li>
                                    <li class=\"list-group-item\">
                                        <div class=\"row\">
                                            <div class=\"col-lg-2 arabe\">
                                                <strong>مكان العمل </strong>
                                            </div>
                                            <div class=\"col-lg-4 arabe\">
                                                {{ doctor.workplace }}
                                            </div>
                                        </div>
                                    </li>
                                    <li class=\"list-group-item\">
                                        <div class=\"row\">
                                            <div class=\"col-lg-2 arabe\">
                                                <strong>اسم جهة العمل </strong>
                                            </div>
                                            <div class=\"col-lg-4 arabe\">
                                                {{ doctor.workplaceName }}
                                            </div>
                                        </div>
                                    </li>
                                    <li class=\"list-group-item\">
                                        <div class=\"row\">
                                            <div class=\"col-lg-2 arabe\">
                                                <strong>بداية الدوام </strong>
                                            </div>
                                            <div class=\"col-lg-4 arabe\">
                                                على الساعة{{ doctor.startTimeWork|date('H') }}  و
                                                {{ doctor.startTimeWork|date('i') }}  دقيقة
                                            </div>
                                        </div>
                                    </li>
                                    <li class=\"list-group-item\">
                                        <div class=\"row\">
                                            <div class=\"col-lg-2 arabe\">
                                                <strong>نهاية الدوام </strong>
                                            </div>
                                            <div class=\"col-lg-4 arabe\">
                                                على الساعة{{ doctor.endTimeWork|date('H') }} و
                                                {{ doctor.endTimeWork|date('i') }}    دقيقة
                                            </div>
                                        </div>
                                    </li>
                                    <li class=\"list-group-item\">
                                        <div class=\"row\">
                                            <div class=\"col-lg-2 arabe\">
                                                <strong>الايميل </strong>
                                            </div>
                                            <div class=\"col-lg-4 arabe\">
                                                {{ doctor.emailDoctor }}
                                            </div>
                                        </div>
                                    </li>

                                    <li class=\"list-group-item\">
                                        <div class=\"row\">
                                            <div class=\"col-lg-2 arabe\">
                                                <strong class=\"arabe\">الصورة</strong>
                                            </div>
                                            <div class=\"col-lg-10\">
                                                <div class=\"row\">


                                                    <div class=\"col-lg-3\">
                                                        <a href=\"#\" id=\"show-{{ doctor.id }}\" data-toggle=\"modal\"
                                                           data-target=\"#show-modal-{{ doctor.id }}\">

                                                            <img src=\"{{ asset('uploads/doctors/'~doctor.photoDoctor) }}\"
                                                                 width=\"180\" height=\"150\" id=\"small-img-{{ doctor.id }}\" class=\"thumbnail\">

                                                        </a>

                                                    </div>


                                                    <div class=\"modal fade\" id=\"show-modal-{{ doctor.id }}\" tabindex=\"-1\"
                                                         role=\"dialog\" aria-labelledby=\"myModalLabel\"
                                                         aria-hidden=\"true\">
                                                        <div class=\"modal-dialog\">
                                                            <div class=\"modal-content\">

                                                                <div class=\"modal-header\">
                                                                    <button type=\"button\" class=\"close\"
                                                                            data-dismiss=\"modal\" aria-hidden=\"true\">
                                                                        &times;
                                                                    </button>
                                                                    <h4 class=\"modal-title arabe\" id=\"myModalLabel\"> اظهار
                                                                        الصورة</h4>
                                                                </div>

                                                                <div class=\"modal-body\">
                                                                    <center>
                                                                        <img src=\"{{ asset('uploads/doctors/'~doctor.photoDoctor) }}\"
                                                                             width=\"500\" height=\"300\" id=\"large-img-7\">
                                                                    </center>
                                                                </div>

                                                                <div class=\"modal-footer\">
                                                                    <button type=\"button\" class=\"btn btn-default arabe\"
                                                                            data-dismiss=\"modal\"> رجوع
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class=\"list-group-item\">
                                        <div class=\"row\">
                                            <div class=\"col-lg-2 arabe\"><strong> قائمة المواعيد  </strong></div>
                                            <div class=\"col-lg-10\">
                                                {% for appointment in appointments %}
                                                    يوم
                                                    {{ appointment.appointment|date('d-m-Y') }} على الساعة {{ appointment.appointment|date('H') }} و {{ appointment.appointment|date('i')  }}  دقيقة
| <strong>حالة الموعد</strong> : {{ appointment.status }}                                                    <br>
                                                {% endfor %}
                                            </div>
                                        </div>
                                    </li>
                                    {#<li class=\"list-group-item\">
                                        <div class=\"row\">
                                            <div class=\"col-lg-2 arabe\">
                                                <strong> الكود الذي تم ارساله على الجوال </strong>
                                            </div>
                                            <div class=\"col-lg-10\">
                                                {{ doctor.codeD }}
                                            </div>
                                        </div>
                                    </li>#}

                                </ul>


                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>

        <!-- /main content -->
        {#{% block footer %}
            {{ parent() }}
        {% endblock %}#}
    </div>
    <!-- /page-wrapper -->
{% endblock %}
{% block javascripts %}
    {{ parent() }}
    <script src=\"{{ asset('datatable/js/jquery.metisMenu.js') }}\"></script>
    <script src=\"{{ asset('datatable/js/jquery-ui.js') }}\"></script>
    <script src=\"{{ asset('datatable/js/jquery.blockUI.js') }}\"></script>

    <!--Functions Js-->
    <script src=\"{{ asset('datatable/js/functions.js') }}\"></script>

    <script src=\"{{ asset('datatable/js/loader.js') }}\"></script>

    <script src=\"{{ asset('datatable/js/jquery.dataTables.min.js') }}\"></script>
    <script src=\"{{ asset('datatable/js/dataTables.bootstrap.min.js') }}\"></script>
    <script src=\"{{ asset('datatable/js/jszip.min.js') }}\"></script>
    <script src=\"{{ asset('datatable/js/pdfmake.min.js') }}\"></script>
    {#<script src=\"{{ asset('datatable/js/vfs_fonts.js') }}\"></script>#}
    <script src=\"{{ asset('datatable/js/buttons.html5.js') }}\"></script>
    <script src=\"{{ asset('datatable/js/buttons.colVis.js') }}\"></script>
    <script src=\"{{ asset('datatable/js/dataTables-script.js') }}\"></script>
    <script src=\"{{ asset('plugins/bower_components/bootstrap-rtl-master/dist/js/bootstrap-rtl.min.js') }}\"></script>
{% endblock %}
", "DoctorsAdminBundle:Doctor:showDoctor.html.twig", "C:\\wamp64\\www\\doctorAdmin\\src\\Doctors\\AdminBundle/Resources/views/Doctor/showDoctor.html.twig");
    }
}
