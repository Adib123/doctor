<?php

/* DoctorsAdminBundle:Appointment:appointments.html.twig */
class __TwigTemplate_288185a5110cdbfd48d4a4c53c73b9b51fb1874153a74d5d170b4da61c0aa470 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::layoutAdmin.html.twig", "DoctorsAdminBundle:Appointment:appointments.html.twig", 1);
        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::layoutAdmin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4c92513853aac58efd5b9151b13d4343ef6452d562772ba992c41652225f953b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4c92513853aac58efd5b9151b13d4343ef6452d562772ba992c41652225f953b->enter($__internal_4c92513853aac58efd5b9151b13d4343ef6452d562772ba992c41652225f953b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "DoctorsAdminBundle:Appointment:appointments.html.twig"));

        $__internal_1260732e5ed3c66f45b86c0c89d99c09ddf079952abcb296dd0a0f74b10b8a93 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1260732e5ed3c66f45b86c0c89d99c09ddf079952abcb296dd0a0f74b10b8a93->enter($__internal_1260732e5ed3c66f45b86c0c89d99c09ddf079952abcb296dd0a0f74b10b8a93_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "DoctorsAdminBundle:Appointment:appointments.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_4c92513853aac58efd5b9151b13d4343ef6452d562772ba992c41652225f953b->leave($__internal_4c92513853aac58efd5b9151b13d4343ef6452d562772ba992c41652225f953b_prof);

        
        $__internal_1260732e5ed3c66f45b86c0c89d99c09ddf079952abcb296dd0a0f74b10b8a93->leave($__internal_1260732e5ed3c66f45b86c0c89d99c09ddf079952abcb296dd0a0f74b10b8a93_prof);

    }

    // line 2
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_8c33598f242272d1a300aaa3cffc8045194687b51421590d8ab6bde5451cef5b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8c33598f242272d1a300aaa3cffc8045194687b51421590d8ab6bde5451cef5b->enter($__internal_8c33598f242272d1a300aaa3cffc8045194687b51421590d8ab6bde5451cef5b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_d43afa916ea163ec41d3bdcd6a7a3df378374c879e5fc36565f7951a1f78ba03 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d43afa916ea163ec41d3bdcd6a7a3df378374c879e5fc36565f7951a1f78ba03->enter($__internal_d43afa916ea163ec41d3bdcd6a7a3df378374c879e5fc36565f7951a1f78ba03_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 3
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    ";
        // line 5
        echo "    <!-- Integral core stylesheet -->
    ";
        // line 7
        echo "    <!-- /integral core stylesheet -->

    <!--Jvector Map-->
    ";
        // line 11
        echo "
    ";
        // line 13
        echo "
    <!-- Bootstrap RTL stylesheet min version -->
    ";
        // line 16
        echo "    <!-- /bootstrap rtl stylesheet min version -->

    <!-- Integral RTL core stylesheet -->
    ";
        // line 20
        echo "
    ";
        // line 22
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("datatable/jquery.dataTables.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
    <link href=\"";
        // line 23
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("datatable/buttons.dataTables.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">

    <link href=\"";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("datatable/select2.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">

    <link href=\"";
        // line 27
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("datatable/bootstrap-toggle.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
";
        
        $__internal_d43afa916ea163ec41d3bdcd6a7a3df378374c879e5fc36565f7951a1f78ba03->leave($__internal_d43afa916ea163ec41d3bdcd6a7a3df378374c879e5fc36565f7951a1f78ba03_prof);

        
        $__internal_8c33598f242272d1a300aaa3cffc8045194687b51421590d8ab6bde5451cef5b->leave($__internal_8c33598f242272d1a300aaa3cffc8045194687b51421590d8ab6bde5451cef5b_prof);

    }

    // line 30
    public function block_body($context, array $blocks = array())
    {
        $__internal_f1e18a69a5f3ed2c900804547559f9c4061511300fc4d8072bcdf04179ca9499 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f1e18a69a5f3ed2c900804547559f9c4061511300fc4d8072bcdf04179ca9499->enter($__internal_f1e18a69a5f3ed2c900804547559f9c4061511300fc4d8072bcdf04179ca9499_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_70be501e2e68a0d51e77e0186410556b6f361cd880f9c4e1269c9ef1ab8dba66 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_70be501e2e68a0d51e77e0186410556b6f361cd880f9c4e1269c9ef1ab8dba66->enter($__internal_70be501e2e68a0d51e77e0186410556b6f361cd880f9c4e1269c9ef1ab8dba66_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 31
        echo "<!-- /main header -->

<div class=\"preloader\">
    <div class=\"cssload-speeding-wheel\"></div>
</div>
<div id=\"page-wrapper\">
    <div class=\"container-fluid\">
        <div class=\"row bg-title\">
            <div class=\"col-lg-3 col-md-4 col-sm-4 col-xs-12\">
                <h4 class=\"page-title arabe\">المواعيد</h4>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <div class=\"row\">
            <div class=\"col-lg-12\">


                <div class=\"panel panel-default\">
                    <div class=\"panel-heading clearfix\">
                        <h4 class=\"panel-title arabe\">قائمة المواعيد</h4>
                    </div>


                    <div class=\"panel-body\">
                        ";
        // line 55
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashbag", array()), "get", array(0 => "msg"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 56
            echo "                            <br><br>
                            <div class='alert alert-success alert-dismissable arabe'>
                                <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;
                                </button>
                                ";
            // line 60
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "
                            </div>

                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 64
        echo "                        <table class=\"table table-striped table-bordered table-hover dataTables-example\">
                            <thead>
                            <tr>
                                <th class=\"arabe\">تاريخ الموعد</th>
                                <th class=\"arabe\">الحالة</th>
                                <th class=\"arabe\">الطبيب</th>
                                <th class=\"arabe\">المستخدم</th>
                                <th class=\"arabe\">عرض المعلومات</th>
                                <th class=\"arabe\">تعديل</th>
                                <th class=\"arabe\">حذف</th>
                            </tr>
                            </thead>
                            <tbody>
                            ";
        // line 77
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["appointments"]) ? $context["appointments"] : $this->getContext($context, "appointments")));
        foreach ($context['_seq'] as $context["_key"] => $context["appointment"]) {
            // line 78
            echo "                            ";
            if (($this->getAttribute($context["appointment"], "isActiveAppointment", array()) == 1)) {
                // line 79
                echo "                            <tr class=\"gradeX\">
                                <td class=\"arabe\">يوم
                                    ";
                // line 81
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["appointment"], "appointment", array()), "d/m/Y"), "html", null, true);
                echo " على الساعة
                                    ";
                // line 82
                if ((twig_date_format_filter($this->env, $this->getAttribute($context["appointment"], "appointment", array()), "i") == 2)) {
                    // line 83
                    echo "                                        و دقيقاتان
                                    ";
                } elseif ((twig_date_format_filter($this->env, $this->getAttribute(                // line 84
$context["appointment"], "appointment", array()), "i") > 10)) {
                    // line 85
                    echo "                                        ";
                    echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["appointment"], "appointment", array()), "H و i دقيقة"), "html", null, true);
                    echo "
                                    ";
                } elseif ((twig_date_format_filter($this->env, $this->getAttribute(                // line 86
$context["appointment"], "appointment", array()), "i") < 2)) {
                    // line 87
                    echo "                                        ";
                    echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["appointment"], "appointment", array()), "H و i دقيقة"), "html", null, true);
                    echo "
                                    ";
                } else {
                    // line 89
                    echo "                                        ";
                    echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["appointment"], "appointment", array()), "H و i دقائق"), "html", null, true);
                    echo "
                                    ";
                }
                // line 91
                echo "                                </td>
                                <td class=\"arabe\">


                                    ";
                // line 95
                if (($this->getAttribute($context["appointment"], "status", array()) == "مؤكد")) {
                    // line 96
                    echo "                                        <span class=\"label label-success\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["appointment"], "status", array()), "html", null, true);
                    echo "</span>
                                    ";
                } elseif (($this->getAttribute(                // line 97
$context["appointment"], "status", array()) == "معلق")) {
                    // line 98
                    echo "                                        <span class=\"label label-info\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["appointment"], "status", array()), "html", null, true);
                    echo " </span>
                                    ";
                } elseif (($this->getAttribute(                // line 99
$context["appointment"], "status", array()) == "منته")) {
                    // line 100
                    echo "                                        <span class=\"label label-primary\"> ";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["appointment"], "status", array()), "html", null, true);
                    echo " </span>
                                    ";
                } elseif (($this->getAttribute(                // line 101
$context["appointment"], "status", array()) == "مرفوض")) {
                    // line 102
                    echo "                                        <span class=\"label label-danger\"> ";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["appointment"], "status", array()), "html", null, true);
                    echo " </span>
                                    ";
                }
                // line 104
                echo "                                </td>
                                <td class=\"arabe\">";
                // line 105
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["appointment"], "doctor", array()), "nameDoctor", array()), "html", null, true);
                echo "</td>
                                <td class=\"arabe\">";
                // line 106
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["appointment"], "user", array()), "name", array()), "html", null, true);
                echo "</td>
                                <td align=\"center\">
                                    <a href=\"";
                // line 108
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("doctors_admin_showAppointment", array("id" => $this->getAttribute($context["appointment"], "id", array()))), "html", null, true);
                echo "\"
                                       class=\"btn btn-info\">
                                        <i class=\"fa fa-search\"></i>
                                    </a>
                                </td>
                                <td align=\"center\">
                                    <a href=\"";
                // line 114
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("doctors_admin_editAppointment", array("id" => $this->getAttribute($context["appointment"], "id", array()))), "html", null, true);
                echo "\"
                                       class=\"btn btn-primary\">
                                        <i class=\"fa fa-refresh\"></i>
                                    </a>
                                </td>
                                <td align=\"center\">
                                    <a href=\"#\" id=\"remove-customer-";
                // line 120
                echo twig_escape_filter($this->env, $this->getAttribute($context["appointment"], "id", array()), "html", null, true);
                echo "\"
                                       class=\"btn btn-danger\" data-toggle=\"modal\"
                                       data-target=\"#confirm-delete-";
                // line 122
                echo twig_escape_filter($this->env, $this->getAttribute($context["appointment"], "id", array()), "html", null, true);
                echo "\"><i
                                                class=\"fa fa-remove\"></i> </a>

                                </td>

                                <!-- Delete Modal -->
















                                <div class=\"modal fade\" id=\"confirm-delete-";
                // line 144
                echo twig_escape_filter($this->env, $this->getAttribute($context["appointment"], "id", array()), "html", null, true);
                echo "\" tabindex=\"-1\" role=\"dialog\"
                                     aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
                                    <div class=\"modal-dialog\">
                                        <div class=\"modal-content\">

                                            <div class=\"modal-header\">
                                                <button type=\"button\" class=\"close\" data-dismiss=\"modal\"
                                                        aria-hidden=\"true\">&times;
                                                </button>
                                                <h4 class=\"modal-title arabe\" id=\"myModalLabel\"> تاكيد الحذف </h4>
                                            </div>

                                            <div class=\"modal-body\">
                                                <p class=\"arabe\">
                                                    هل تريد حذف الموعد :
                                                    <br><br>
                                                    <span style=\"border: 1px dashed black; padding: 3px\">
                                                        <strong class=\"arabe\">
                                                            الموعد :
                                                        </strong>
                                                        ";
                // line 164
                echo twig_escape_filter($this->env, $this->getAttribute($context["appointment"], "status", array()), "html", null, true);
                echo " </span><br><br>
                                                    <strong class=\"arabe\">

                                                        تاريخ الموعد :
                                                    </strong>
                                                    ";
                // line 169
                echo twig_escape_filter($this->env, ((((((" اليوم : " . twig_date_format_filter($this->env, $this->getAttribute($context["appointment"], "appointment", array()), "d/m/Y")) . " الساعة : ") . twig_date_format_filter($this->env, $this->getAttribute($context["appointment"], "appointment", array()), "h")) . " و : ") . twig_date_format_filter($this->env, $this->getAttribute($context["appointment"], "appointment", array()), "I")) . " دقيقة "), "html", null, true);
                echo " <br>

                                                    <strong>

                                                        اسم الطبيب :
                                                    </strong>
                                                    ";
                // line 175
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["appointment"], "doctor", array()), "nameDoctor", array()), "html", null, true);
                echo " <br>
                                                    <strong>
                                                        اسم المستخدم :
                                                    </strong>
                                                    ";
                // line 179
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["appointment"], "user", array()), "name", array()), "html", null, true);
                echo "

                                                </p>
                                            </div>

                                            <div class=\"modal-footer\">
                                                <button type=\"button\" class=\"btn btn-default arabe\"
                                                        data-dismiss=\"modal\">
                                                    رجوع
                                                </button>
                                                <a href=\"";
                // line 189
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("doctors_admin_deleteAppointment", array("id" => $this->getAttribute($context["appointment"], "id", array()))), "html", null, true);
                echo "\"
                                                   class=\"btn btn-danger arabe\" id=\"btn-remove\"> حذف</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

















                                ";
                // line 247
                echo "
                            </tr>
                            ";
            }
            // line 250
            echo "                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['appointment'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 251
        echo "                            </tbody>
                            <tfoot>
                            <tr>
                                <th class=\"arabe\">تاريخ الموعد</th>
                                <th class=\"arabe\">الحالة</th>
                                <th class=\"arabe\">الطبيب</th>
                                <th class=\"arabe\">المستخدم</th>
                                <th class=\"arabe\">تعديل</th>
                                <th class=\"arabe\">حذف</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>

            </div>

        </div>


    </div>

    <!-- /main content -->
    ";
        // line 277
        echo "</div>
<!-- /page-wrapper -->
";
        
        $__internal_70be501e2e68a0d51e77e0186410556b6f361cd880f9c4e1269c9ef1ab8dba66->leave($__internal_70be501e2e68a0d51e77e0186410556b6f361cd880f9c4e1269c9ef1ab8dba66_prof);

        
        $__internal_f1e18a69a5f3ed2c900804547559f9c4061511300fc4d8072bcdf04179ca9499->leave($__internal_f1e18a69a5f3ed2c900804547559f9c4061511300fc4d8072bcdf04179ca9499_prof);

    }

    // line 280
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_ba16c45dc6ccd87ae5a41115fdbd9411c592a7cdbc79c9f22f5b8f96a48d68cc = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ba16c45dc6ccd87ae5a41115fdbd9411c592a7cdbc79c9f22f5b8f96a48d68cc->enter($__internal_ba16c45dc6ccd87ae5a41115fdbd9411c592a7cdbc79c9f22f5b8f96a48d68cc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_eac92a12ea4d4e58143b4808fd658798e33847993d29dc1695c30d3b26063ddf = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_eac92a12ea4d4e58143b4808fd658798e33847993d29dc1695c30d3b26063ddf->enter($__internal_eac92a12ea4d4e58143b4808fd658798e33847993d29dc1695c30d3b26063ddf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 281
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    <script src=\"";
        // line 282
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("datatable/js/jquery.metisMenu.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 283
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("datatable/js/jquery-ui.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 284
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("datatable/js/jquery.blockUI.js"), "html", null, true);
        echo "\"></script>

    <!--Functions Js-->
    <script src=\"";
        // line 287
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("datatable/js/functions.js"), "html", null, true);
        echo "\"></script>

    <script src=\"";
        // line 289
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("datatable/js/loader.js"), "html", null, true);
        echo "\"></script>

    <script src=\"";
        // line 291
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("datatable/js/jquery.dataTables.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 292
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("datatable/js/dataTables.bootstrap.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 293
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("datatable/js/jszip.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 294
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("datatable/js/pdfmake.min.js"), "html", null, true);
        echo "\"></script>
    ";
        // line 296
        echo "    <script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("datatable/js/buttons.html5.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 297
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("datatable/js/buttons.colVis.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 298
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("datatable/js/dataTables-script.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 299
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("plugins/bower_components/bootstrap-rtl-master/dist/js/bootstrap-rtl.min.js"), "html", null, true);
        echo "\"></script>
";
        
        $__internal_eac92a12ea4d4e58143b4808fd658798e33847993d29dc1695c30d3b26063ddf->leave($__internal_eac92a12ea4d4e58143b4808fd658798e33847993d29dc1695c30d3b26063ddf_prof);

        
        $__internal_ba16c45dc6ccd87ae5a41115fdbd9411c592a7cdbc79c9f22f5b8f96a48d68cc->leave($__internal_ba16c45dc6ccd87ae5a41115fdbd9411c592a7cdbc79c9f22f5b8f96a48d68cc_prof);

    }

    public function getTemplateName()
    {
        return "DoctorsAdminBundle:Appointment:appointments.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  513 => 299,  509 => 298,  505 => 297,  500 => 296,  496 => 294,  492 => 293,  488 => 292,  484 => 291,  479 => 289,  474 => 287,  468 => 284,  464 => 283,  460 => 282,  455 => 281,  446 => 280,  434 => 277,  409 => 251,  403 => 250,  398 => 247,  372 => 189,  359 => 179,  352 => 175,  343 => 169,  335 => 164,  312 => 144,  287 => 122,  282 => 120,  273 => 114,  264 => 108,  259 => 106,  255 => 105,  252 => 104,  246 => 102,  244 => 101,  239 => 100,  237 => 99,  232 => 98,  230 => 97,  225 => 96,  223 => 95,  217 => 91,  211 => 89,  205 => 87,  203 => 86,  198 => 85,  196 => 84,  193 => 83,  191 => 82,  187 => 81,  183 => 79,  180 => 78,  176 => 77,  161 => 64,  151 => 60,  145 => 56,  141 => 55,  115 => 31,  106 => 30,  94 => 27,  89 => 25,  84 => 23,  79 => 22,  76 => 20,  71 => 16,  67 => 13,  64 => 11,  59 => 7,  56 => 5,  51 => 3,  42 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"::layoutAdmin.html.twig\" %}
{% block stylesheets %}
    {{ parent() }}
    {#<link rel=\"stylesheet\" href=\"{{ asset('bootstrap/dist/css/bootstrap.min.css') }}\">#}
    <!-- Integral core stylesheet -->
    {#<link href=\"{{ asset('datatable/integral-core.css') }}\" rel=\"stylesheet\">#}
    <!-- /integral core stylesheet -->

    <!--Jvector Map-->
    {#<link href=\"{{ asset('datatable/jquery-jvectormap-2.0.3.css') }}\" rel=\"stylesheet\">#}

    {#<link href=\"{{ asset('datatable/integral-forms.css') }}\" rel=\"stylesheet\">#}

    <!-- Bootstrap RTL stylesheet min version -->
    {# <link href=\"{{ asset('datatable/bootstrap-rtl.min.css') }}\" rel=\"stylesheet\">#}
    <!-- /bootstrap rtl stylesheet min version -->

    <!-- Integral RTL core stylesheet -->
    {#<link href=\"{{ asset('datatable/integral-rtl-core.css') }}\" rel=\"stylesheet\">#}

    {#<link href=\"{{ asset('datatable/integral-forms.css') }}\" rel=\"stylesheet\">#}
    <link href=\"{{ asset('datatable/jquery.dataTables.css') }}\" rel=\"stylesheet\">
    <link href=\"{{ asset('datatable/buttons.dataTables.css') }}\" rel=\"stylesheet\">

    <link href=\"{{ asset('datatable/select2.css') }}\" rel=\"stylesheet\">

    <link href=\"{{ asset('datatable/bootstrap-toggle.min.css') }}\" rel=\"stylesheet\">
{% endblock %}

{% block body %}
<!-- /main header -->

<div class=\"preloader\">
    <div class=\"cssload-speeding-wheel\"></div>
</div>
<div id=\"page-wrapper\">
    <div class=\"container-fluid\">
        <div class=\"row bg-title\">
            <div class=\"col-lg-3 col-md-4 col-sm-4 col-xs-12\">
                <h4 class=\"page-title arabe\">المواعيد</h4>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <div class=\"row\">
            <div class=\"col-lg-12\">


                <div class=\"panel panel-default\">
                    <div class=\"panel-heading clearfix\">
                        <h4 class=\"panel-title arabe\">قائمة المواعيد</h4>
                    </div>


                    <div class=\"panel-body\">
                        {% for flashMessage in app.session.flashbag.get('msg') %}
                            <br><br>
                            <div class='alert alert-success alert-dismissable arabe'>
                                <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;
                                </button>
                                {{ flashMessage }}
                            </div>

                        {% endfor %}
                        <table class=\"table table-striped table-bordered table-hover dataTables-example\">
                            <thead>
                            <tr>
                                <th class=\"arabe\">تاريخ الموعد</th>
                                <th class=\"arabe\">الحالة</th>
                                <th class=\"arabe\">الطبيب</th>
                                <th class=\"arabe\">المستخدم</th>
                                <th class=\"arabe\">عرض المعلومات</th>
                                <th class=\"arabe\">تعديل</th>
                                <th class=\"arabe\">حذف</th>
                            </tr>
                            </thead>
                            <tbody>
                            {% for appointment in appointments %}
                            {% if appointment.isActiveAppointment == 1 %}
                            <tr class=\"gradeX\">
                                <td class=\"arabe\">يوم
                                    {{ appointment.appointment|date('d/m/Y') }} على الساعة
                                    {% if appointment.appointment|date('i') == 02 %}
                                        و دقيقاتان
                                    {% elseif appointment.appointment|date('i') > 10 %}
                                        {{ appointment.appointment|date('H و i دقيقة') }}
                                    {% elseif appointment.appointment|date('i') < 2 %}
                                        {{ appointment.appointment|date('H و i دقيقة') }}
                                    {% else %}
                                        {{ appointment.appointment|date('H و i دقائق') }}
                                    {% endif %}
                                </td>
                                <td class=\"arabe\">


                                    {% if appointment.status == 'مؤكد' %}
                                        <span class=\"label label-success\">{{ appointment.status }}</span>
                                    {% elseif appointment.status == 'معلق' %}
                                        <span class=\"label label-info\">{{ appointment.status }} </span>
                                    {% elseif appointment.status == 'منته' %}
                                        <span class=\"label label-primary\"> {{ appointment.status }} </span>
                                    {% elseif appointment.status == 'مرفوض' %}
                                        <span class=\"label label-danger\"> {{ appointment.status }} </span>
                                    {% endif %}
                                </td>
                                <td class=\"arabe\">{{ appointment.doctor.nameDoctor }}</td>
                                <td class=\"arabe\">{{ appointment.user.name }}</td>
                                <td align=\"center\">
                                    <a href=\"{{ path('doctors_admin_showAppointment', {'id':appointment.id}) }}\"
                                       class=\"btn btn-info\">
                                        <i class=\"fa fa-search\"></i>
                                    </a>
                                </td>
                                <td align=\"center\">
                                    <a href=\"{{ path('doctors_admin_editAppointment', {'id':appointment.id}) }}\"
                                       class=\"btn btn-primary\">
                                        <i class=\"fa fa-refresh\"></i>
                                    </a>
                                </td>
                                <td align=\"center\">
                                    <a href=\"#\" id=\"remove-customer-{{ appointment.id }}\"
                                       class=\"btn btn-danger\" data-toggle=\"modal\"
                                       data-target=\"#confirm-delete-{{ appointment.id }}\"><i
                                                class=\"fa fa-remove\"></i> </a>

                                </td>

                                <!-- Delete Modal -->
















                                <div class=\"modal fade\" id=\"confirm-delete-{{ appointment.id }}\" tabindex=\"-1\" role=\"dialog\"
                                     aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
                                    <div class=\"modal-dialog\">
                                        <div class=\"modal-content\">

                                            <div class=\"modal-header\">
                                                <button type=\"button\" class=\"close\" data-dismiss=\"modal\"
                                                        aria-hidden=\"true\">&times;
                                                </button>
                                                <h4 class=\"modal-title arabe\" id=\"myModalLabel\"> تاكيد الحذف </h4>
                                            </div>

                                            <div class=\"modal-body\">
                                                <p class=\"arabe\">
                                                    هل تريد حذف الموعد :
                                                    <br><br>
                                                    <span style=\"border: 1px dashed black; padding: 3px\">
                                                        <strong class=\"arabe\">
                                                            الموعد :
                                                        </strong>
                                                        {{ appointment.status }} </span><br><br>
                                                    <strong class=\"arabe\">

                                                        تاريخ الموعد :
                                                    </strong>
                                                    {{ \" اليوم : \" ~ appointment.appointment|date('d/m/Y') ~  \" الساعة : \" ~ appointment.appointment|date('h') ~ \" و : \" ~ appointment.appointment|date('I') ~ \" دقيقة \" }} <br>

                                                    <strong>

                                                        اسم الطبيب :
                                                    </strong>
                                                    {{ appointment.doctor.nameDoctor }} <br>
                                                    <strong>
                                                        اسم المستخدم :
                                                    </strong>
                                                    {{ appointment.user.name }}

                                                </p>
                                            </div>

                                            <div class=\"modal-footer\">
                                                <button type=\"button\" class=\"btn btn-default arabe\"
                                                        data-dismiss=\"modal\">
                                                    رجوع
                                                </button>
                                                <a href=\"{{ path('doctors_admin_deleteAppointment', {'id':appointment.id}) }}\"
                                                   class=\"btn btn-danger arabe\" id=\"btn-remove\"> حذف</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

















                                {#<div class=\"modal fade\" id=\"confirm-delete-{{ appointment.id }}\"
                                     tabindex=\"-1\" role=\"dialog\"
                                     aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
                                    <div class=\"modal-dialog\">
                                        <div class=\"modal-content\">

                                            <div class=\"modal-header\">
                                                <button type=\"button\" class=\"close\" data-dismiss=\"modal\"
                                                        aria-hidden=\"true\">&times;
                                                </button>
                                                <h4 class=\"modal-title arabe\" id=\"myModalLabel\"> تاكيد الحذف </h4>
                                            </div>

                                            <div class=\"modal-body\">
                                                <p class=\"arabe\">
                                                    هل تريد حذف الموعد :
                                                    <br>
                                                    <strong class=\"arabe\">
                                                        {{ appointment.status }} للمستخدم {{ appointment.user.name }} و
                                                        الطبيب {{ appointment.doctor.nameDoctor }}
                                                    </strong>
                                                </p>
                                            </div>

                                            <div class=\"modal-footer\">
                                                <button type=\"button\" class=\"btn btn-default arabe\"
                                                        data-dismiss=\"modal\">
                                                    رجوع
                                                </button>
                                                <a href=\"{{ path('doctors_admin_deleteAppointment', {'id':appointment.id}) }}\"
                                                   class=\"btn btn-danger arabe\" id=\"btn-remove\"> حذف</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>#}

                            </tr>
                            {% endif %}
                            {% endfor %}
                            </tbody>
                            <tfoot>
                            <tr>
                                <th class=\"arabe\">تاريخ الموعد</th>
                                <th class=\"arabe\">الحالة</th>
                                <th class=\"arabe\">الطبيب</th>
                                <th class=\"arabe\">المستخدم</th>
                                <th class=\"arabe\">تعديل</th>
                                <th class=\"arabe\">حذف</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>

            </div>

        </div>


    </div>

    <!-- /main content -->
    {#{% block footer %}
        {{ parent() }}
    {% endblock %}#}
</div>
<!-- /page-wrapper -->
{% endblock %}
{% block javascripts %}
    {{ parent() }}
    <script src=\"{{ asset('datatable/js/jquery.metisMenu.js') }}\"></script>
    <script src=\"{{ asset('datatable/js/jquery-ui.js') }}\"></script>
    <script src=\"{{ asset('datatable/js/jquery.blockUI.js') }}\"></script>

    <!--Functions Js-->
    <script src=\"{{ asset('datatable/js/functions.js') }}\"></script>

    <script src=\"{{ asset('datatable/js/loader.js') }}\"></script>

    <script src=\"{{ asset('datatable/js/jquery.dataTables.min.js') }}\"></script>
    <script src=\"{{ asset('datatable/js/dataTables.bootstrap.min.js') }}\"></script>
    <script src=\"{{ asset('datatable/js/jszip.min.js') }}\"></script>
    <script src=\"{{ asset('datatable/js/pdfmake.min.js') }}\"></script>
    {#<script src=\"{{ asset('datatable/js/vfs_fonts.js') }}\"></script>#}
    <script src=\"{{ asset('datatable/js/buttons.html5.js') }}\"></script>
    <script src=\"{{ asset('datatable/js/buttons.colVis.js') }}\"></script>
    <script src=\"{{ asset('datatable/js/dataTables-script.js') }}\"></script>
    <script src=\"{{ asset('plugins/bower_components/bootstrap-rtl-master/dist/js/bootstrap-rtl.min.js') }}\"></script>
{% endblock %}
", "DoctorsAdminBundle:Appointment:appointments.html.twig", "C:\\wamp64\\www\\doctorAdmin\\src\\Doctors\\AdminBundle/Resources/views/Appointment/appointments.html.twig");
    }
}
