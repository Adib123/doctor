<?php

/* DoctorsAdminBundle:Appointment:ajouterAppointment.html.twig */
class __TwigTemplate_4c2b0a8f625023726aee34cf17b442e3b3d74b4d11f4f59752d0d50d79dc0435 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::layoutAdmin.html.twig", "DoctorsAdminBundle:Appointment:ajouterAppointment.html.twig", 1);
        $this->blocks = array(
            'stylesh' => array($this, 'block_stylesh'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::layoutAdmin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1580c0de4b935825165e6a45b78f9fc54938e7554a0345865d4423cf16f48d84 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1580c0de4b935825165e6a45b78f9fc54938e7554a0345865d4423cf16f48d84->enter($__internal_1580c0de4b935825165e6a45b78f9fc54938e7554a0345865d4423cf16f48d84_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "DoctorsAdminBundle:Appointment:ajouterAppointment.html.twig"));

        $__internal_9bd9cf00718d895cd6cd44cb9c0d6359f4857fd7005fade16908e68c630f40e3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9bd9cf00718d895cd6cd44cb9c0d6359f4857fd7005fade16908e68c630f40e3->enter($__internal_9bd9cf00718d895cd6cd44cb9c0d6359f4857fd7005fade16908e68c630f40e3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "DoctorsAdminBundle:Appointment:ajouterAppointment.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_1580c0de4b935825165e6a45b78f9fc54938e7554a0345865d4423cf16f48d84->leave($__internal_1580c0de4b935825165e6a45b78f9fc54938e7554a0345865d4423cf16f48d84_prof);

        
        $__internal_9bd9cf00718d895cd6cd44cb9c0d6359f4857fd7005fade16908e68c630f40e3->leave($__internal_9bd9cf00718d895cd6cd44cb9c0d6359f4857fd7005fade16908e68c630f40e3_prof);

    }

    // line 2
    public function block_stylesh($context, array $blocks = array())
    {
        $__internal_c689e29bd6c73dc513a54a61008264dc09e09b3f7ab4c07988d6f2ff0303b3c2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c689e29bd6c73dc513a54a61008264dc09e09b3f7ab4c07988d6f2ff0303b3c2->enter($__internal_c689e29bd6c73dc513a54a61008264dc09e09b3f7ab4c07988d6f2ff0303b3c2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesh"));

        $__internal_fcc7ad478a5f7438b43b5c357450189b33aebadfdc0f71adf2490401015bfc1c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fcc7ad478a5f7438b43b5c357450189b33aebadfdc0f71adf2490401015bfc1c->enter($__internal_fcc7ad478a5f7438b43b5c357450189b33aebadfdc0f71adf2490401015bfc1c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesh"));

        // line 3
        echo "
    ";
        // line 6
        echo "    ";
        // line 7
        echo "    ";
        // line 8
        echo "    ";
        // line 9
        echo "    ";
        // line 10
        echo "    <link rel=\"stylesheet\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/jquery-ui-1.12.1.custom/jquery-ui.min.css"), "html", null, true);
        echo "\"
";
        
        $__internal_fcc7ad478a5f7438b43b5c357450189b33aebadfdc0f71adf2490401015bfc1c->leave($__internal_fcc7ad478a5f7438b43b5c357450189b33aebadfdc0f71adf2490401015bfc1c_prof);

        
        $__internal_c689e29bd6c73dc513a54a61008264dc09e09b3f7ab4c07988d6f2ff0303b3c2->leave($__internal_c689e29bd6c73dc513a54a61008264dc09e09b3f7ab4c07988d6f2ff0303b3c2_prof);

    }

    // line 12
    public function block_body($context, array $blocks = array())
    {
        $__internal_5d43a640419f8bbe8f4e7de47b89193184eba9870ecb236d7230cd9b17ef3da2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5d43a640419f8bbe8f4e7de47b89193184eba9870ecb236d7230cd9b17ef3da2->enter($__internal_5d43a640419f8bbe8f4e7de47b89193184eba9870ecb236d7230cd9b17ef3da2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_297f93c842821d2217a25a366b61d7fc8a071fa298a572db8e7a804003b15279 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_297f93c842821d2217a25a366b61d7fc8a071fa298a572db8e7a804003b15279->enter($__internal_297f93c842821d2217a25a366b61d7fc8a071fa298a572db8e7a804003b15279_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 13
        echo "    <div class=\"preloader\">
        <div class=\"cssload-speeding-wheel\"></div>
    </div>
    <div id=\"page-wrapper\">
        <div class=\"container-fluid\">
            <div class=\"row bg-title\">
                <div class=\"col-lg-3 col-md-4 col-sm-4 col-xs-12\">
                    <h4 class=\"page-title arabe\"> اضافة موعد</h4></div>
                <!-- /.col-lg-12 -->
            </div>
            <div class=\"row\">
                <div class=\"white-box\">
                    ";
        // line 25
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start');
        echo "
                    ";
        // line 36
        echo "                    ";
        // line 45
        echo "                    <div class=\"form-group\">
                        ";
        // line 47
        echo "                        ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "doctor", array()), 'label', array("label_attr" => array("class" => "arabe control-label"), "label" => "الطبيب"));
        echo "

                        ";
        // line 50
        echo "                        ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "doctor", array()), 'errors');
        echo "
                        ";
        // line 52
        echo "                        ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "doctor", array()), 'widget', array("attr" => array("class" => "arabe form-control doctor_select")));
        echo "
                    </div>
                    <div class=\"form-group user_select\">
                        ";
        // line 56
        echo "                        ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "user", array()), 'label', array("label_attr" => array("class" => "arabe control-label"), "label" => "المستخدم"));
        echo "

                        ";
        // line 59
        echo "                        ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "user", array()), 'errors');
        echo "
                        ";
        // line 61
        echo "                        ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "user", array()), 'widget', array("attr" => array("class" => "arabe form-control user_select")));
        echo "
                    </div>
                    <div class=\"form-group\">
                        <label for=\"datetimepicker\" class=\"control-label arabe\">تاريخ الموعد المرجو</label>
                        <input type=\"text\" value=\"\" id=\"datepicker\"
                               name=\"doctors_adminbundle_appointment[appointment]\" class=\"arabe form-control\"/>
                    </div>
                    <div class=\"text-center loading\">
                        <img src=\"";
        // line 69
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("uploads/load.gif"), "html", null, true);
        echo "\" alt=\"loading\" id=\"loading\"/>
                        ";
        // line 75
        echo "
                    </div>
                    <div class=\"form-group\">
                        <label class=\"control-label arabe\">حجز موعد</label>
                        <select name=\"dateAppt\" id=\"mySelect\">

                        </select>
                    </div>
                    ";
        // line 98
        echo "                    <div class=\"form-group\">
                        ";
        // line 100
        echo "                        ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "status", array()), 'label', array("label_attr" => array("class" => "arabe control-label"), "label" => "الحالة"));
        echo "

                        ";
        // line 103
        echo "                        ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "status", array()), 'errors');
        echo "
                        ";
        // line 105
        echo "                        ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "status", array()), 'widget', array("attr" => array("class" => "arabe form-control")));
        echo "
                    </div>
                    <input type=\"submit\" value=\"اضافة الموعد\" class=\"arabe btn btn-primary btn-lg\"/>
                    ";
        // line 108
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "
                </div>
            </div>
        </div>
    </div>
";
        
        $__internal_297f93c842821d2217a25a366b61d7fc8a071fa298a572db8e7a804003b15279->leave($__internal_297f93c842821d2217a25a366b61d7fc8a071fa298a572db8e7a804003b15279_prof);

        
        $__internal_5d43a640419f8bbe8f4e7de47b89193184eba9870ecb236d7230cd9b17ef3da2->leave($__internal_5d43a640419f8bbe8f4e7de47b89193184eba9870ecb236d7230cd9b17ef3da2_prof);

    }

    // line 114
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_15dcdd160ef2094535bc05203b9e851b0f9fb7a7b1b2caed584f81bcd8fe8e05 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_15dcdd160ef2094535bc05203b9e851b0f9fb7a7b1b2caed584f81bcd8fe8e05->enter($__internal_15dcdd160ef2094535bc05203b9e851b0f9fb7a7b1b2caed584f81bcd8fe8e05_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_3c23cf3248722842223c9c038c0d7e2e8c5f448ddc15f7b77fabd867a2c8a42d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3c23cf3248722842223c9c038c0d7e2e8c5f448ddc15f7b77fabd867a2c8a42d->enter($__internal_3c23cf3248722842223c9c038c0d7e2e8c5f448ddc15f7b77fabd867a2c8a42d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 115
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    <script src=\"";
        // line 116
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("datatable/js/jquery.metisMenu.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 117
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("datatable/js/jquery-ui.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 118
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("datatable/js/jquery.blockUI.js"), "html", null, true);
        echo "\"></script>

    <!--Functions Js-->
    <script src=\"";
        // line 121
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("datatable/js/functions.js"), "html", null, true);
        echo "\"></script>

    <script src=\"";
        // line 123
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("datatable/js/loader.js"), "html", null, true);
        echo "\"></script>

    <script src=\"";
        // line 125
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("datatable/js/jquery.dataTables.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 126
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("datatable/js/dataTables.bootstrap.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 127
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("datatable/js/jszip.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 128
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("datatable/js/pdfmake.min.js"), "html", null, true);
        echo "\"></script>
    ";
        // line 130
        echo "    <script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("datatable/js/buttons.html5.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 131
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("datatable/js/buttons.colVis.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 132
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("datatable/js/dataTables-script.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 133
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/jquery-ui-1.12.1.custom/datepicker-ar.js"), "html", null, true);
        echo "\"></script>
    <script>
        \$(function () {
            /*\$(\"#datepicker\").datepicker(\$.datepicker.regional[\"ar\"]);*/
            \$(\"#datepicker\").datepicker({
                maxDate: \"+30\",
            });
            /*\$(\"#locale\").on(\"change\", function () {
             \$(\"#datepicker\").datepicker(\"option\",
             \$.datepicker.regional[\$(this).val()]);
             });*/

        });
    </script>
    <script>
        var startWorkTime, endWorkTime, dateObject = new Date();
        \$('#datepicker').hide();
        \$('#loading').hide();
        \$('#mySelect').hide();
        \$('.user_select').hide();
        var id_doctor;
        \$('.doctor_select').on('change', function () {
            \$('.user_select').show();
            console.log('__________akrem________');
            id_doctor = this.value;
            console.log(id_doctor);
        });
        \$('.user_select').on('change', function () {
            \$('#datepicker').show();
            console.log('__________akrem________');
            id_user = this.value;
            console.log(id_user);
        });
        \$(\"#datepicker\").datepicker({
            maxDate: +30,
            onSelect: function () {
                dateObject = \$(this).datepicker('getDate');
                dateObject = new Date(dateObject);
                var month = dateObject.getMonth() + 1; //months from 1-12
                var day = dateObject.getDate();
                var year = dateObject.getFullYear();
                if (month < 10) {
                    month = '0' + month;
                }
                if (day < 10) {
                    day = '0' + day;
                }
                newdate = year + \"-\" + month + \"-\" + day;
                console.log(id_doctor);
                console.log('newdate');
                console.log(newdate);
                \$('#loading').show();
                \$('#mySelect').hide();
                \$.ajax({
                    type: \"POST\",
                    url: \"";
        // line 188
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("loadDataTimeDoctor");
        echo "\",
                    data: {doctor_id: id_doctor, user_id: id_user, dateAppointment: newdate},
                    dataType: \"json\",
                    success: function (reponse) {
                        /*console.log(reponse.date);*/
                        console.log('success');
                        \$('#loading').hide();
                        /*startWorkTime =  reponse.date[0];
                        endWorkTime = reponse.date[reponse.length];
                        \$('div.loading').append('<h2>أوقات العمل</h2><p> وقت بداية الدوام  : ' + startWorkTime.getHours() + ' و ' + startWorkTime.getMinutes() + ' دقيقة</p>');
                        \$('div.loading').append('<br> وقت نهاية الدوام : ' + endWorkTime.getHours() + ' و ' + endWorkTime.getMinutes() + ' دقيقة</p>');
                        \$('div.loading').append('<br><h2>الأوقات المتاحة لحجز موعد</h2> <br>');*/
                        \$.each(reponse, function (i, item) {
                            console.log(reponse[i].date);
                            if (reponse[i].disponible == 0) {
                            \$('#mySelect').show();
                            \$('#mySelect').append(
                                '<option value='+ reponse[i].date +' disabled>'+
                                reponse[i].date.substring(11, 16),+
                                '</option>'
                            );


                                /*\$('#mySelect option').attr('disabled','disabled');
                                \$('#mySelect option').css('background-color','red');*/
                            }else{
                                \$('#mySelect').show();
                                \$('#mySelect').append(\$('<option>', {
                                    value: reponse[i].date,
                                    text: reponse[i].date.substring(11, 16),
                                }))
                            }
                        });
                        console.log(reponse);


                        /*startWorkTime = new Date(reponse.date);
                         endWorkTime = new Date(reponse.endTimeWork.date);
                         console.log('startWorkTime');
                         console.log(startWorkTime);
                         console.log('endWorkTime');
                         console.log(endWorkTime);
                         \$('div.loading').empty();
                         \$('#loading').hide();
                         console.log(reponse.date);*/
                        /*\$('div.loading').append('<h2>أوقات العمل</h2><p> وقت بداية الدوام  : ' + startWorkTime.getHours() + ' و ' + startWorkTime.getMinutes() + ' دقيقة</p>');
                         \$('div.loading').append('<br> وقت نهاية الدوام : ' + endWorkTime.getHours() + ' و ' + endWorkTime.getMinutes() + ' دقيقة</p>');
                         \$('div.loading').append('<br><h2>الأوقات المتاحة لحجز موعد</h2> <br>');*/
                        /*var interval = \"30\";
                         timeslots = [startWorkTime];
                         while (startWorkTime != endWorkTime) {
                         startWorkTime = addMinutes(startWorkTime, interval);
                         /!*alert(timeslots);*!/
                         if (startWorkTime == false) {
                         \$('#mySelect').show();
                         timeslots.push(startWorkTime);
                         return false;
                         }
                         timeslots.push(startWorkTime);
                         console.log('------timeslot iiinn of while------');
                         console.log(timeslots);
                         \$.each(timeslots, function (i, item) {
                         itemHour = item.getHours();
                         if (itemHour < 10) {
                         itemHour = '0' + itemHour;
                         }

                         itemMinute = item.getMinutes();
                         if (itemMinute < 10) {
                         itemMinute = '0' + itemMinute;
                         /!*minutes = parseInt(minutes);*!/
                         }
                         itemSecondes = item.getSeconds();
                         if (itemSecondes < 10) {
                         itemSecondes = '0' + itemSecondes;
                         /!*minutes = parseInt(minutes);*!/
                         }
                         itemDate = itemHour + ':' + itemMinute + ':' + itemSecondes;
                         console.log('-----item-----');
                         console.log(item);
                         \$('#mySelect').append(\$('<option>', {
                         value: item,
                         text: itemDate
                         }));
                         });
                         }
                         console.log('------timeslot out of while------');
                         console.log(timeslots);
                         */
                        /*console.log(reponse);*/
                    },


                    error: function (reponse) {
                        console.log('erreur');
                        console.log(reponse);


                    }


                });
            }
        });


        /*\$('#datepicker').datepicker({
         onSelect : function() {
         alert('aaaaaa');
         var dateRDV = \$('#datepicker').datepicker('getDate');
         console.log('__________boussaha________');
         }
         });*/



        /*var dateRDV = \$('#datetimepicker3').val();*!/
         console.log(dateRDV);
         alert('__ok________');
         \$.ajax({
         type: \"POST\",
         url: \"";
        // line 309
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("loadDataTimeDoctor");
        echo "\",
         data: {doctor_id: id_doctor, dateAppointment: dateRDV},
         dataType: \"json\",
         success: function (reponse) {
         console.log(reponse);
         alert(reponse);
         },
         error: function (reponse) {
         console.log(reponse);
         alert(reponse);

         }
         });*/


    </script>
    ";
        // line 326
        echo "    ";
        // line 327
        echo "    ";
        // line 328
        echo "    <script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("plugins/bower_components/bootstrap-rtl-master/dist/js/bootstrap-rtl.min.js"), "html", null, true);
        echo "\"></script>

";
        
        $__internal_3c23cf3248722842223c9c038c0d7e2e8c5f448ddc15f7b77fabd867a2c8a42d->leave($__internal_3c23cf3248722842223c9c038c0d7e2e8c5f448ddc15f7b77fabd867a2c8a42d_prof);

        
        $__internal_15dcdd160ef2094535bc05203b9e851b0f9fb7a7b1b2caed584f81bcd8fe8e05->leave($__internal_15dcdd160ef2094535bc05203b9e851b0f9fb7a7b1b2caed584f81bcd8fe8e05_prof);

    }

    public function getTemplateName()
    {
        return "DoctorsAdminBundle:Appointment:ajouterAppointment.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  471 => 328,  469 => 327,  467 => 326,  448 => 309,  324 => 188,  266 => 133,  262 => 132,  258 => 131,  253 => 130,  249 => 128,  245 => 127,  241 => 126,  237 => 125,  232 => 123,  227 => 121,  221 => 118,  217 => 117,  213 => 116,  208 => 115,  199 => 114,  183 => 108,  176 => 105,  171 => 103,  165 => 100,  162 => 98,  152 => 75,  148 => 69,  136 => 61,  131 => 59,  125 => 56,  118 => 52,  113 => 50,  107 => 47,  104 => 45,  102 => 36,  98 => 25,  84 => 13,  75 => 12,  62 => 10,  60 => 9,  58 => 8,  56 => 7,  54 => 6,  51 => 3,  42 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"::layoutAdmin.html.twig\" %}
{% block stylesh %}

    {#<link href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css\" rel=\"stylesheet\">
    <link rel=\"stylesheet\" href=\"//cdn.rawgit.com/morteza/bootstrap-rtl/v3.3.4/dist/css/bootstrap-rtl.min.css\">#}
    {#<link href=\"{{ asset('bootstrap-datetimepicker-master/build/css/bootstrap-datetimepicker-standalone.css') }}\" rel=\"stylesheet\">#}
    {#<link rel=\"stylesheet\" href=\"{{ asset('js/jquery-ui-1.12.1.custom/jquery-ui.min.css') }}\" rel=\"stylesheet\">#}
    {#<link rel=\"stylesheet\" type=\"text/css\" href=\"{{ asset('datetimepicker-master/jquery.datetimepicker.css') }}\"/>#}
    {# <link href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css\" rel=\"stylesheet\">#}
    <link rel=\"stylesheet\" href=\"{{ asset('js/jquery-ui-1.12.1.custom/jquery-ui.min.css') }}\"
{% endblock %}
{% block body %}
    <div class=\"preloader\">
        <div class=\"cssload-speeding-wheel\"></div>
    </div>
    <div id=\"page-wrapper\">
        <div class=\"container-fluid\">
            <div class=\"row bg-title\">
                <div class=\"col-lg-3 col-md-4 col-sm-4 col-xs-12\">
                    <h4 class=\"page-title arabe\"> اضافة موعد</h4></div>
                <!-- /.col-lg-12 -->
            </div>
            <div class=\"row\">
                <div class=\"white-box\">
                    {{ form_start(form) }}
                    {#<div class=\"form-group\">
                        #}{# Génération du label. #}{#
                        {{ form_label(form.appointment, \"التاريخ المرجو للموعد\", {'label_attr': {'class': 'arabe control-label'}}) }}

                        #}{# Affichage des erreurs pour ce champ précis. #}{#
                        {{ form_errors(form.appointment) }}
                        #}{# Génération de l'input. #}{#
                        <input value=\"\" id=\"datetimepicker\" type=\"text\">
                        {{ form_widget(form.appointment, {'attr': {'id': 'datetimepicker arabe'}}) }}
                    </div>#}
                    {#<div class=\"form-group\">
                        <div class='input-group date' id='datetimepicker'>
                            {{ form_label(form.appointment, null, { 'label_attr': {'class': 'control-label arabe'} }) }}{{ form_errors(form.appointment) }}
                            {{ form_widget(form.appointment, {'attr': {'class': 'form-control datetimepicker'}}) }}
                            <span class=\"input-group-addon\">
                                    <span class=\"glyphicon glyphicon-calendar\"></span>
                                </span>
                        </div>
                    </div>#}
                    <div class=\"form-group\">
                        {# Génération du label. #}
                        {{ form_label(form.doctor, \"الطبيب\", {'label_attr': {'class': 'arabe control-label'}}) }}

                        {# Affichage des erreurs pour ce champ précis. #}
                        {{ form_errors(form.doctor) }}
                        {# Génération de l'input. #}
                        {{ form_widget(form.doctor, {'attr': {'class': 'arabe form-control doctor_select'}}) }}
                    </div>
                    <div class=\"form-group user_select\">
                        {# Génération du label. #}
                        {{ form_label(form.user, \"المستخدم\", {'label_attr': {'class': 'arabe control-label'}}) }}

                        {# Affichage des erreurs pour ce champ précis. #}
                        {{ form_errors(form.user) }}
                        {# Génération de l'input. #}
                        {{ form_widget(form.user, {'attr': {'class': 'arabe form-control user_select'}}) }}
                    </div>
                    <div class=\"form-group\">
                        <label for=\"datetimepicker\" class=\"control-label arabe\">تاريخ الموعد المرجو</label>
                        <input type=\"text\" value=\"\" id=\"datepicker\"
                               name=\"doctors_adminbundle_appointment[appointment]\" class=\"arabe form-control\"/>
                    </div>
                    <div class=\"text-center loading\">
                        <img src=\"{{ asset('uploads/load.gif') }}\" alt=\"loading\" id=\"loading\"/>
                        {#{% if (doctor is not defined) %}
                            <p>i'm here</p>
                        {% else %}
                            {{ doctor.startTimeWork|date('h:i') }}
                        {% endif %}#}

                    </div>
                    <div class=\"form-group\">
                        <label class=\"control-label arabe\">حجز موعد</label>
                        <select name=\"dateAppt\" id=\"mySelect\">

                        </select>
                    </div>
                    {#
                    <!-- code datetimepicker calendar for date and time -->
                    <div class=\"form-group\">
                        <div class=\"input-group date\" id=\"datetimepicker1\">
                            #}{# Génération du label. #}{#
                            {{ form_label(form.appointment, \"التاريخ المرجو للموعد\", {'label_attr': {'class': 'arabe control-label'}}) }}

                            #}{# Affichage des erreurs pour ce champ précis. #}{#
                            {{ form_errors(form.appointment) }}
                            #}{# Génération de l'input. #}{#
                            {{ form_widget(form.appointment, {'attr': {'class': 'arabe form-control'}}) }}
                            <span class=\"input-group-addon\">
                        <span class=\"glyphicon glyphicon-calendar\"></span></span>
                        </div>
                    </div>#}
                    <div class=\"form-group\">
                        {# Génération du label. #}
                        {{ form_label(form.status, \"الحالة\", {'label_attr': {'class': 'arabe control-label'}}) }}

                        {# Affichage des erreurs pour ce champ précis. #}
                        {{ form_errors(form.status) }}
                        {# Génération de l'input. #}
                        {{ form_widget(form.status, {'attr': {'class': 'arabe form-control'}}) }}
                    </div>
                    <input type=\"submit\" value=\"اضافة الموعد\" class=\"arabe btn btn-primary btn-lg\"/>
                    {{ form_end(form) }}
                </div>
            </div>
        </div>
    </div>
{% endblock %}
{% block javascripts %}
    {{ parent() }}
    <script src=\"{{ asset('datatable/js/jquery.metisMenu.js') }}\"></script>
    <script src=\"{{ asset('datatable/js/jquery-ui.js') }}\"></script>
    <script src=\"{{ asset('datatable/js/jquery.blockUI.js') }}\"></script>

    <!--Functions Js-->
    <script src=\"{{ asset('datatable/js/functions.js') }}\"></script>

    <script src=\"{{ asset('datatable/js/loader.js') }}\"></script>

    <script src=\"{{ asset('datatable/js/jquery.dataTables.min.js') }}\"></script>
    <script src=\"{{ asset('datatable/js/dataTables.bootstrap.min.js') }}\"></script>
    <script src=\"{{ asset('datatable/js/jszip.min.js') }}\"></script>
    <script src=\"{{ asset('datatable/js/pdfmake.min.js') }}\"></script>
    {#<script src=\"{{ asset('datatable/js/vfs_fonts.js') }}\"></script>#}
    <script src=\"{{ asset('datatable/js/buttons.html5.js') }}\"></script>
    <script src=\"{{ asset('datatable/js/buttons.colVis.js') }}\"></script>
    <script src=\"{{ asset('datatable/js/dataTables-script.js') }}\"></script>
    <script src=\"{{ asset('js/jquery-ui-1.12.1.custom/datepicker-ar.js') }}\"></script>
    <script>
        \$(function () {
            /*\$(\"#datepicker\").datepicker(\$.datepicker.regional[\"ar\"]);*/
            \$(\"#datepicker\").datepicker({
                maxDate: \"+30\",
            });
            /*\$(\"#locale\").on(\"change\", function () {
             \$(\"#datepicker\").datepicker(\"option\",
             \$.datepicker.regional[\$(this).val()]);
             });*/

        });
    </script>
    <script>
        var startWorkTime, endWorkTime, dateObject = new Date();
        \$('#datepicker').hide();
        \$('#loading').hide();
        \$('#mySelect').hide();
        \$('.user_select').hide();
        var id_doctor;
        \$('.doctor_select').on('change', function () {
            \$('.user_select').show();
            console.log('__________akrem________');
            id_doctor = this.value;
            console.log(id_doctor);
        });
        \$('.user_select').on('change', function () {
            \$('#datepicker').show();
            console.log('__________akrem________');
            id_user = this.value;
            console.log(id_user);
        });
        \$(\"#datepicker\").datepicker({
            maxDate: +30,
            onSelect: function () {
                dateObject = \$(this).datepicker('getDate');
                dateObject = new Date(dateObject);
                var month = dateObject.getMonth() + 1; //months from 1-12
                var day = dateObject.getDate();
                var year = dateObject.getFullYear();
                if (month < 10) {
                    month = '0' + month;
                }
                if (day < 10) {
                    day = '0' + day;
                }
                newdate = year + \"-\" + month + \"-\" + day;
                console.log(id_doctor);
                console.log('newdate');
                console.log(newdate);
                \$('#loading').show();
                \$('#mySelect').hide();
                \$.ajax({
                    type: \"POST\",
                    url: \"{{ path('loadDataTimeDoctor') }}\",
                    data: {doctor_id: id_doctor, user_id: id_user, dateAppointment: newdate},
                    dataType: \"json\",
                    success: function (reponse) {
                        /*console.log(reponse.date);*/
                        console.log('success');
                        \$('#loading').hide();
                        /*startWorkTime =  reponse.date[0];
                        endWorkTime = reponse.date[reponse.length];
                        \$('div.loading').append('<h2>أوقات العمل</h2><p> وقت بداية الدوام  : ' + startWorkTime.getHours() + ' و ' + startWorkTime.getMinutes() + ' دقيقة</p>');
                        \$('div.loading').append('<br> وقت نهاية الدوام : ' + endWorkTime.getHours() + ' و ' + endWorkTime.getMinutes() + ' دقيقة</p>');
                        \$('div.loading').append('<br><h2>الأوقات المتاحة لحجز موعد</h2> <br>');*/
                        \$.each(reponse, function (i, item) {
                            console.log(reponse[i].date);
                            if (reponse[i].disponible == 0) {
                            \$('#mySelect').show();
                            \$('#mySelect').append(
                                '<option value='+ reponse[i].date +' disabled>'+
                                reponse[i].date.substring(11, 16),+
                                '</option>'
                            );


                                /*\$('#mySelect option').attr('disabled','disabled');
                                \$('#mySelect option').css('background-color','red');*/
                            }else{
                                \$('#mySelect').show();
                                \$('#mySelect').append(\$('<option>', {
                                    value: reponse[i].date,
                                    text: reponse[i].date.substring(11, 16),
                                }))
                            }
                        });
                        console.log(reponse);


                        /*startWorkTime = new Date(reponse.date);
                         endWorkTime = new Date(reponse.endTimeWork.date);
                         console.log('startWorkTime');
                         console.log(startWorkTime);
                         console.log('endWorkTime');
                         console.log(endWorkTime);
                         \$('div.loading').empty();
                         \$('#loading').hide();
                         console.log(reponse.date);*/
                        /*\$('div.loading').append('<h2>أوقات العمل</h2><p> وقت بداية الدوام  : ' + startWorkTime.getHours() + ' و ' + startWorkTime.getMinutes() + ' دقيقة</p>');
                         \$('div.loading').append('<br> وقت نهاية الدوام : ' + endWorkTime.getHours() + ' و ' + endWorkTime.getMinutes() + ' دقيقة</p>');
                         \$('div.loading').append('<br><h2>الأوقات المتاحة لحجز موعد</h2> <br>');*/
                        /*var interval = \"30\";
                         timeslots = [startWorkTime];
                         while (startWorkTime != endWorkTime) {
                         startWorkTime = addMinutes(startWorkTime, interval);
                         /!*alert(timeslots);*!/
                         if (startWorkTime == false) {
                         \$('#mySelect').show();
                         timeslots.push(startWorkTime);
                         return false;
                         }
                         timeslots.push(startWorkTime);
                         console.log('------timeslot iiinn of while------');
                         console.log(timeslots);
                         \$.each(timeslots, function (i, item) {
                         itemHour = item.getHours();
                         if (itemHour < 10) {
                         itemHour = '0' + itemHour;
                         }

                         itemMinute = item.getMinutes();
                         if (itemMinute < 10) {
                         itemMinute = '0' + itemMinute;
                         /!*minutes = parseInt(minutes);*!/
                         }
                         itemSecondes = item.getSeconds();
                         if (itemSecondes < 10) {
                         itemSecondes = '0' + itemSecondes;
                         /!*minutes = parseInt(minutes);*!/
                         }
                         itemDate = itemHour + ':' + itemMinute + ':' + itemSecondes;
                         console.log('-----item-----');
                         console.log(item);
                         \$('#mySelect').append(\$('<option>', {
                         value: item,
                         text: itemDate
                         }));
                         });
                         }
                         console.log('------timeslot out of while------');
                         console.log(timeslots);
                         */
                        /*console.log(reponse);*/
                    },


                    error: function (reponse) {
                        console.log('erreur');
                        console.log(reponse);


                    }


                });
            }
        });


        /*\$('#datepicker').datepicker({
         onSelect : function() {
         alert('aaaaaa');
         var dateRDV = \$('#datepicker').datepicker('getDate');
         console.log('__________boussaha________');
         }
         });*/



        /*var dateRDV = \$('#datetimepicker3').val();*!/
         console.log(dateRDV);
         alert('__ok________');
         \$.ajax({
         type: \"POST\",
         url: \"{{ path('loadDataTimeDoctor') }}\",
         data: {doctor_id: id_doctor, dateAppointment: dateRDV},
         dataType: \"json\",
         success: function (reponse) {
         console.log(reponse);
         alert(reponse);
         },
         error: function (reponse) {
         console.log(reponse);
         alert(reponse);

         }
         });*/


    </script>
    {#<script src=\"{{ asset('js/jquery-ui-1.12.1.custom/jquery-ui.min.js') }}\"></script>#}
    {#<script src=\"{{ asset('js/arabic-date.js') }}\"></script>#}
    {#<script src=\"{{ asset('datetimepicker-master/build/jquery.datetimepicker.full.min.js') }}\"></script>#}
    <script src=\"{{ asset('plugins/bower_components/bootstrap-rtl-master/dist/js/bootstrap-rtl.min.js') }}\"></script>

{% endblock %}", "DoctorsAdminBundle:Appointment:ajouterAppointment.html.twig", "C:\\wamp64\\www\\doctorAdmin\\src\\Doctors\\AdminBundle/Resources/views/Appointment/ajouterAppointment.html.twig");
    }
}
