<?php

/* DoctorsAdminBundle:Admin:index.html.twig */
class __TwigTemplate_5a428a8c0d1cb528b9e218cf682b3b83719f9a3e964e16f4a9a35e62892c472a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::layoutAdmin.html.twig", "DoctorsAdminBundle:Admin:index.html.twig", 1);
        $this->blocks = array(
            'stylesh' => array($this, 'block_stylesh'),
            'body' => array($this, 'block_body'),
            'footer' => array($this, 'block_footer'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::layoutAdmin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c0e473fb2c87718b8ea3bc3319ef3c29887e76e1bb3c9048fdc45cd5abf470cb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c0e473fb2c87718b8ea3bc3319ef3c29887e76e1bb3c9048fdc45cd5abf470cb->enter($__internal_c0e473fb2c87718b8ea3bc3319ef3c29887e76e1bb3c9048fdc45cd5abf470cb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "DoctorsAdminBundle:Admin:index.html.twig"));

        $__internal_6ed76ee367a11a2f29534764fd56985d57871a174a7b0c529af0390b5f687c61 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6ed76ee367a11a2f29534764fd56985d57871a174a7b0c529af0390b5f687c61->enter($__internal_6ed76ee367a11a2f29534764fd56985d57871a174a7b0c529af0390b5f687c61_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "DoctorsAdminBundle:Admin:index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_c0e473fb2c87718b8ea3bc3319ef3c29887e76e1bb3c9048fdc45cd5abf470cb->leave($__internal_c0e473fb2c87718b8ea3bc3319ef3c29887e76e1bb3c9048fdc45cd5abf470cb_prof);

        
        $__internal_6ed76ee367a11a2f29534764fd56985d57871a174a7b0c529af0390b5f687c61->leave($__internal_6ed76ee367a11a2f29534764fd56985d57871a174a7b0c529af0390b5f687c61_prof);

    }

    // line 2
    public function block_stylesh($context, array $blocks = array())
    {
        $__internal_40b07962dea2d0d15d58de7df748b17b1ced4a85fd006e48fa8276f1c8a0d501 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_40b07962dea2d0d15d58de7df748b17b1ced4a85fd006e48fa8276f1c8a0d501->enter($__internal_40b07962dea2d0d15d58de7df748b17b1ced4a85fd006e48fa8276f1c8a0d501_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesh"));

        $__internal_5aef34c4ab9dbd7e1ba913eb1a8f6419efe7f1872c0f99ad4a34ac7fcc7c7aac = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5aef34c4ab9dbd7e1ba913eb1a8f6419efe7f1872c0f99ad4a34ac7fcc7c7aac->enter($__internal_5aef34c4ab9dbd7e1ba913eb1a8f6419efe7f1872c0f99ad4a34ac7fcc7c7aac_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesh"));

        // line 3
        echo "    <style>
        #chartdiv, #chartdivv {
            width: 100%;
            height: 400px;
        }
    </style>
";
        
        $__internal_5aef34c4ab9dbd7e1ba913eb1a8f6419efe7f1872c0f99ad4a34ac7fcc7c7aac->leave($__internal_5aef34c4ab9dbd7e1ba913eb1a8f6419efe7f1872c0f99ad4a34ac7fcc7c7aac_prof);

        
        $__internal_40b07962dea2d0d15d58de7df748b17b1ced4a85fd006e48fa8276f1c8a0d501->leave($__internal_40b07962dea2d0d15d58de7df748b17b1ced4a85fd006e48fa8276f1c8a0d501_prof);

    }

    // line 10
    public function block_body($context, array $blocks = array())
    {
        $__internal_9700c99cfc0f11ddbccf310b08359b98f43141667a7b46f577b6b8922d5e4138 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9700c99cfc0f11ddbccf310b08359b98f43141667a7b46f577b6b8922d5e4138->enter($__internal_9700c99cfc0f11ddbccf310b08359b98f43141667a7b46f577b6b8922d5e4138_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_fad8fb5be38be0e49d3605d53bbcae2a507542057efc86f781363ff0a5638540 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fad8fb5be38be0e49d3605d53bbcae2a507542057efc86f781363ff0a5638540->enter($__internal_fad8fb5be38be0e49d3605d53bbcae2a507542057efc86f781363ff0a5638540_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 11
        echo "    <div class=\"preloader\">
        <div class=\"cssload-speeding-wheel\"></div>
    </div>
    <div id=\"wrapper\">
        <!-- Page Content -->
        <div id=\"page-wrapper\">
            <div class=\"container-fluid\">
                <div class=\"row bg-title\">
                    <div class=\"col-lg-3 col-md-4 col-sm-4 col-xs-12\">
                        <h4 class=\"page-title arabe\">لوحة التحكم</h4>
                    </div>
                </div>
                <div class=\"row\">
                    <div class=\"col-md-3 col-sm-6\">
                        <div class=\"white-box\">
                            <div class=\"r-icon-stats\"><i class=\"fa fa-4x fa-user-md \"></i>
                                <div class=\"bodystate\">
                                    <h4>";
        // line 28
        echo twig_escape_filter($this->env, (isset($context["doctorNumbers"]) ? $context["doctorNumbers"] : $this->getContext($context, "doctorNumbers")), "html", null, true);
        echo "</h4> <span class=\"text-muted arabe\">الأطباء</span></div>
                            </div>
                        </div>
                    </div>
                    <div class=\"col-md-3 col-sm-6\">
                        <div class=\"white-box\">
                            <div class=\"r-icon-stats\"><i class=\"fa fa-4x fa-users\" aria-hidden=\"true\"></i>
                                <div class=\"bodystate\">
                                    <h4>";
        // line 36
        echo twig_escape_filter($this->env, (isset($context["userNumbers"]) ? $context["userNumbers"] : $this->getContext($context, "userNumbers")), "html", null, true);
        echo "</h4> <span class=\"text-muted arabe\">المستخدمين</span></div>
                            </div>
                        </div>
                    </div>
                    <div class=\"col-md-3 col-sm-6\">
                        <div class=\"white-box\">
                            <div class=\"r-icon-stats\"><i class=\"fa fa-4x fa-calendar\" aria-hidden=\"true\"></i>

                                <div class=\"bodystate\">
                                    <h4>";
        // line 45
        echo twig_escape_filter($this->env, (isset($context["appointmentNumbers"]) ? $context["appointmentNumbers"] : $this->getContext($context, "appointmentNumbers")), "html", null, true);
        echo "</h4> <span class=\"text-muted arabe\">المواعيد</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class=\"col-md-3 col-sm-6\">
                        <div class=\"white-box\">
                            <div class=\"r-icon-stats\"><i class=\"fa fa-4x fa-comments\" aria-hidden=\"true\"></i>
                                <div class=\"bodystate\">
                                    <h4>";
        // line 54
        echo twig_escape_filter($this->env, (isset($context["evaluationNumbers"]) ? $context["evaluationNumbers"] : $this->getContext($context, "evaluationNumbers")), "html", null, true);
        echo "</h4> <span class=\"text-muted arabe\">التقييمات</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
                <div class=\"row\">
                    <div class=\"col-sm-12 col-md-12 col-xs-12\">
                        <div class=\"row\">
                            <div class=\"col-sm-12 col-md-6 col-xs-12\">
                                <div class=\"white-box\">
                                    <div id=\"container_barStats\"></div>
                                </div>
                            </div>
                            <div class=\"col-sm-12 col-md-6 col-xs-12\">
                                <div class=\"white-box\">
                                    <div id=\"container_barStatss\"></div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <!--row -->
                <div class=\"row\">

                    <div class=\"col-sm-12 col-md-12 col-lg-12 col-xs-12 white-box\">

                        <div class=\"col-md-5\">
                            <div class=\"panel panel-primary\">
                                <div class=\"panel-heading\">
                                    اخر المواعيد المطلوبة
                                </div>
                                <table class=\"arabe table table-striped table-hover table-responsive table-bordered\">
                                    <tr>
                                        <th>اخر المواعيد المطلوبة</th>
                                        <th>الطبيب</th>
                                        <th>المستخدم</th>
                                    </tr>
                                    ";
        // line 94
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["lastAppointments"]) ? $context["lastAppointments"] : $this->getContext($context, "lastAppointments")));
        foreach ($context['_seq'] as $context["_key"] => $context["appointment"]) {
            // line 95
            echo "                                        <tr>
                                            <td>";
            // line 96
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["appointment"], "appointment", array()), "Y-m-d H:i:s"), "html", null, true);
            echo "</td>
                                            <td>";
            // line 97
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["appointment"], "doctor", array()), "nameDoctor", array()), "html", null, true);
            echo "</td>
                                            <td>";
            // line 98
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["appointment"], "user", array()), "name", array()), "html", null, true);
            echo "</td>
                                        </tr>
                                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['appointment'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 101
        echo "                                </table>
                            </div>
                        </div>
                        <div class=\"col-md-3\">
                            <div class=\"panel panel-primary\">
                                <div class=\"panel-heading\">
                                    اخر المستخدمين
                                </div>
                                <table class=\"arabe table table-striped table-hover table-responsive table-bordered\">
                                    <tr>
                                        <th>اخر المستخدمين</th>
                                        <th>أرقام التواصل</th>

                                        ";
        // line 114
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["lastUsers"]) ? $context["lastUsers"] : $this->getContext($context, "lastUsers")));
        foreach ($context['_seq'] as $context["_key"] => $context["user"]) {
            // line 115
            echo "                                    <tr>
                                        <td>";
            // line 116
            echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "name", array()), "html", null, true);
            echo "</td>
                                        <td>";
            // line 117
            echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "phoneNumber", array()), "html", null, true);
            echo "</td>
                                    </tr>
                                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['user'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 120
        echo "                                </table>
                            </div>
                        </div>
                        <div class=\"col-md-4\">
                            <div class=\"panel panel-primary\">
                                <div class=\"panel-heading\">
                                    اخر التقييمات
                                </div>
                                <table class=\"arabe table table-striped table-hover table-responsive table-bordered\">
                                    <tr>
                                        <th>اخر التقييمات</th>
                                        <th>المستخدم</th>
                                        <th>الطبيب</th>

                                        ";
        // line 134
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["lastEvaluations"]) ? $context["lastEvaluations"] : $this->getContext($context, "lastEvaluations")));
        foreach ($context['_seq'] as $context["_key"] => $context["evaluation"]) {
            // line 135
            echo "                                    <tr>
                                        <td>";
            // line 136
            echo twig_escape_filter($this->env, $this->getAttribute($context["evaluation"], "evaluation", array()), "html", null, true);
            echo "</td>
                                        <td>";
            // line 137
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["evaluation"], "user", array()), "name", array()), "html", null, true);
            echo "</td>
                                        <td>";
            // line 138
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["evaluation"], "doctor", array()), "nameDoctor", array()), "html", null, true);
            echo "</td>
                                    </tr>
                                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['evaluation'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 141
        echo "                                </table>
                            </div>
                        </div>
                    </div>
                    <div style=\"height: 280px;\">
                        <div id=\"placeholder\" class=\"demo-placeholder\"></div>
                    </div>

                </div>
                <!-- row -->
                <!-- /.row -->
                <!-- .right-sidebar -->
                <div class=\"right-sidebar\">
                    <div class=\"slimscrollright\">
                        <div class=\"rpanel-title\"> Service Panel <span><i
                                        class=\"ti-close right-side-toggle\"></i></span></div>
                        <div class=\"r-panel-body\">
                            <ul>
                                <li><b>Layout Options</b></li>
                                <li>
                                    <div class=\"checkbox checkbox-info\">
                                        <input id=\"checkbox1\" type=\"checkbox\" class=\"fxhdr\">
                                        <label for=\"checkbox1\"> Fix Header </label>
                                    </div>
                                </li>
                                <li>
                                    <div class=\"checkbox checkbox-warning\">
                                        <input id=\"checkbox2\" type=\"checkbox\" class=\"fxsdr\">
                                        <label for=\"checkbox2\"> Fix Sidebar </label>
                                    </div>
                                </li>
                                <li>
                                    <div class=\"checkbox checkbox-success\">
                                        <input id=\"checkbox4\" type=\"checkbox\" class=\"open-close\">
                                        <label for=\"checkbox4\"> Toggle Sidebar </label>
                                    </div>
                                </li>
                            </ul>
                            <ul id=\"themecolors\" class=\"m-t-20\">
                                <li><b>With Light sidebar</b></li>
                                <li><a href=\"javascript:void(0)\" theme=\"default\" class=\"default-theme\">1</a></li>
                                <li><a href=\"javascript:void(0)\" theme=\"green\" class=\"green-theme\">2</a></li>
                                <li><a href=\"javascript:void(0)\" theme=\"gray\" class=\"yellow-theme\">3</a></li>
                                <li><a href=\"javascript:void(0)\" theme=\"blue\" class=\"blue-theme working\">4</a></li>
                                <li><a href=\"javascript:void(0)\" theme=\"purple\" class=\"purple-theme\">5</a></li>
                                <li><a href=\"javascript:void(0)\" theme=\"megna\" class=\"megna-theme\">6</a></li>
                                <li><b>With Dark sidebar</b></li>
                                <br/>
                                <li><a href=\"javascript:void(0)\" theme=\"default-dark\"
                                       class=\"default-dark-theme\">7</a></li>
                                <li><a href=\"javascript:void(0)\" theme=\"green-dark\" class=\"green-dark-theme\">8</a>
                                </li>
                                <li><a href=\"javascript:void(0)\" theme=\"gray-dark\" class=\"yellow-dark-theme\">9</a>
                                </li>

                                <li><a href=\"javascript:void(0)\" theme=\"blue-dark\" class=\"blue-dark-theme\">10</a>
                                </li>
                                <li><a href=\"javascript:void(0)\" theme=\"purple-dark\"
                                       class=\"purple-dark-theme\">11</a></li>
                                <li><a href=\"javascript:void(0)\" theme=\"megna-dark\" class=\"megna-dark-theme\">12</a>
                                </li>
                            </ul>
                            <ul class=\"m-t-20 chatonline\">
                                <li><b>Chat option</b></li>
                                <li><a href=\"javascript:void(0)\"><img
                                                src=\"";
        // line 206
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("plugins/images/users/varun.jpg"), "html", null, true);
        echo "\"
                                                alt=\"user-img\" class=\"img-circle\"> <span>Varun Dhavan <small
                                                    class=\"text-success\">online</small></span></a></li>
                                <li><a href=\"javascript:void(0)\"><img
                                                src=\"";
        // line 210
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("plugins/images/users/genu.jpg"), "html", null, true);
        echo "\"
                                                alt=\"user-img\" class=\"img-circle\"> <span>Genelia Deshmukh <small
                                                    class=\"text-warning\">Away</small></span></a></li>
                                <li><a href=\"javascript:void(0)\"><img
                                                src=\"";
        // line 214
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("plugins/images/users/ritesh.jpg"), "html", null, true);
        echo "\"
                                                alt=\"user-img\" class=\"img-circle\"> <span>Ritesh Deshmukh <small
                                                    class=\"text-danger\">Busy</small></span></a></li>
                                <li><a href=\"javascript:void(0)\"><img
                                                src=\"";
        // line 218
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("plugins/images/users/arijit.jpg"), "html", null, true);
        echo "\"
                                                alt=\"user-img\" class=\"img-circle\"> <span>Arijit Sinh <small
                                                    class=\"text-muted\">Offline</small></span></a></li>
                                <li><a href=\"javascript:void(0)\"><img
                                                src=\"";
        // line 222
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("plugins/images/users/govinda.jpg"), "html", null, true);
        echo "\"
                                                alt=\"user-img\" class=\"img-circle\"> <span>Govinda Star <small
                                                    class=\"text-success\">online</small></span></a></li>
                                <li><a href=\"javascript:void(0)\"><img
                                                src=\"";
        // line 226
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("plugins/images/users/hritik.jpg"), "html", null, true);
        echo "\"
                                                alt=\"user-img\" class=\"img-circle\"> <span>John Abraham<small
                                                    class=\"text-success\">online</small></span></a></li>
                                <li><a href=\"javascript:void(0)\"><img
                                                src=\"";
        // line 230
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("plugins/images/users/john.jpg"), "html", null, true);
        echo "\"
                                                alt=\"user-img\" class=\"img-circle\"> <span>Hritik Roshan<small
                                                    class=\"text-success\">online</small></span></a></li>
                                <li><a href=\"javascript:void(0)\"><img
                                                src=\"";
        // line 234
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("plugins/images/users/pawandeep.jpg"), "html", null, true);
        echo "\"
                                                alt=\"user-img\" class=\"img-circle\"> <span>Pwandeep rajan <small
                                                    class=\"text-success\">online</small></span></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- /.right-sidebar -->
            </div>
            <!-- /.container-fluid -->
            ";
        // line 244
        $this->displayBlock('footer', $context, $blocks);
        // line 247
        echo "        </div>
        <!-- /#page-wrapper -->
    </div>
    </div>
";
        
        $__internal_fad8fb5be38be0e49d3605d53bbcae2a507542057efc86f781363ff0a5638540->leave($__internal_fad8fb5be38be0e49d3605d53bbcae2a507542057efc86f781363ff0a5638540_prof);

        
        $__internal_9700c99cfc0f11ddbccf310b08359b98f43141667a7b46f577b6b8922d5e4138->leave($__internal_9700c99cfc0f11ddbccf310b08359b98f43141667a7b46f577b6b8922d5e4138_prof);

    }

    // line 244
    public function block_footer($context, array $blocks = array())
    {
        $__internal_a989fab2fd4183793d977262fd250b14e362dc62cbef848f02049579523d6474 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a989fab2fd4183793d977262fd250b14e362dc62cbef848f02049579523d6474->enter($__internal_a989fab2fd4183793d977262fd250b14e362dc62cbef848f02049579523d6474_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "footer"));

        $__internal_da5c2ea446de5c2b65c71dad1663053a56702bef74633378e68119de079156c0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_da5c2ea446de5c2b65c71dad1663053a56702bef74633378e68119de079156c0->enter($__internal_da5c2ea446de5c2b65c71dad1663053a56702bef74633378e68119de079156c0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "footer"));

        // line 245
        echo "                ";
        $this->displayParentBlock("footer", $context, $blocks);
        echo "
            ";
        
        $__internal_da5c2ea446de5c2b65c71dad1663053a56702bef74633378e68119de079156c0->leave($__internal_da5c2ea446de5c2b65c71dad1663053a56702bef74633378e68119de079156c0_prof);

        
        $__internal_a989fab2fd4183793d977262fd250b14e362dc62cbef848f02049579523d6474->leave($__internal_a989fab2fd4183793d977262fd250b14e362dc62cbef848f02049579523d6474_prof);

    }

    // line 252
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_bf13eff4f15fde896bf7e150046f16d317a37aaf9db0b0a06dbe6c432034a780 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_bf13eff4f15fde896bf7e150046f16d317a37aaf9db0b0a06dbe6c432034a780->enter($__internal_bf13eff4f15fde896bf7e150046f16d317a37aaf9db0b0a06dbe6c432034a780_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_356f6758e39dfe3c5dbc73c2f940298289dc23879f9634a65f87a454c7b76760 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_356f6758e39dfe3c5dbc73c2f940298289dc23879f9634a65f87a454c7b76760->enter($__internal_356f6758e39dfe3c5dbc73c2f940298289dc23879f9634a65f87a454c7b76760_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 253
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    <script src=\"";
        // line 254
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("datatable/js/jquery.metisMenu.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 255
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("datatable/js/jquery-ui.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 256
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("datatable/js/jquery.blockUI.js"), "html", null, true);
        echo "\"></script>

    <!--Functions Js-->
    <script src=\"";
        // line 259
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("datatable/js/functions.js"), "html", null, true);
        echo "\"></script>

    <script src=\"";
        // line 261
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("datatable/js/loader.js"), "html", null, true);
        echo "\"></script>

    <script src=\"";
        // line 263
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("datatable/js/jquery.dataTables.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 264
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("datatable/js/dataTables.bootstrap.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 265
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("datatable/js/jszip.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 266
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("datatable/js/pdfmake.min.js"), "html", null, true);
        echo "\"></script>
    ";
        // line 268
        echo "    <script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("datatable/js/buttons.html5.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 269
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("datatable/js/buttons.colVis.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 270
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("datatable/js/dataTables-script.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 271
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("plugins/bower_components/bootstrap-rtl-master/dist/js/bootstrap-rtl.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"https://code.jquery.com/jquery-3.1.1.min.js\"></script>
    <script src=\"https://code.highcharts.com/highcharts.js\"></script>
    <script src=\"https://code.highcharts.com/modules/exporting.js\"></script>
    <script>
        var chart = new Highcharts.Chart({

            chart: {
                renderTo: 'container_barStats',
                type: 'column',
                style: {
                    fontFamily: 'Droid Arabic Kufi, serif'
                }
            },

            xAxis: {
                categories: [
                    'المواعيد المؤكدة',
                    'المواعيد المنتهية',
                    'المواعيد المعلقة',
                    'المواعيد المرفوضة',
                ],
                reversed: true,
                title:{
                    style: {
                        fontFamily: 'Droid Arabic Kufi, serif'
                    }
                }
            },

            yAxis: {
                title: {
                    text: 'عدد المواعيد',
                    useHTML: Highcharts.hasBidiBug,
                    style: {
                        fontFamily: 'Droid Arabic Kufi, serif'
                    }
                },
                opposite: true
            },

            title: {
                text: 'احصائيات  حالات المواعيد',
                useHTML: Highcharts.hasBidiBug,
                style: {
                    fontFamily: 'Droid Arabic Kufi, serif'
                }
            },

            legend: {
                useHTML: Highcharts.hasBidiBug,
                style: {
                    fontFamily: 'Droid Arabic Kufi, serif'
                }
            },

            tooltip: {
                useHTML: true,
                style: {
                    fontFamily: 'Droid Arabic Kufi, serif'
                }
            },

            series: [{
                showInLegend: false,
                name: '',
                data: [";
        // line 337
        echo twig_escape_filter($this->env, (isset($context["appointmentConfirmedNumbers"]) ? $context["appointmentConfirmedNumbers"] : $this->getContext($context, "appointmentConfirmedNumbers")), "html", null, true);
        echo ",";
        echo twig_escape_filter($this->env, (isset($context["appointmentExceededNumbers"]) ? $context["appointmentExceededNumbers"] : $this->getContext($context, "appointmentExceededNumbers")), "html", null, true);
        echo ",";
        echo twig_escape_filter($this->env, (isset($context["appointmentWaintingNumbers"]) ? $context["appointmentWaintingNumbers"] : $this->getContext($context, "appointmentWaintingNumbers")), "html", null, true);
        echo ",";
        echo twig_escape_filter($this->env, (isset($context["appointmnentUnconfirmedNumbers"]) ? $context["appointmnentUnconfirmedNumbers"] : $this->getContext($context, "appointmnentUnconfirmedNumbers")), "html", null, true);
        echo "]
            }]

        });

    </script>
    <script>
        var chart = new Highcharts.Chart({

            chart: {
                renderTo: 'container_barStatss',
                type: 'column',
                style: {
                    fontFamily: 'Droid Arabic Kufi, serif'
                }
            },

            xAxis: {
                categories: [
                    ";
        // line 356
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["doctorsSpecialities"]) ? $context["doctorsSpecialities"] : $this->getContext($context, "doctorsSpecialities")));
        foreach ($context['_seq'] as $context["speciality"] => $context["value"]) {
            // line 357
            echo "
                        \"";
            // line 358
            echo twig_escape_filter($this->env, $context["speciality"], "html", null, true);
            echo "\",


                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['speciality'], $context['value'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 362
        echo "
                ],
                reversed: true
            },

            yAxis: {
                title: {
                    text: 'عدد الأطباء',
                    useHTML: Highcharts.hasBidiBug,
                    style: {
                        fontFamily: 'Droid Arabic Kufi, serif'
                    }
                },
                opposite: true
            },

            title: {
                text: 'احصائيات  الأطباء حسب الاختصاص',
                useHTML: Highcharts.hasBidiBug,
                style: {
                    fontFamily: 'Droid Arabic Kufi, serif'
                }
            },

            legend: {
                useHTML: Highcharts.hasBidiBug,
                style: {
                    fontFamily: 'Droid Arabic Kufi, serif'
                }
            },

            tooltip: {
                useHTML: true,
                style: {
                    fontFamily: 'Droid Arabic Kufi, serif'
                }
            },

            series: [{
                showInLegend: false,
                name: '',
                data: [";
        // line 403
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["doctorsSpecialities"]) ? $context["doctorsSpecialities"] : $this->getContext($context, "doctorsSpecialities")));
        foreach ($context['_seq'] as $context["speciality"] => $context["value"]) {
            // line 404
            echo "                    ";
            echo twig_escape_filter($this->env, $context["value"], "html", null, true);
            echo ",
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['speciality'], $context['value'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 405
        echo "]
            }]

        });
    </script>
    <script>
        \$(document).ready(function () {

            // Build the chart
            Highcharts.chart('container', {
                chart: {
                    renderTo: 'container',
                    type: 'column'
                },
                title: {
                    text: 'الاحصائيات التي تخص المواعيد',
                    style: {
                        fontSize: '13px',
                        fontFamily: 'DroidKufiRegular, Verdana, sans-serif'
                    }
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                },
                xAxis: {
                    reversed:true
                },
                yAxis: {
                    opposite:true
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            style: {
                                fontSize: '7px',
                                fontFamily: 'DroidKufiRegular, Verdana, sans-serif'
                            },
                        },
                        showInLegend: true
                    }
                },
                series: [{
                    name: 'النسبة',
                    colorByPoint: true,
                    style: {
                        fontSize: '13px',
                        fontFamily: 'DroidKufiRegular, Verdana, sans-serif'
                    },
                    data: [{
                        name: 'المواعيد المؤكدة',
                        y: ";
        // line 458
        echo twig_escape_filter($this->env, (isset($context["confirmedPercentAppointment"]) ? $context["confirmedPercentAppointment"] : $this->getContext($context, "confirmedPercentAppointment")), "html", null, true);
        echo ",
                    }, {
                        name: 'المواعيد المنتهية',
                        y: ";
        // line 461
        echo twig_escape_filter($this->env, (isset($context["exceededPercentAppointment"]) ? $context["exceededPercentAppointment"] : $this->getContext($context, "exceededPercentAppointment")), "html", null, true);
        echo ",
                        sliced: true,
                        selected: true
                    }, {
                        name: 'المواعيد المعلقة',
                        y: ";
        // line 466
        echo twig_escape_filter($this->env, (isset($context["waitingPercentAppointment"]) ? $context["waitingPercentAppointment"] : $this->getContext($context, "waitingPercentAppointment")), "html", null, true);
        echo "
                    }, {
                        name: 'المواعيد المرفوضة',
                        y: ";
        // line 469
        echo twig_escape_filter($this->env, (isset($context["unconfirmedPercentAppointment"]) ? $context["unconfirmedPercentAppointment"] : $this->getContext($context, "unconfirmedPercentAppointment")), "html", null, true);
        echo ",
                    },
                    ]
                }]
            });
        });
    </script>
    <script>
        Highcharts.chart('container_barStaats', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'الاحصائيات التي تخص المواعيد',
                style: {
                    fontSize: '13px',
                    fontFamily: 'DroidKufiRegular, Verdana, sans-serif'
                }
            },
            subtitle: {
                text: 'التصنيف حسب الحالة'
            },
            xAxis: {
                type: 'category',
                labels: {
                    rotation: -45,
                    style: {
                        fontSize: '13px',
                        fontFamily: 'DroidKufiRegular, Verdana, sans-serif '
                    }
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'عدد المواعيد'
                },
                style: {
                    fontSize: '13px',
                    fontFamily: 'DroidKufiRegular, Verdana, sans-serif'
                }
            },
            legend: {
                enabled: false
            },
            tooltip: {
                pointFormat: 'العدد'
            },
            series: [{
                name: 'المواعيد',
                data: [
                    ['المؤكدة', ";
        // line 520
        echo twig_escape_filter($this->env, (isset($context["appointmentConfirmedNumbers"]) ? $context["appointmentConfirmedNumbers"] : $this->getContext($context, "appointmentConfirmedNumbers")), "html", null, true);
        echo "],
                    ['المرفوضة', ";
        // line 521
        echo twig_escape_filter($this->env, (isset($context["appointmnentUnconfirmedNumbers"]) ? $context["appointmnentUnconfirmedNumbers"] : $this->getContext($context, "appointmnentUnconfirmedNumbers")), "html", null, true);
        echo "],
                    ['المعلقة', ";
        // line 522
        echo twig_escape_filter($this->env, (isset($context["appointmentWaintingNumbers"]) ? $context["appointmentWaintingNumbers"] : $this->getContext($context, "appointmentWaintingNumbers")), "html", null, true);
        echo "],
                    ['المنتهية', ";
        // line 523
        echo twig_escape_filter($this->env, (isset($context["appointmentExceededNumbers"]) ? $context["appointmentExceededNumbers"] : $this->getContext($context, "appointmentExceededNumbers")), "html", null, true);
        echo "],
                    /*
                     ['Mexico City', 8.9],
                     ['Lima', 8.9]*/
                ],
                dataLabels: {
                    enabled: true,
                    rotation: -90,
                    color: '#FFFFFF',
                    align: 'right',
                    format: '{point.y:.1f}', // one decimal
                    y: 10, // 10 pixels down from the top
                    style: {
                        fontSize: '13px',
                        fontFamily: 'DroidKufiRegular, Verdana, sans-serif'
                    }
                }
            }]
        });
    </script>
    <script src=\"https://www.amcharts.com/lib/3/amcharts.js\"></script>
    <script src=\"https://www.amcharts.com/lib/3/pie.js\"></script>
    <script src=\"https://www.amcharts.com/lib/3/serial.js\"></script>
    <script src=\"https://www.amcharts.com/lib/3/plugins/export/export.min.js\"></script>
    <link rel=\"stylesheet\" href=\"https://www.amcharts.com/lib/3/plugins/export/export.css\" type=\"text/css\" media=\"all\"/>
    <script src=\"https://www.amcharts.com/lib/3/themes/light.js\"></script>

    <!-- Chart code -->
    <script>

        var chart = AmCharts.makeChart(\"chartdiv\", {
            \"theme\": \"light\",
            \"type\": \"serial\",
            \"dataProvider\": [
                ";
        // line 557
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["doctorsSpecialities"]) ? $context["doctorsSpecialities"] : $this->getContext($context, "doctorsSpecialities")));
        foreach ($context['_seq'] as $context["speciality"] => $context["value"]) {
            // line 558
            echo "                {
                    \"country\": \"";
            // line 559
            echo twig_escape_filter($this->env, $context["speciality"], "html", null, true);
            echo "\",
                    \"visits\": \"";
            // line 560
            echo twig_escape_filter($this->env, $context["value"], "html", null, true);
            echo "\",
                    \"color\": \"FCD202\"
                },
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['speciality'], $context['value'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 564
        echo "            ],
            \"valueAxes\": [{
                \"position\": \"right\",
                \"gridColor\": \"#FFFFFF\",
                \"title\": \"عدد الاطباء\",
                \"gridAlpha\": 0.5,
                \"dashLength\": 0
            }],
            \"gridAboveGraphs\": true,
            \"startDuration\": 1,
            \"graphs\": [{
                \"balloonText\": \"[[category]]: <b>[[value]]</b>\",
                \"fillAlphas\": 0.8,
                \"lineAlpha\": 0.2,
                \"type\": \"column\",
                \"valueField\": \"visits\"
            }],
            \"chartCursor\": {
                \"categoryBalloonEnabled\": false,
                \"cursorAlpha\": 0,
                \"zoomable\": false
            },
            \"categoryField\": \"country\",
            \"categoryAxis\": {
                \"gridPosition\": \"start\",
                \"gridAlpha\": 0,
                \"tickPosition\": \"start\",
                \"tickLength\": 20
            },
            \"export\": {
                \"enabled\": true
            }


        }, 0);
    </script>
    <script>
        var chart = AmCharts.makeChart(\"chartdivv\", {
            \"type\": \"pie\",
            \"theme\": \"light\",
            \"dataProvider\": [{
                \"country\": \"المواعيد المعلقة\",
                \"value\": ";
        // line 606
        echo twig_escape_filter($this->env, (isset($context["appointmentWaintingNumbers"]) ? $context["appointmentWaintingNumbers"] : $this->getContext($context, "appointmentWaintingNumbers")), "html", null, true);
        echo "
            }, {
                \"country\": \"المواعيد المنتهية\",
                \"value\": ";
        // line 609
        echo twig_escape_filter($this->env, (isset($context["appointmentExceededNumbers"]) ? $context["appointmentExceededNumbers"] : $this->getContext($context, "appointmentExceededNumbers")), "html", null, true);
        echo "
            }, {
                \"country\": \"المواعيد المرفوضة\",
                \"value\": ";
        // line 612
        echo twig_escape_filter($this->env, (isset($context["appointmnentUnconfirmedNumbers"]) ? $context["appointmnentUnconfirmedNumbers"] : $this->getContext($context, "appointmnentUnconfirmedNumbers")), "html", null, true);
        echo "
            }, {
                \"country\": \"المواعيد المؤكدة\",
                \"value\": ";
        // line 615
        echo twig_escape_filter($this->env, (isset($context["appointmentConfirmedNumbers"]) ? $context["appointmentConfirmedNumbers"] : $this->getContext($context, "appointmentConfirmedNumbers")), "html", null, true);
        echo "
            }],
            \"valueField\": \"value\",
            \"titleField\": \"country\",
            \"outlineAlpha\": 0.4,
            \"depth3D\": 15,
            \"balloonText\": \"[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>\",
            \"angle\": 30,
            \"export\": {
                \"enabled\": true
            }
        });
    </script>
";
        
        $__internal_356f6758e39dfe3c5dbc73c2f940298289dc23879f9634a65f87a454c7b76760->leave($__internal_356f6758e39dfe3c5dbc73c2f940298289dc23879f9634a65f87a454c7b76760_prof);

        
        $__internal_bf13eff4f15fde896bf7e150046f16d317a37aaf9db0b0a06dbe6c432034a780->leave($__internal_bf13eff4f15fde896bf7e150046f16d317a37aaf9db0b0a06dbe6c432034a780_prof);

    }

    public function getTemplateName()
    {
        return "DoctorsAdminBundle:Admin:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  938 => 615,  932 => 612,  926 => 609,  920 => 606,  876 => 564,  866 => 560,  862 => 559,  859 => 558,  855 => 557,  818 => 523,  814 => 522,  810 => 521,  806 => 520,  752 => 469,  746 => 466,  738 => 461,  732 => 458,  677 => 405,  668 => 404,  664 => 403,  621 => 362,  611 => 358,  608 => 357,  604 => 356,  576 => 337,  507 => 271,  503 => 270,  499 => 269,  494 => 268,  490 => 266,  486 => 265,  482 => 264,  478 => 263,  473 => 261,  468 => 259,  462 => 256,  458 => 255,  454 => 254,  449 => 253,  440 => 252,  427 => 245,  418 => 244,  404 => 247,  402 => 244,  389 => 234,  382 => 230,  375 => 226,  368 => 222,  361 => 218,  354 => 214,  347 => 210,  340 => 206,  273 => 141,  264 => 138,  260 => 137,  256 => 136,  253 => 135,  249 => 134,  233 => 120,  224 => 117,  220 => 116,  217 => 115,  213 => 114,  198 => 101,  189 => 98,  185 => 97,  181 => 96,  178 => 95,  174 => 94,  131 => 54,  119 => 45,  107 => 36,  96 => 28,  77 => 11,  68 => 10,  52 => 3,  43 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"::layoutAdmin.html.twig\" %}
{% block stylesh %}
    <style>
        #chartdiv, #chartdivv {
            width: 100%;
            height: 400px;
        }
    </style>
{% endblock %}
{% block body %}
    <div class=\"preloader\">
        <div class=\"cssload-speeding-wheel\"></div>
    </div>
    <div id=\"wrapper\">
        <!-- Page Content -->
        <div id=\"page-wrapper\">
            <div class=\"container-fluid\">
                <div class=\"row bg-title\">
                    <div class=\"col-lg-3 col-md-4 col-sm-4 col-xs-12\">
                        <h4 class=\"page-title arabe\">لوحة التحكم</h4>
                    </div>
                </div>
                <div class=\"row\">
                    <div class=\"col-md-3 col-sm-6\">
                        <div class=\"white-box\">
                            <div class=\"r-icon-stats\"><i class=\"fa fa-4x fa-user-md \"></i>
                                <div class=\"bodystate\">
                                    <h4>{{ doctorNumbers }}</h4> <span class=\"text-muted arabe\">الأطباء</span></div>
                            </div>
                        </div>
                    </div>
                    <div class=\"col-md-3 col-sm-6\">
                        <div class=\"white-box\">
                            <div class=\"r-icon-stats\"><i class=\"fa fa-4x fa-users\" aria-hidden=\"true\"></i>
                                <div class=\"bodystate\">
                                    <h4>{{ userNumbers }}</h4> <span class=\"text-muted arabe\">المستخدمين</span></div>
                            </div>
                        </div>
                    </div>
                    <div class=\"col-md-3 col-sm-6\">
                        <div class=\"white-box\">
                            <div class=\"r-icon-stats\"><i class=\"fa fa-4x fa-calendar\" aria-hidden=\"true\"></i>

                                <div class=\"bodystate\">
                                    <h4>{{ appointmentNumbers }}</h4> <span class=\"text-muted arabe\">المواعيد</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class=\"col-md-3 col-sm-6\">
                        <div class=\"white-box\">
                            <div class=\"r-icon-stats\"><i class=\"fa fa-4x fa-comments\" aria-hidden=\"true\"></i>
                                <div class=\"bodystate\">
                                    <h4>{{ evaluationNumbers }}</h4> <span class=\"text-muted arabe\">التقييمات</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
                <div class=\"row\">
                    <div class=\"col-sm-12 col-md-12 col-xs-12\">
                        <div class=\"row\">
                            <div class=\"col-sm-12 col-md-6 col-xs-12\">
                                <div class=\"white-box\">
                                    <div id=\"container_barStats\"></div>
                                </div>
                            </div>
                            <div class=\"col-sm-12 col-md-6 col-xs-12\">
                                <div class=\"white-box\">
                                    <div id=\"container_barStatss\"></div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <!--row -->
                <div class=\"row\">

                    <div class=\"col-sm-12 col-md-12 col-lg-12 col-xs-12 white-box\">

                        <div class=\"col-md-5\">
                            <div class=\"panel panel-primary\">
                                <div class=\"panel-heading\">
                                    اخر المواعيد المطلوبة
                                </div>
                                <table class=\"arabe table table-striped table-hover table-responsive table-bordered\">
                                    <tr>
                                        <th>اخر المواعيد المطلوبة</th>
                                        <th>الطبيب</th>
                                        <th>المستخدم</th>
                                    </tr>
                                    {% for appointment in lastAppointments %}
                                        <tr>
                                            <td>{{ appointment.appointment|date('Y-m-d H:i:s') }}</td>
                                            <td>{{ appointment.doctor.nameDoctor }}</td>
                                            <td>{{ appointment.user.name }}</td>
                                        </tr>
                                    {% endfor %}
                                </table>
                            </div>
                        </div>
                        <div class=\"col-md-3\">
                            <div class=\"panel panel-primary\">
                                <div class=\"panel-heading\">
                                    اخر المستخدمين
                                </div>
                                <table class=\"arabe table table-striped table-hover table-responsive table-bordered\">
                                    <tr>
                                        <th>اخر المستخدمين</th>
                                        <th>أرقام التواصل</th>

                                        {% for user in lastUsers %}
                                    <tr>
                                        <td>{{ user.name }}</td>
                                        <td>{{ user.phoneNumber }}</td>
                                    </tr>
                                    {% endfor %}
                                </table>
                            </div>
                        </div>
                        <div class=\"col-md-4\">
                            <div class=\"panel panel-primary\">
                                <div class=\"panel-heading\">
                                    اخر التقييمات
                                </div>
                                <table class=\"arabe table table-striped table-hover table-responsive table-bordered\">
                                    <tr>
                                        <th>اخر التقييمات</th>
                                        <th>المستخدم</th>
                                        <th>الطبيب</th>

                                        {% for evaluation in lastEvaluations %}
                                    <tr>
                                        <td>{{ evaluation.evaluation }}</td>
                                        <td>{{ evaluation.user.name }}</td>
                                        <td>{{ evaluation.doctor.nameDoctor }}</td>
                                    </tr>
                                    {% endfor %}
                                </table>
                            </div>
                        </div>
                    </div>
                    <div style=\"height: 280px;\">
                        <div id=\"placeholder\" class=\"demo-placeholder\"></div>
                    </div>

                </div>
                <!-- row -->
                <!-- /.row -->
                <!-- .right-sidebar -->
                <div class=\"right-sidebar\">
                    <div class=\"slimscrollright\">
                        <div class=\"rpanel-title\"> Service Panel <span><i
                                        class=\"ti-close right-side-toggle\"></i></span></div>
                        <div class=\"r-panel-body\">
                            <ul>
                                <li><b>Layout Options</b></li>
                                <li>
                                    <div class=\"checkbox checkbox-info\">
                                        <input id=\"checkbox1\" type=\"checkbox\" class=\"fxhdr\">
                                        <label for=\"checkbox1\"> Fix Header </label>
                                    </div>
                                </li>
                                <li>
                                    <div class=\"checkbox checkbox-warning\">
                                        <input id=\"checkbox2\" type=\"checkbox\" class=\"fxsdr\">
                                        <label for=\"checkbox2\"> Fix Sidebar </label>
                                    </div>
                                </li>
                                <li>
                                    <div class=\"checkbox checkbox-success\">
                                        <input id=\"checkbox4\" type=\"checkbox\" class=\"open-close\">
                                        <label for=\"checkbox4\"> Toggle Sidebar </label>
                                    </div>
                                </li>
                            </ul>
                            <ul id=\"themecolors\" class=\"m-t-20\">
                                <li><b>With Light sidebar</b></li>
                                <li><a href=\"javascript:void(0)\" theme=\"default\" class=\"default-theme\">1</a></li>
                                <li><a href=\"javascript:void(0)\" theme=\"green\" class=\"green-theme\">2</a></li>
                                <li><a href=\"javascript:void(0)\" theme=\"gray\" class=\"yellow-theme\">3</a></li>
                                <li><a href=\"javascript:void(0)\" theme=\"blue\" class=\"blue-theme working\">4</a></li>
                                <li><a href=\"javascript:void(0)\" theme=\"purple\" class=\"purple-theme\">5</a></li>
                                <li><a href=\"javascript:void(0)\" theme=\"megna\" class=\"megna-theme\">6</a></li>
                                <li><b>With Dark sidebar</b></li>
                                <br/>
                                <li><a href=\"javascript:void(0)\" theme=\"default-dark\"
                                       class=\"default-dark-theme\">7</a></li>
                                <li><a href=\"javascript:void(0)\" theme=\"green-dark\" class=\"green-dark-theme\">8</a>
                                </li>
                                <li><a href=\"javascript:void(0)\" theme=\"gray-dark\" class=\"yellow-dark-theme\">9</a>
                                </li>

                                <li><a href=\"javascript:void(0)\" theme=\"blue-dark\" class=\"blue-dark-theme\">10</a>
                                </li>
                                <li><a href=\"javascript:void(0)\" theme=\"purple-dark\"
                                       class=\"purple-dark-theme\">11</a></li>
                                <li><a href=\"javascript:void(0)\" theme=\"megna-dark\" class=\"megna-dark-theme\">12</a>
                                </li>
                            </ul>
                            <ul class=\"m-t-20 chatonline\">
                                <li><b>Chat option</b></li>
                                <li><a href=\"javascript:void(0)\"><img
                                                src=\"{{ asset('plugins/images/users/varun.jpg') }}\"
                                                alt=\"user-img\" class=\"img-circle\"> <span>Varun Dhavan <small
                                                    class=\"text-success\">online</small></span></a></li>
                                <li><a href=\"javascript:void(0)\"><img
                                                src=\"{{ asset('plugins/images/users/genu.jpg') }}\"
                                                alt=\"user-img\" class=\"img-circle\"> <span>Genelia Deshmukh <small
                                                    class=\"text-warning\">Away</small></span></a></li>
                                <li><a href=\"javascript:void(0)\"><img
                                                src=\"{{ asset('plugins/images/users/ritesh.jpg') }}\"
                                                alt=\"user-img\" class=\"img-circle\"> <span>Ritesh Deshmukh <small
                                                    class=\"text-danger\">Busy</small></span></a></li>
                                <li><a href=\"javascript:void(0)\"><img
                                                src=\"{{ asset('plugins/images/users/arijit.jpg') }}\"
                                                alt=\"user-img\" class=\"img-circle\"> <span>Arijit Sinh <small
                                                    class=\"text-muted\">Offline</small></span></a></li>
                                <li><a href=\"javascript:void(0)\"><img
                                                src=\"{{ asset('plugins/images/users/govinda.jpg') }}\"
                                                alt=\"user-img\" class=\"img-circle\"> <span>Govinda Star <small
                                                    class=\"text-success\">online</small></span></a></li>
                                <li><a href=\"javascript:void(0)\"><img
                                                src=\"{{ asset('plugins/images/users/hritik.jpg') }}\"
                                                alt=\"user-img\" class=\"img-circle\"> <span>John Abraham<small
                                                    class=\"text-success\">online</small></span></a></li>
                                <li><a href=\"javascript:void(0)\"><img
                                                src=\"{{ asset('plugins/images/users/john.jpg') }}\"
                                                alt=\"user-img\" class=\"img-circle\"> <span>Hritik Roshan<small
                                                    class=\"text-success\">online</small></span></a></li>
                                <li><a href=\"javascript:void(0)\"><img
                                                src=\"{{ asset('plugins/images/users/pawandeep.jpg') }}\"
                                                alt=\"user-img\" class=\"img-circle\"> <span>Pwandeep rajan <small
                                                    class=\"text-success\">online</small></span></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- /.right-sidebar -->
            </div>
            <!-- /.container-fluid -->
            {% block footer %}
                {{ parent() }}
            {% endblock %}
        </div>
        <!-- /#page-wrapper -->
    </div>
    </div>
{% endblock %}
{% block javascripts %}
    {{ parent() }}
    <script src=\"{{ asset('datatable/js/jquery.metisMenu.js') }}\"></script>
    <script src=\"{{ asset('datatable/js/jquery-ui.js') }}\"></script>
    <script src=\"{{ asset('datatable/js/jquery.blockUI.js') }}\"></script>

    <!--Functions Js-->
    <script src=\"{{ asset('datatable/js/functions.js') }}\"></script>

    <script src=\"{{ asset('datatable/js/loader.js') }}\"></script>

    <script src=\"{{ asset('datatable/js/jquery.dataTables.min.js') }}\"></script>
    <script src=\"{{ asset('datatable/js/dataTables.bootstrap.min.js') }}\"></script>
    <script src=\"{{ asset('datatable/js/jszip.min.js') }}\"></script>
    <script src=\"{{ asset('datatable/js/pdfmake.min.js') }}\"></script>
    {#<script src=\"{{ asset('datatable/js/vfs_fonts.js') }}\"></script>#}
    <script src=\"{{ asset('datatable/js/buttons.html5.js') }}\"></script>
    <script src=\"{{ asset('datatable/js/buttons.colVis.js') }}\"></script>
    <script src=\"{{ asset('datatable/js/dataTables-script.js') }}\"></script>
    <script src=\"{{ asset('plugins/bower_components/bootstrap-rtl-master/dist/js/bootstrap-rtl.min.js') }}\"></script>
    <script src=\"https://code.jquery.com/jquery-3.1.1.min.js\"></script>
    <script src=\"https://code.highcharts.com/highcharts.js\"></script>
    <script src=\"https://code.highcharts.com/modules/exporting.js\"></script>
    <script>
        var chart = new Highcharts.Chart({

            chart: {
                renderTo: 'container_barStats',
                type: 'column',
                style: {
                    fontFamily: 'Droid Arabic Kufi, serif'
                }
            },

            xAxis: {
                categories: [
                    'المواعيد المؤكدة',
                    'المواعيد المنتهية',
                    'المواعيد المعلقة',
                    'المواعيد المرفوضة',
                ],
                reversed: true,
                title:{
                    style: {
                        fontFamily: 'Droid Arabic Kufi, serif'
                    }
                }
            },

            yAxis: {
                title: {
                    text: 'عدد المواعيد',
                    useHTML: Highcharts.hasBidiBug,
                    style: {
                        fontFamily: 'Droid Arabic Kufi, serif'
                    }
                },
                opposite: true
            },

            title: {
                text: 'احصائيات  حالات المواعيد',
                useHTML: Highcharts.hasBidiBug,
                style: {
                    fontFamily: 'Droid Arabic Kufi, serif'
                }
            },

            legend: {
                useHTML: Highcharts.hasBidiBug,
                style: {
                    fontFamily: 'Droid Arabic Kufi, serif'
                }
            },

            tooltip: {
                useHTML: true,
                style: {
                    fontFamily: 'Droid Arabic Kufi, serif'
                }
            },

            series: [{
                showInLegend: false,
                name: '',
                data: [{{ appointmentConfirmedNumbers }},{{ appointmentExceededNumbers }},{{ appointmentWaintingNumbers }},{{ appointmnentUnconfirmedNumbers }}]
            }]

        });

    </script>
    <script>
        var chart = new Highcharts.Chart({

            chart: {
                renderTo: 'container_barStatss',
                type: 'column',
                style: {
                    fontFamily: 'Droid Arabic Kufi, serif'
                }
            },

            xAxis: {
                categories: [
                    {% for speciality,value in doctorsSpecialities %}

                        \"{{ speciality }}\",


                    {% endfor %}

                ],
                reversed: true
            },

            yAxis: {
                title: {
                    text: 'عدد الأطباء',
                    useHTML: Highcharts.hasBidiBug,
                    style: {
                        fontFamily: 'Droid Arabic Kufi, serif'
                    }
                },
                opposite: true
            },

            title: {
                text: 'احصائيات  الأطباء حسب الاختصاص',
                useHTML: Highcharts.hasBidiBug,
                style: {
                    fontFamily: 'Droid Arabic Kufi, serif'
                }
            },

            legend: {
                useHTML: Highcharts.hasBidiBug,
                style: {
                    fontFamily: 'Droid Arabic Kufi, serif'
                }
            },

            tooltip: {
                useHTML: true,
                style: {
                    fontFamily: 'Droid Arabic Kufi, serif'
                }
            },

            series: [{
                showInLegend: false,
                name: '',
                data: [{% for speciality,value in doctorsSpecialities %}
                    {{ value }},
                    {% endfor %}]
            }]

        });
    </script>
    <script>
        \$(document).ready(function () {

            // Build the chart
            Highcharts.chart('container', {
                chart: {
                    renderTo: 'container',
                    type: 'column'
                },
                title: {
                    text: 'الاحصائيات التي تخص المواعيد',
                    style: {
                        fontSize: '13px',
                        fontFamily: 'DroidKufiRegular, Verdana, sans-serif'
                    }
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                },
                xAxis: {
                    reversed:true
                },
                yAxis: {
                    opposite:true
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            style: {
                                fontSize: '7px',
                                fontFamily: 'DroidKufiRegular, Verdana, sans-serif'
                            },
                        },
                        showInLegend: true
                    }
                },
                series: [{
                    name: 'النسبة',
                    colorByPoint: true,
                    style: {
                        fontSize: '13px',
                        fontFamily: 'DroidKufiRegular, Verdana, sans-serif'
                    },
                    data: [{
                        name: 'المواعيد المؤكدة',
                        y: {{ confirmedPercentAppointment }},
                    }, {
                        name: 'المواعيد المنتهية',
                        y: {{ exceededPercentAppointment }},
                        sliced: true,
                        selected: true
                    }, {
                        name: 'المواعيد المعلقة',
                        y: {{ waitingPercentAppointment }}
                    }, {
                        name: 'المواعيد المرفوضة',
                        y: {{ unconfirmedPercentAppointment }},
                    },
                    ]
                }]
            });
        });
    </script>
    <script>
        Highcharts.chart('container_barStaats', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'الاحصائيات التي تخص المواعيد',
                style: {
                    fontSize: '13px',
                    fontFamily: 'DroidKufiRegular, Verdana, sans-serif'
                }
            },
            subtitle: {
                text: 'التصنيف حسب الحالة'
            },
            xAxis: {
                type: 'category',
                labels: {
                    rotation: -45,
                    style: {
                        fontSize: '13px',
                        fontFamily: 'DroidKufiRegular, Verdana, sans-serif '
                    }
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'عدد المواعيد'
                },
                style: {
                    fontSize: '13px',
                    fontFamily: 'DroidKufiRegular, Verdana, sans-serif'
                }
            },
            legend: {
                enabled: false
            },
            tooltip: {
                pointFormat: 'العدد'
            },
            series: [{
                name: 'المواعيد',
                data: [
                    ['المؤكدة', {{ appointmentConfirmedNumbers }}],
                    ['المرفوضة', {{ appointmnentUnconfirmedNumbers }}],
                    ['المعلقة', {{ appointmentWaintingNumbers }}],
                    ['المنتهية', {{ appointmentExceededNumbers }}],
                    /*
                     ['Mexico City', 8.9],
                     ['Lima', 8.9]*/
                ],
                dataLabels: {
                    enabled: true,
                    rotation: -90,
                    color: '#FFFFFF',
                    align: 'right',
                    format: '{point.y:.1f}', // one decimal
                    y: 10, // 10 pixels down from the top
                    style: {
                        fontSize: '13px',
                        fontFamily: 'DroidKufiRegular, Verdana, sans-serif'
                    }
                }
            }]
        });
    </script>
    <script src=\"https://www.amcharts.com/lib/3/amcharts.js\"></script>
    <script src=\"https://www.amcharts.com/lib/3/pie.js\"></script>
    <script src=\"https://www.amcharts.com/lib/3/serial.js\"></script>
    <script src=\"https://www.amcharts.com/lib/3/plugins/export/export.min.js\"></script>
    <link rel=\"stylesheet\" href=\"https://www.amcharts.com/lib/3/plugins/export/export.css\" type=\"text/css\" media=\"all\"/>
    <script src=\"https://www.amcharts.com/lib/3/themes/light.js\"></script>

    <!-- Chart code -->
    <script>

        var chart = AmCharts.makeChart(\"chartdiv\", {
            \"theme\": \"light\",
            \"type\": \"serial\",
            \"dataProvider\": [
                {% for speciality,value in doctorsSpecialities %}
                {
                    \"country\": \"{{ speciality }}\",
                    \"visits\": \"{{ value }}\",
                    \"color\": \"FCD202\"
                },
                {% endfor %}
            ],
            \"valueAxes\": [{
                \"position\": \"right\",
                \"gridColor\": \"#FFFFFF\",
                \"title\": \"عدد الاطباء\",
                \"gridAlpha\": 0.5,
                \"dashLength\": 0
            }],
            \"gridAboveGraphs\": true,
            \"startDuration\": 1,
            \"graphs\": [{
                \"balloonText\": \"[[category]]: <b>[[value]]</b>\",
                \"fillAlphas\": 0.8,
                \"lineAlpha\": 0.2,
                \"type\": \"column\",
                \"valueField\": \"visits\"
            }],
            \"chartCursor\": {
                \"categoryBalloonEnabled\": false,
                \"cursorAlpha\": 0,
                \"zoomable\": false
            },
            \"categoryField\": \"country\",
            \"categoryAxis\": {
                \"gridPosition\": \"start\",
                \"gridAlpha\": 0,
                \"tickPosition\": \"start\",
                \"tickLength\": 20
            },
            \"export\": {
                \"enabled\": true
            }


        }, 0);
    </script>
    <script>
        var chart = AmCharts.makeChart(\"chartdivv\", {
            \"type\": \"pie\",
            \"theme\": \"light\",
            \"dataProvider\": [{
                \"country\": \"المواعيد المعلقة\",
                \"value\": {{ appointmentWaintingNumbers }}
            }, {
                \"country\": \"المواعيد المنتهية\",
                \"value\": {{ appointmentExceededNumbers }}
            }, {
                \"country\": \"المواعيد المرفوضة\",
                \"value\": {{ appointmnentUnconfirmedNumbers }}
            }, {
                \"country\": \"المواعيد المؤكدة\",
                \"value\": {{ appointmentConfirmedNumbers }}
            }],
            \"valueField\": \"value\",
            \"titleField\": \"country\",
            \"outlineAlpha\": 0.4,
            \"depth3D\": 15,
            \"balloonText\": \"[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>\",
            \"angle\": 30,
            \"export\": {
                \"enabled\": true
            }
        });
    </script>
{% endblock %}
", "DoctorsAdminBundle:Admin:index.html.twig", "C:\\wamp64\\www\\doctorAdmin\\src\\Doctors\\AdminBundle/Resources/views/Admin/index.html.twig");
    }
}
