<?php

/* DoctorsAdminBundle:Doctor:doctors.html.twig */
class __TwigTemplate_08a0ebec80a87769d2cf33d67a0a673a09c88d9d4c6fa93abd1768b63e69d624 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::layoutAdmin.html.twig", "DoctorsAdminBundle:Doctor:doctors.html.twig", 1);
        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::layoutAdmin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_53abbe0acb21731bcd2d07be10a8ade7ab9549e007c48e436e3c23bcfa38fef6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_53abbe0acb21731bcd2d07be10a8ade7ab9549e007c48e436e3c23bcfa38fef6->enter($__internal_53abbe0acb21731bcd2d07be10a8ade7ab9549e007c48e436e3c23bcfa38fef6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "DoctorsAdminBundle:Doctor:doctors.html.twig"));

        $__internal_8e5669dc9e31ac18f244d3d0ad5154fd1e0947504f6490c010b4abee00805921 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8e5669dc9e31ac18f244d3d0ad5154fd1e0947504f6490c010b4abee00805921->enter($__internal_8e5669dc9e31ac18f244d3d0ad5154fd1e0947504f6490c010b4abee00805921_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "DoctorsAdminBundle:Doctor:doctors.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_53abbe0acb21731bcd2d07be10a8ade7ab9549e007c48e436e3c23bcfa38fef6->leave($__internal_53abbe0acb21731bcd2d07be10a8ade7ab9549e007c48e436e3c23bcfa38fef6_prof);

        
        $__internal_8e5669dc9e31ac18f244d3d0ad5154fd1e0947504f6490c010b4abee00805921->leave($__internal_8e5669dc9e31ac18f244d3d0ad5154fd1e0947504f6490c010b4abee00805921_prof);

    }

    // line 2
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_9ee4ad76c87dfdb53cbc89943384b63e770416fe8012a23a80f8eed8c29547ac = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9ee4ad76c87dfdb53cbc89943384b63e770416fe8012a23a80f8eed8c29547ac->enter($__internal_9ee4ad76c87dfdb53cbc89943384b63e770416fe8012a23a80f8eed8c29547ac_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_a94a084570ffcbafaa5d2506a89d0a9ff836d5b4c9ae8d2bcc86364c0c5aaf37 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a94a084570ffcbafaa5d2506a89d0a9ff836d5b4c9ae8d2bcc86364c0c5aaf37->enter($__internal_a94a084570ffcbafaa5d2506a89d0a9ff836d5b4c9ae8d2bcc86364c0c5aaf37_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 3
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    ";
        // line 5
        echo "    <!-- Integral core stylesheet -->
    ";
        // line 7
        echo "    <!-- /integral core stylesheet -->

    <!--Jvector Map-->
    ";
        // line 11
        echo "
    ";
        // line 13
        echo "
    <!-- Bootstrap RTL stylesheet min version -->
    ";
        // line 16
        echo "    <!-- /bootstrap rtl stylesheet min version -->

    <!-- Integral RTL core stylesheet -->
    ";
        // line 20
        echo "
    ";
        // line 22
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("datatable/jquery.dataTables.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
    <link href=\"";
        // line 23
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("datatable/buttons.dataTables.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">

    <link href=\"";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("datatable/select2.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">

    <link href=\"";
        // line 27
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("datatable/bootstrap-toggle.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
";
        
        $__internal_a94a084570ffcbafaa5d2506a89d0a9ff836d5b4c9ae8d2bcc86364c0c5aaf37->leave($__internal_a94a084570ffcbafaa5d2506a89d0a9ff836d5b4c9ae8d2bcc86364c0c5aaf37_prof);

        
        $__internal_9ee4ad76c87dfdb53cbc89943384b63e770416fe8012a23a80f8eed8c29547ac->leave($__internal_9ee4ad76c87dfdb53cbc89943384b63e770416fe8012a23a80f8eed8c29547ac_prof);

    }

    // line 30
    public function block_body($context, array $blocks = array())
    {
        $__internal_93bc9873cf350ffa2b786c533eccb54c5b74f752608155c17df3b9305d6c559b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_93bc9873cf350ffa2b786c533eccb54c5b74f752608155c17df3b9305d6c559b->enter($__internal_93bc9873cf350ffa2b786c533eccb54c5b74f752608155c17df3b9305d6c559b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_d523d4c565531e63bcd341b33a975596828b60282f480768a9670314b33be613 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d523d4c565531e63bcd341b33a975596828b60282f480768a9670314b33be613->enter($__internal_d523d4c565531e63bcd341b33a975596828b60282f480768a9670314b33be613_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 31
        echo "    <div id=\"page-wrapper\">
        <div class=\"container-fluid\">
            <div class=\"row bg-title\">
                <div class=\"col-lg-3 col-md-4 col-sm-4 col-xs-12\">
                    <h4 class=\"page-title arabe\">الأطباء</h4>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <div class=\"row\">
                <div class=\"col-lg-12\">
                    <div class=\"panel panel-default\">
                        <div class=\"panel-heading clearfix\">
                            <h4 class=\"panel-title arabe\">قائمة الأطباء</h4>
                        </div>

                        <div class=\"panel-body\">
                            ";
        // line 47
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashbag", array()), "get", array(0 => "msg"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 48
            echo "                                <br><br>
                                <div class='alert alert-success alert-dismissable arabe'>
                                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;
                                    </button>
                                    ";
            // line 52
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "
                                </div>

                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 56
        echo "                            <table class=\"table table-striped table-bordered table-hover dataTables-example\">
                                <thead>
                                <tr>
                                    <th class=\"arabe\">صورة الطبيب</th>
                                    <th class=\"arabe\">اسم الطبيب</th>
                                    <th class=\"arabe\">رقم الجوال</th>
                                    <th class=\"arabe\">الاختصاص</th>
                                    <th class=\"arabe\">الجنسية</th>
                                    <th class=\"arabe\">عرض المعلومات</th>
                                    <th class=\"arabe\">تعديل</th>
                                    <th class=\"arabe\">حذف</th>
                                </tr>
                                </thead>
                                <tbody>
                                ";
        // line 70
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["doctors"]) ? $context["doctors"] : $this->getContext($context, "doctors")));
        foreach ($context['_seq'] as $context["_key"] => $context["doctor"]) {
            // line 71
            echo "                                    ";
            if (($this->getAttribute($context["doctor"], "isActiveDoctor", array()) == 1)) {
                // line 72
                echo "                                        <tr class=\"gradeX arabe\">
                                            <td align=\"center\">
                                                <a href=\"#\" id=\"show-";
                // line 74
                echo twig_escape_filter($this->env, $this->getAttribute($context["doctor"], "id", array()), "html", null, true);
                echo "\" data-toggle=\"modal\"
                                                   data-target=\"#show-modal-";
                // line 75
                echo twig_escape_filter($this->env, $this->getAttribute($context["doctor"], "id", array()), "html", null, true);
                echo "\">
                                                    <img src=\"";
                // line 76
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl(("uploads/doctors/" . $this->getAttribute($context["doctor"], "photoDoctor", array()))), "html", null, true);
                echo "\" class=\"thumbnail image\"/>
                                                </a>
                                                <div class=\"modal fade\" id=\"show-modal-";
                // line 78
                echo twig_escape_filter($this->env, $this->getAttribute($context["doctor"], "id", array()), "html", null, true);
                echo "\" tabindex=\"-1\"
                                                     role=\"dialog\" aria-labelledby=\"myModalLabel\"
                                                     aria-hidden=\"true\">
                                                    <div class=\"modal-dialog\">
                                                        <div class=\"modal-content\">

                                                            <div class=\"modal-header\">
                                                                <button type=\"button\" class=\"close\"
                                                                        data-dismiss=\"modal\" aria-hidden=\"true\">
                                                                    &times;
                                                                </button>
                                                                <h4 class=\"modal-title arabe\" id=\"myModalLabel\"> اظهار
                                                                    الصورة</h4>
                                                            </div>

                                                            <div class=\"modal-body\">
                                                                <center>
                                                                    <img src=\"";
                // line 95
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl(("uploads/doctors/" . $this->getAttribute($context["doctor"], "photoDoctor", array()))), "html", null, true);
                echo "\"
                                                                         width=\"500\" height=\"300\" id=\"large-img-";
                // line 96
                echo twig_escape_filter($this->env, $this->getAttribute($context["doctor"], "id", array()), "html", null, true);
                echo "\">
                                                                </center>
                                                            </div>

                                                            <div class=\"modal-footer\">
                                                                <button type=\"button\" class=\"btn btn-default arabe\"
                                                                        data-dismiss=\"modal\"> رجوع
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>";
                // line 109
                echo twig_escape_filter($this->env, $this->getAttribute($context["doctor"], "nameDoctor", array()), "html", null, true);
                echo "</td>
                                            <td>";
                // line 110
                echo twig_escape_filter($this->env, $this->getAttribute($context["doctor"], "phoneNumberDoctor", array()), "html", null, true);
                echo "</td>
                                            <td>";
                // line 111
                if ( !$this->getAttribute($this->getAttribute($context["doctor"], "speciality", array(), "any", false, true), "nameSpeciality", array(), "any", true, true)) {
                    // line 112
                    echo "                                                    طبيب عام
                                                ";
                } else {
                    // line 114
                    echo "                                                    ";
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["doctor"], "speciality", array()), "nameSpeciality", array()), "html", null, true);
                    echo "
                                                ";
                }
                // line 115
                echo "</td>
                                            <td>";
                // line 116
                echo twig_escape_filter($this->env, $this->getAttribute($context["doctor"], "nationality", array()), "html", null, true);
                echo "</td>
                                            <td align=\"center\">
                                                <a  href=\"";
                // line 118
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("doctors_admin_showDoctor", array("id" => $this->getAttribute($context["doctor"], "id", array()))), "html", null, true);
                echo "\" class=\"btn btn-info\">
                                                    <i class=\"fa fa-search\"></i>
                                                </a>
                                            </td>
                                            <td align=\"center\">
                                                <a href=\"";
                // line 123
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("doctors_admin_editDoctor", array("id" => $this->getAttribute($context["doctor"], "id", array()))), "html", null, true);
                echo "\"
                                                   class=\"btn btn-primary\">
                                                    <i class=\"fa fa-refresh\"></i>
                                                </a>
                                            </td>

                                            <td align=\"center\">
                                                <a href=\"#\" id=\"remove-customer-";
                // line 130
                echo twig_escape_filter($this->env, $this->getAttribute($context["doctor"], "id", array()), "html", null, true);
                echo "\" class=\"btn btn-danger\"
                                                   data-toggle=\"modal\" data-target=\"#confirm-delete-";
                // line 131
                echo twig_escape_filter($this->env, $this->getAttribute($context["doctor"], "id", array()), "html", null, true);
                echo "\"><i
                                                            class=\"fa fa-remove\"></i> </a>

                                            </td>

                                            <!-- Delete Modal -->
                                            <div class=\"modal fade\" id=\"confirm-delete-";
                // line 137
                echo twig_escape_filter($this->env, $this->getAttribute($context["doctor"], "id", array()), "html", null, true);
                echo "\" tabindex=\"-1\"
                                                 role=\"dialog\"
                                                 aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
                                                <div class=\"modal-dialog\">
                                                    <div class=\"modal-content\">
                                                        <div class=\"modal-header\">
                                                            <button type=\"button\" class=\"close\" data-dismiss=\"modal\"
                                                                    aria-hidden=\"true\">&times;
                                                            </button>
                                                            <h4 class=\"modal-title arabe\" id=\"myModalLabel\"> تاكيد الحذف </h4>
                                                        </div>

                                                        <div class=\"modal-body\">
                                                            <p class=\"arabe\">
                                                                هل تريد حذف الطبيب :
                                                                <br>
                                                                <strong class=\"arabe\">
                                                                    ";
                // line 154
                echo twig_escape_filter($this->env, $this->getAttribute($context["doctor"], "nameDoctor", array()), "html", null, true);
                echo "
                                                                </strong>
                                                            </p>
                                                        </div>

                                                        <div class=\"modal-footer\">
                                                            <button type=\"button\" class=\"btn btn-default arabe\"
                                                                    data-dismiss=\"modal\">
                                                                رجوع
                                                            </button>
                                                            <a href=\"";
                // line 164
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("doctors_admin_deleteDoctor", array("id" => $this->getAttribute($context["doctor"], "id", array()))), "html", null, true);
                echo "\"
                                                               class=\"btn btn-danger arabe\" id=\"btn-remove\"> حذف</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </tr>
                                    ";
            }
            // line 173
            echo "                                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['doctor'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 174
        echo "
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th class=\"arabe\">صورة الطبيب</th>
                                    <th class=\"arabe\">اسم الطبيب</th>
                                    <th class=\"arabe\">رقم الجوال</th>
                                    <th class=\"arabe\">الاختصاص</th>
                                    <th class=\"arabe\">الجنسية</th>
                                    <th class=\"arabe\">عرض المعلومات</th>
                                    <th class=\"arabe\">تعديل</th>
                                    <th class=\"arabe\">حذف</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>

                </div>

            </div>


        </div>

        <!-- /main content -->
        ";
        // line 203
        echo "    </div>
    <!-- /page-wrapper -->
";
        
        $__internal_d523d4c565531e63bcd341b33a975596828b60282f480768a9670314b33be613->leave($__internal_d523d4c565531e63bcd341b33a975596828b60282f480768a9670314b33be613_prof);

        
        $__internal_93bc9873cf350ffa2b786c533eccb54c5b74f752608155c17df3b9305d6c559b->leave($__internal_93bc9873cf350ffa2b786c533eccb54c5b74f752608155c17df3b9305d6c559b_prof);

    }

    // line 206
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_e22eaf4ffd5e61d8945591286834c3b7497404bfd95266a8085d460ecad7dbae = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e22eaf4ffd5e61d8945591286834c3b7497404bfd95266a8085d460ecad7dbae->enter($__internal_e22eaf4ffd5e61d8945591286834c3b7497404bfd95266a8085d460ecad7dbae_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_c45f22ac92e435cc0610ecdb87932aa253550d6808eb189ca056d195f43e9024 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c45f22ac92e435cc0610ecdb87932aa253550d6808eb189ca056d195f43e9024->enter($__internal_c45f22ac92e435cc0610ecdb87932aa253550d6808eb189ca056d195f43e9024_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 207
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    <script src=\"";
        // line 208
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("datatable/js/jquery.metisMenu.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 209
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("datatable/js/jquery-ui.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 210
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("datatable/js/jquery.blockUI.js"), "html", null, true);
        echo "\"></script>

    <!--Functions Js-->
    <script src=\"";
        // line 213
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("datatable/js/functions.js"), "html", null, true);
        echo "\"></script>

    <script src=\"";
        // line 215
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("datatable/js/loader.js"), "html", null, true);
        echo "\"></script>

    <script src=\"";
        // line 217
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("datatable/js/jquery.dataTables.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 218
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("datatable/js/dataTables.bootstrap.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 219
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("datatable/js/jszip.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 220
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("datatable/js/pdfmake.min.js"), "html", null, true);
        echo "\"></script>
    ";
        // line 222
        echo "    <script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("datatable/js/buttons.html5.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 223
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("datatable/js/buttons.colVis.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 224
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("datatable/js/dataTables-script.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 225
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("plugins/bower_components/bootstrap-rtl-master/dist/js/bootstrap-rtl.min.js"), "html", null, true);
        echo "\"></script>
";
        
        $__internal_c45f22ac92e435cc0610ecdb87932aa253550d6808eb189ca056d195f43e9024->leave($__internal_c45f22ac92e435cc0610ecdb87932aa253550d6808eb189ca056d195f43e9024_prof);

        
        $__internal_e22eaf4ffd5e61d8945591286834c3b7497404bfd95266a8085d460ecad7dbae->leave($__internal_e22eaf4ffd5e61d8945591286834c3b7497404bfd95266a8085d460ecad7dbae_prof);

    }

    public function getTemplateName()
    {
        return "DoctorsAdminBundle:Doctor:doctors.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  450 => 225,  446 => 224,  442 => 223,  437 => 222,  433 => 220,  429 => 219,  425 => 218,  421 => 217,  416 => 215,  411 => 213,  405 => 210,  401 => 209,  397 => 208,  392 => 207,  383 => 206,  371 => 203,  343 => 174,  337 => 173,  325 => 164,  312 => 154,  292 => 137,  283 => 131,  279 => 130,  269 => 123,  261 => 118,  256 => 116,  253 => 115,  247 => 114,  243 => 112,  241 => 111,  237 => 110,  233 => 109,  217 => 96,  213 => 95,  193 => 78,  188 => 76,  184 => 75,  180 => 74,  176 => 72,  173 => 71,  169 => 70,  153 => 56,  143 => 52,  137 => 48,  133 => 47,  115 => 31,  106 => 30,  94 => 27,  89 => 25,  84 => 23,  79 => 22,  76 => 20,  71 => 16,  67 => 13,  64 => 11,  59 => 7,  56 => 5,  51 => 3,  42 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"::layoutAdmin.html.twig\" %}
{% block stylesheets %}
    {{ parent() }}
    {#<link rel=\"stylesheet\" href=\"{{ asset('bootstrap/dist/css/bootstrap.min.css') }}\">#}
    <!-- Integral core stylesheet -->
    {#<link href=\"{{ asset('datatable/integral-core.css') }}\" rel=\"stylesheet\">#}
    <!-- /integral core stylesheet -->

    <!--Jvector Map-->
    {#<link href=\"{{ asset('datatable/jquery-jvectormap-2.0.3.css') }}\" rel=\"stylesheet\">#}

    {#<link href=\"{{ asset('datatable/integral-forms.css') }}\" rel=\"stylesheet\">#}

    <!-- Bootstrap RTL stylesheet min version -->
    {# <link href=\"{{ asset('datatable/bootstrap-rtl.min.css') }}\" rel=\"stylesheet\">#}
    <!-- /bootstrap rtl stylesheet min version -->

    <!-- Integral RTL core stylesheet -->
    {#<link href=\"{{ asset('datatable/integral-rtl-core.css') }}\" rel=\"stylesheet\">#}

    {#<link href=\"{{ asset('datatable/integral-forms.css') }}\" rel=\"stylesheet\">#}
    <link href=\"{{ asset('datatable/jquery.dataTables.css') }}\" rel=\"stylesheet\">
    <link href=\"{{ asset('datatable/buttons.dataTables.css') }}\" rel=\"stylesheet\">

    <link href=\"{{ asset('datatable/select2.css') }}\" rel=\"stylesheet\">

    <link href=\"{{ asset('datatable/bootstrap-toggle.min.css') }}\" rel=\"stylesheet\">
{% endblock %}

{% block body %}
    <div id=\"page-wrapper\">
        <div class=\"container-fluid\">
            <div class=\"row bg-title\">
                <div class=\"col-lg-3 col-md-4 col-sm-4 col-xs-12\">
                    <h4 class=\"page-title arabe\">الأطباء</h4>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <div class=\"row\">
                <div class=\"col-lg-12\">
                    <div class=\"panel panel-default\">
                        <div class=\"panel-heading clearfix\">
                            <h4 class=\"panel-title arabe\">قائمة الأطباء</h4>
                        </div>

                        <div class=\"panel-body\">
                            {% for flashMessage in app.session.flashbag.get('msg') %}
                                <br><br>
                                <div class='alert alert-success alert-dismissable arabe'>
                                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;
                                    </button>
                                    {{ flashMessage }}
                                </div>

                            {% endfor %}
                            <table class=\"table table-striped table-bordered table-hover dataTables-example\">
                                <thead>
                                <tr>
                                    <th class=\"arabe\">صورة الطبيب</th>
                                    <th class=\"arabe\">اسم الطبيب</th>
                                    <th class=\"arabe\">رقم الجوال</th>
                                    <th class=\"arabe\">الاختصاص</th>
                                    <th class=\"arabe\">الجنسية</th>
                                    <th class=\"arabe\">عرض المعلومات</th>
                                    <th class=\"arabe\">تعديل</th>
                                    <th class=\"arabe\">حذف</th>
                                </tr>
                                </thead>
                                <tbody>
                                {% for doctor in doctors %}
                                    {% if doctor.isActiveDoctor == 1 %}
                                        <tr class=\"gradeX arabe\">
                                            <td align=\"center\">
                                                <a href=\"#\" id=\"show-{{ doctor.id }}\" data-toggle=\"modal\"
                                                   data-target=\"#show-modal-{{ doctor.id }}\">
                                                    <img src=\"{{ asset('uploads/doctors/'~doctor.photoDoctor) }}\" class=\"thumbnail image\"/>
                                                </a>
                                                <div class=\"modal fade\" id=\"show-modal-{{ doctor.id }}\" tabindex=\"-1\"
                                                     role=\"dialog\" aria-labelledby=\"myModalLabel\"
                                                     aria-hidden=\"true\">
                                                    <div class=\"modal-dialog\">
                                                        <div class=\"modal-content\">

                                                            <div class=\"modal-header\">
                                                                <button type=\"button\" class=\"close\"
                                                                        data-dismiss=\"modal\" aria-hidden=\"true\">
                                                                    &times;
                                                                </button>
                                                                <h4 class=\"modal-title arabe\" id=\"myModalLabel\"> اظهار
                                                                    الصورة</h4>
                                                            </div>

                                                            <div class=\"modal-body\">
                                                                <center>
                                                                    <img src=\"{{ asset('uploads/doctors/'~doctor.photoDoctor) }}\"
                                                                         width=\"500\" height=\"300\" id=\"large-img-{{ doctor.id }}\">
                                                                </center>
                                                            </div>

                                                            <div class=\"modal-footer\">
                                                                <button type=\"button\" class=\"btn btn-default arabe\"
                                                                        data-dismiss=\"modal\"> رجوع
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>{{ doctor.nameDoctor }}</td>
                                            <td>{{ doctor.phoneNumberDoctor }}</td>
                                            <td>{% if  doctor.speciality.nameSpeciality is not defined  %}
                                                    طبيب عام
                                                {% else %}
                                                    {{ doctor.speciality.nameSpeciality }}
                                                {% endif %}</td>
                                            <td>{{ doctor.nationality }}</td>
                                            <td align=\"center\">
                                                <a  href=\"{{ path('doctors_admin_showDoctor', {'id':doctor.id}) }}\" class=\"btn btn-info\">
                                                    <i class=\"fa fa-search\"></i>
                                                </a>
                                            </td>
                                            <td align=\"center\">
                                                <a href=\"{{ path('doctors_admin_editDoctor', {'id':doctor.id}) }}\"
                                                   class=\"btn btn-primary\">
                                                    <i class=\"fa fa-refresh\"></i>
                                                </a>
                                            </td>

                                            <td align=\"center\">
                                                <a href=\"#\" id=\"remove-customer-{{ doctor.id }}\" class=\"btn btn-danger\"
                                                   data-toggle=\"modal\" data-target=\"#confirm-delete-{{ doctor.id }}\"><i
                                                            class=\"fa fa-remove\"></i> </a>

                                            </td>

                                            <!-- Delete Modal -->
                                            <div class=\"modal fade\" id=\"confirm-delete-{{ doctor.id }}\" tabindex=\"-1\"
                                                 role=\"dialog\"
                                                 aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
                                                <div class=\"modal-dialog\">
                                                    <div class=\"modal-content\">
                                                        <div class=\"modal-header\">
                                                            <button type=\"button\" class=\"close\" data-dismiss=\"modal\"
                                                                    aria-hidden=\"true\">&times;
                                                            </button>
                                                            <h4 class=\"modal-title arabe\" id=\"myModalLabel\"> تاكيد الحذف </h4>
                                                        </div>

                                                        <div class=\"modal-body\">
                                                            <p class=\"arabe\">
                                                                هل تريد حذف الطبيب :
                                                                <br>
                                                                <strong class=\"arabe\">
                                                                    {{ doctor.nameDoctor }}
                                                                </strong>
                                                            </p>
                                                        </div>

                                                        <div class=\"modal-footer\">
                                                            <button type=\"button\" class=\"btn btn-default arabe\"
                                                                    data-dismiss=\"modal\">
                                                                رجوع
                                                            </button>
                                                            <a href=\"{{ path('doctors_admin_deleteDoctor', { 'id': doctor.id }) }}\"
                                                               class=\"btn btn-danger arabe\" id=\"btn-remove\"> حذف</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </tr>
                                    {% endif %}
                                {% endfor %}

                                </tbody>
                                <tfoot>
                                <tr>
                                    <th class=\"arabe\">صورة الطبيب</th>
                                    <th class=\"arabe\">اسم الطبيب</th>
                                    <th class=\"arabe\">رقم الجوال</th>
                                    <th class=\"arabe\">الاختصاص</th>
                                    <th class=\"arabe\">الجنسية</th>
                                    <th class=\"arabe\">عرض المعلومات</th>
                                    <th class=\"arabe\">تعديل</th>
                                    <th class=\"arabe\">حذف</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>

                </div>

            </div>


        </div>

        <!-- /main content -->
        {#{% block footer %}
            {{ parent() }}
        {% endblock %}#}
    </div>
    <!-- /page-wrapper -->
{% endblock %}
{% block javascripts %}
    {{ parent() }}
    <script src=\"{{ asset('datatable/js/jquery.metisMenu.js') }}\"></script>
    <script src=\"{{ asset('datatable/js/jquery-ui.js') }}\"></script>
    <script src=\"{{ asset('datatable/js/jquery.blockUI.js') }}\"></script>

    <!--Functions Js-->
    <script src=\"{{ asset('datatable/js/functions.js') }}\"></script>

    <script src=\"{{ asset('datatable/js/loader.js') }}\"></script>

    <script src=\"{{ asset('datatable/js/jquery.dataTables.min.js') }}\"></script>
    <script src=\"{{ asset('datatable/js/dataTables.bootstrap.min.js') }}\"></script>
    <script src=\"{{ asset('datatable/js/jszip.min.js') }}\"></script>
    <script src=\"{{ asset('datatable/js/pdfmake.min.js') }}\"></script>
    {#<script src=\"{{ asset('datatable/js/vfs_fonts.js') }}\"></script>#}
    <script src=\"{{ asset('datatable/js/buttons.html5.js') }}\"></script>
    <script src=\"{{ asset('datatable/js/buttons.colVis.js') }}\"></script>
    <script src=\"{{ asset('datatable/js/dataTables-script.js') }}\"></script>
    <script src=\"{{ asset('plugins/bower_components/bootstrap-rtl-master/dist/js/bootstrap-rtl.min.js') }}\"></script>
{% endblock %}
", "DoctorsAdminBundle:Doctor:doctors.html.twig", "C:\\wamp64\\www\\doctorAdmin\\src\\Doctors\\AdminBundle/Resources/views/Doctor/doctors.html.twig");
    }
}
