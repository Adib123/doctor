<?php

/* DoctorsAdminBundle:User:ajouterUser.html.twig */
class __TwigTemplate_2bfad3ffaf5e356b1f80a3a0d379366d50ed0ef25c88d58dd71dc94194362604 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::layoutAdmin.html.twig", "DoctorsAdminBundle:User:ajouterUser.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::layoutAdmin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_dea86860cd335c2fecb02b8ed03d8195914f2b0a5a2bf5f9c02457a9a95f285f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_dea86860cd335c2fecb02b8ed03d8195914f2b0a5a2bf5f9c02457a9a95f285f->enter($__internal_dea86860cd335c2fecb02b8ed03d8195914f2b0a5a2bf5f9c02457a9a95f285f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "DoctorsAdminBundle:User:ajouterUser.html.twig"));

        $__internal_5f812ba8154851a707ee6217a3aaa65fb3ef96291fc2008189fb5d5ed3dddec4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5f812ba8154851a707ee6217a3aaa65fb3ef96291fc2008189fb5d5ed3dddec4->enter($__internal_5f812ba8154851a707ee6217a3aaa65fb3ef96291fc2008189fb5d5ed3dddec4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "DoctorsAdminBundle:User:ajouterUser.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_dea86860cd335c2fecb02b8ed03d8195914f2b0a5a2bf5f9c02457a9a95f285f->leave($__internal_dea86860cd335c2fecb02b8ed03d8195914f2b0a5a2bf5f9c02457a9a95f285f_prof);

        
        $__internal_5f812ba8154851a707ee6217a3aaa65fb3ef96291fc2008189fb5d5ed3dddec4->leave($__internal_5f812ba8154851a707ee6217a3aaa65fb3ef96291fc2008189fb5d5ed3dddec4_prof);

    }

    // line 2
    public function block_body($context, array $blocks = array())
    {
        $__internal_2852d67d2a09919f899a979bb76081a1087d27978cca740825e0e7dcd386993c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2852d67d2a09919f899a979bb76081a1087d27978cca740825e0e7dcd386993c->enter($__internal_2852d67d2a09919f899a979bb76081a1087d27978cca740825e0e7dcd386993c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_ae0205fd1e0d232433ef530b7bec20b517f7f18a04aeaafb1f991806e39ab9fc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ae0205fd1e0d232433ef530b7bec20b517f7f18a04aeaafb1f991806e39ab9fc->enter($__internal_ae0205fd1e0d232433ef530b7bec20b517f7f18a04aeaafb1f991806e39ab9fc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 3
        echo "    <div class=\"preloader\">
        <div class=\"cssload-speeding-wheel\"></div>
    </div>
    <div id=\"page-wrapper\">
        <div class=\"container-fluid\">
            <div class=\"row bg-title\">
                <div class=\"col-lg-3 col-md-4 col-sm-4 col-xs-12\">
                    <h4 class=\"page-title arabe\"> اضافة مستخدم</h4></div>
                <!-- /.col-lg-12 -->
            </div>
            <div class=\"row\">
                <div class=\"white-box\">
                    ";
        // line 15
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start');
        echo "
                    <div class=\"form-group\">
                        ";
        // line 18
        echo "                        ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "photo", array()), 'label', array("label_attr" => array("class" => "arabe control-label"), "label" => "صورة المستخدم"));
        echo "

                        ";
        // line 21
        echo "                        ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "photo", array()), 'errors');
        echo "
                        ";
        // line 23
        echo "                        ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "photo", array()), 'widget', array("attr" => array("class" => "arabe")));
        echo "
                    </div>
                    <div class=\"form-group\">
                        ";
        // line 27
        echo "                        ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "phoneNumber", array()), 'label', array("label_attr" => array("class" => "arabe control-label"), "label" => "رقم الجوال"));
        echo "

                        ";
        // line 30
        echo "                        ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "phoneNumber", array()), 'errors');
        echo "
                        ";
        // line 32
        echo "                        ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "phoneNumber", array()), 'widget', array("attr" => array("class" => "form-control arabe")));
        echo "
                    </div>


                    <div class=\"form-group\">
                        ";
        // line 38
        echo "                        ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "name", array()), 'label', array("label_attr" => array("class" => "arabe control-label"), "label" => "اسم المستخدم"));
        echo "

                        ";
        // line 41
        echo "                        ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "name", array()), 'errors');
        echo "
                        ";
        // line 43
        echo "                        ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "name", array()), 'widget', array("attr" => array("class" => "form-control arabe")));
        echo "
                    </div>

                    <div class=\"form-group\">
                        ";
        // line 48
        echo "                        ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "country", array()), 'label', array("label_attr" => array("class" => "arabe control-label"), "label" => "البلد"));
        echo "

                        ";
        // line 51
        echo "                        ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "country", array()), 'errors');
        echo "
                        ";
        // line 53
        echo "                        ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "country", array()), 'widget', array("attr" => array("class" => "form-control arabe")));
        echo "
                    </div>
                    <div class=\"form-group\">
                        ";
        // line 57
        echo "                        ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "town", array()), 'label', array("label_attr" => array("class" => "arabe control-label"), "label" => "المدينة"));
        echo "

                        ";
        // line 60
        echo "                        ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "town", array()), 'errors');
        echo "
                        ";
        // line 62
        echo "                        ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "town", array()), 'widget', array("attr" => array("class" => "form-control arabe")));
        echo "
                    </div>
                    <div class=\"form-group\">
                        ";
        // line 66
        echo "                        ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "city", array()), 'label', array("label_attr" => array("class" => "arabe control-label"), "label" => "الحي"));
        echo "

                        ";
        // line 69
        echo "                        ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "city", array()), 'errors');
        echo "
                        ";
        // line 71
        echo "                        ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "city", array()), 'widget', array("attr" => array("class" => "form-control arabe")));
        echo "
                    </div>

                    <div class=\"form-group\">
                        ";
        // line 76
        echo "                        ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "email", array()), 'label', array("label_attr" => array("class" => "arabe control-label"), "label" => "البريد الالكتروني"));
        echo "

                        ";
        // line 79
        echo "                        ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "email", array()), 'errors');
        echo "
                        ";
        // line 81
        echo "                        ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "email", array()), 'widget', array("attr" => array("class" => "form-control arabe")));
        echo "
                    </div>

                    <div class=\"form-group\">
                        ";
        // line 89
        echo "                        ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "password", array()), 'errors');
        echo "
                        ";
        // line 91
        echo "                        ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "password", array()), 'widget', array("attr" => array("class" => "arabe")));
        echo "
                    </div>
                    <input type=\"submit\" value=\"اضافة المستخدم\" class=\"btn btn-primary btn-lg arabe\" />
                    ";
        // line 94
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "
                </div>
            </div>
        </div>
    </div>
";
        
        $__internal_ae0205fd1e0d232433ef530b7bec20b517f7f18a04aeaafb1f991806e39ab9fc->leave($__internal_ae0205fd1e0d232433ef530b7bec20b517f7f18a04aeaafb1f991806e39ab9fc_prof);

        
        $__internal_2852d67d2a09919f899a979bb76081a1087d27978cca740825e0e7dcd386993c->leave($__internal_2852d67d2a09919f899a979bb76081a1087d27978cca740825e0e7dcd386993c_prof);

    }

    // line 100
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_67859d2aa73e0529256d256e85d7ff53af9d867a8b10ef96c8485dbb7ad03a92 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_67859d2aa73e0529256d256e85d7ff53af9d867a8b10ef96c8485dbb7ad03a92->enter($__internal_67859d2aa73e0529256d256e85d7ff53af9d867a8b10ef96c8485dbb7ad03a92_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_94fd7f0bf8210876c3d92c9b5b26d48521ff21d924f876440a8cb5448a9d218f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_94fd7f0bf8210876c3d92c9b5b26d48521ff21d924f876440a8cb5448a9d218f->enter($__internal_94fd7f0bf8210876c3d92c9b5b26d48521ff21d924f876440a8cb5448a9d218f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 101
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    <script src=\"";
        // line 102
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("datatable/js/jquery.metisMenu.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 103
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("datatable/js/jquery-ui.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 104
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("datatable/js/jquery.blockUI.js"), "html", null, true);
        echo "\"></script>

    <!--Functions Js-->
    <script src=\"";
        // line 107
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("datatable/js/functions.js"), "html", null, true);
        echo "\"></script>

    <script src=\"";
        // line 109
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("datatable/js/loader.js"), "html", null, true);
        echo "\"></script>

    <script src=\"";
        // line 111
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("datatable/js/jquery.dataTables.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 112
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("datatable/js/dataTables.bootstrap.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 113
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("datatable/js/jszip.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 114
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("datatable/js/pdfmake.min.js"), "html", null, true);
        echo "\"></script>
    ";
        // line 116
        echo "    <script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("datatable/js/buttons.html5.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 117
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("datatable/js/buttons.colVis.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 118
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("datatable/js/dataTables-script.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 119
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("plugins/bower_components/bootstrap-rtl-master/dist/js/bootstrap-rtl.min.js"), "html", null, true);
        echo "\"></script>
";
        
        $__internal_94fd7f0bf8210876c3d92c9b5b26d48521ff21d924f876440a8cb5448a9d218f->leave($__internal_94fd7f0bf8210876c3d92c9b5b26d48521ff21d924f876440a8cb5448a9d218f_prof);

        
        $__internal_67859d2aa73e0529256d256e85d7ff53af9d867a8b10ef96c8485dbb7ad03a92->leave($__internal_67859d2aa73e0529256d256e85d7ff53af9d867a8b10ef96c8485dbb7ad03a92_prof);

    }

    public function getTemplateName()
    {
        return "DoctorsAdminBundle:User:ajouterUser.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  295 => 119,  291 => 118,  287 => 117,  282 => 116,  278 => 114,  274 => 113,  270 => 112,  266 => 111,  261 => 109,  256 => 107,  250 => 104,  246 => 103,  242 => 102,  237 => 101,  228 => 100,  212 => 94,  205 => 91,  200 => 89,  192 => 81,  187 => 79,  181 => 76,  173 => 71,  168 => 69,  162 => 66,  155 => 62,  150 => 60,  144 => 57,  137 => 53,  132 => 51,  126 => 48,  118 => 43,  113 => 41,  107 => 38,  98 => 32,  93 => 30,  87 => 27,  80 => 23,  75 => 21,  69 => 18,  64 => 15,  50 => 3,  41 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"::layoutAdmin.html.twig\" %}
{% block body %}
    <div class=\"preloader\">
        <div class=\"cssload-speeding-wheel\"></div>
    </div>
    <div id=\"page-wrapper\">
        <div class=\"container-fluid\">
            <div class=\"row bg-title\">
                <div class=\"col-lg-3 col-md-4 col-sm-4 col-xs-12\">
                    <h4 class=\"page-title arabe\"> اضافة مستخدم</h4></div>
                <!-- /.col-lg-12 -->
            </div>
            <div class=\"row\">
                <div class=\"white-box\">
                    {{ form_start(form) }}
                    <div class=\"form-group\">
                        {# Génération du label. #}
                        {{ form_label(form.photo, \"صورة المستخدم\", {'label_attr': {'class': 'arabe control-label'}}) }}

                        {# Affichage des erreurs pour ce champ précis. #}
                        {{ form_errors(form.photo) }}
                        {# Génération de l'input. #}
                        {{ form_widget(form.photo, {'attr': {'class': 'arabe'}}) }}
                    </div>
                    <div class=\"form-group\">
                        {# Génération du label. #}
                        {{ form_label(form.phoneNumber, \"رقم الجوال\", {'label_attr': {'class': 'arabe control-label'}}) }}

                        {# Affichage des erreurs pour ce champ précis. #}
                        {{ form_errors(form.phoneNumber) }}
                        {# Génération de l'input. #}
                        {{ form_widget(form.phoneNumber, {'attr': {'class': 'form-control arabe'}}) }}
                    </div>


                    <div class=\"form-group\">
                        {# Génération du label. #}
                        {{ form_label(form.name, \"اسم المستخدم\", {'label_attr': {'class': 'arabe control-label'}}) }}

                        {# Affichage des erreurs pour ce champ précis. #}
                        {{ form_errors(form.name) }}
                        {# Génération de l'input. #}
                        {{ form_widget(form.name, {'attr': {'class': 'form-control arabe'}}) }}
                    </div>

                    <div class=\"form-group\">
                        {# Génération du label. #}
                        {{ form_label(form.country, \"البلد\", {'label_attr': {'class': 'arabe control-label'}}) }}

                        {# Affichage des erreurs pour ce champ précis. #}
                        {{ form_errors(form.country) }}
                        {# Génération de l'input. #}
                        {{ form_widget(form.country, {'attr': {'class': 'form-control arabe'}}) }}
                    </div>
                    <div class=\"form-group\">
                        {# Génération du label. #}
                        {{ form_label(form.town, \"المدينة\", {'label_attr': {'class': 'arabe control-label'}}) }}

                        {# Affichage des erreurs pour ce champ précis. #}
                        {{ form_errors(form.town) }}
                        {# Génération de l'input. #}
                        {{ form_widget(form.town, {'attr': {'class': 'form-control arabe'}}) }}
                    </div>
                    <div class=\"form-group\">
                        {# Génération du label. #}
                        {{ form_label(form.city, \"الحي\", {'label_attr': {'class': 'arabe control-label'}}) }}

                        {# Affichage des erreurs pour ce champ précis. #}
                        {{ form_errors(form.city) }}
                        {# Génération de l'input. #}
                        {{ form_widget(form.city, {'attr': {'class': 'form-control arabe'}}) }}
                    </div>

                    <div class=\"form-group\">
                        {# Génération du label. #}
                        {{ form_label(form.email, \"البريد الالكتروني\", {'label_attr': {'class': 'arabe control-label'}}) }}

                        {# Affichage des erreurs pour ce champ précis. #}
                        {{ form_errors(form.email) }}
                        {# Génération de l'input. #}
                        {{ form_widget(form.email, {'attr': {'class': 'form-control arabe'}}) }}
                    </div>

                    <div class=\"form-group\">
                        {# Génération du label. #}{#
                        {{ form_label(form.password, \"كلمة المرور\", {'label_attr': {'class': 'arabe control-label'}}) }}

                        #}{# Affichage des erreurs pour ce champ précis. #}
                        {{ form_errors(form.password) }}
                        {# Génération de l'input. #}
                        {{ form_widget(form.password, {'attr': {'class': 'arabe'}}) }}
                    </div>
                    <input type=\"submit\" value=\"اضافة المستخدم\" class=\"btn btn-primary btn-lg arabe\" />
                    {{ form_end(form) }}
                </div>
            </div>
        </div>
    </div>
{% endblock %}
{% block javascripts %}
    {{ parent() }}
    <script src=\"{{ asset('datatable/js/jquery.metisMenu.js') }}\"></script>
    <script src=\"{{ asset('datatable/js/jquery-ui.js') }}\"></script>
    <script src=\"{{ asset('datatable/js/jquery.blockUI.js') }}\"></script>

    <!--Functions Js-->
    <script src=\"{{ asset('datatable/js/functions.js') }}\"></script>

    <script src=\"{{ asset('datatable/js/loader.js') }}\"></script>

    <script src=\"{{ asset('datatable/js/jquery.dataTables.min.js') }}\"></script>
    <script src=\"{{ asset('datatable/js/dataTables.bootstrap.min.js') }}\"></script>
    <script src=\"{{ asset('datatable/js/jszip.min.js') }}\"></script>
    <script src=\"{{ asset('datatable/js/pdfmake.min.js') }}\"></script>
    {#<script src=\"{{ asset('datatable/js/vfs_fonts.js') }}\"></script>#}
    <script src=\"{{ asset('datatable/js/buttons.html5.js') }}\"></script>
    <script src=\"{{ asset('datatable/js/buttons.colVis.js') }}\"></script>
    <script src=\"{{ asset('datatable/js/dataTables-script.js') }}\"></script>
    <script src=\"{{ asset('plugins/bower_components/bootstrap-rtl-master/dist/js/bootstrap-rtl.min.js') }}\"></script>
{% endblock %}", "DoctorsAdminBundle:User:ajouterUser.html.twig", "C:\\wamp64\\www\\doctorAdmin\\src\\Doctors\\AdminBundle/Resources/views/User/ajouterUser.html.twig");
    }
}
