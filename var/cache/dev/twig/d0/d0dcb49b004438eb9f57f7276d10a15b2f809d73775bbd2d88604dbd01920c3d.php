<?php

/* ::layoutAdmin.html.twig */
class __TwigTemplate_f8678c7f394923ab3306f50873f50116d5a0585cda3af1685288ff7e298da6f8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'stylesh' => array($this, 'block_stylesh'),
            'navbarHeader' => array($this, 'block_navbarHeader'),
            'body' => array($this, 'block_body'),
            'footer' => array($this, 'block_footer'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ec270c05bac60754463d9954b8caa9d2a9c5833b0b33aadbd06bb4751dbb4752 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ec270c05bac60754463d9954b8caa9d2a9c5833b0b33aadbd06bb4751dbb4752->enter($__internal_ec270c05bac60754463d9954b8caa9d2a9c5833b0b33aadbd06bb4751dbb4752_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "::layoutAdmin.html.twig"));

        $__internal_d36618da724895ee76c900c8f5fdb94db6ea25f8320e5208a5bcec5b1a96afff = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d36618da724895ee76c900c8f5fdb94db6ea25f8320e5208a5bcec5b1a96afff->enter($__internal_d36618da724895ee76c900c8f5fdb94db6ea25f8320e5208a5bcec5b1a96afff_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "::layoutAdmin.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<head>
    <meta charset=\"utf-8\">
    <!DOCTYPE html>
    <html lang=\"en\" dir=\"rtl\">
    <head>
        <meta charset=\"UTF-8\"/>
        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
        <meta name=\"description\" content=\"\">
        <meta name=\"author\" content=\"\">
        <title>";
        // line 12
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 13
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 41
        echo "        ";
        // line 42
        echo "    </head>
<body>
";
        // line 44
        $this->displayBlock('navbarHeader', $context, $blocks);
        // line 162
        $this->displayBlock('body', $context, $blocks);
        // line 163
        $this->displayBlock('footer', $context, $blocks);
        // line 167
        $this->displayBlock('javascripts', $context, $blocks);
        // line 206
        echo "
</body>
</html>
";
        
        $__internal_ec270c05bac60754463d9954b8caa9d2a9c5833b0b33aadbd06bb4751dbb4752->leave($__internal_ec270c05bac60754463d9954b8caa9d2a9c5833b0b33aadbd06bb4751dbb4752_prof);

        
        $__internal_d36618da724895ee76c900c8f5fdb94db6ea25f8320e5208a5bcec5b1a96afff->leave($__internal_d36618da724895ee76c900c8f5fdb94db6ea25f8320e5208a5bcec5b1a96afff_prof);

    }

    // line 12
    public function block_title($context, array $blocks = array())
    {
        $__internal_c17c4b616f0c510db4c0ef7d99656250a06bf1bbf5b28ad12f824d93c4bd165c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c17c4b616f0c510db4c0ef7d99656250a06bf1bbf5b28ad12f824d93c4bd165c->enter($__internal_c17c4b616f0c510db4c0ef7d99656250a06bf1bbf5b28ad12f824d93c4bd165c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_d274c5f8076656880513e2b15bf72158b0307f1bf8d8247106299b5a99a5773b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d274c5f8076656880513e2b15bf72158b0307f1bf8d8247106299b5a99a5773b->enter($__internal_d274c5f8076656880513e2b15bf72158b0307f1bf8d8247106299b5a99a5773b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Doctors";
        
        $__internal_d274c5f8076656880513e2b15bf72158b0307f1bf8d8247106299b5a99a5773b->leave($__internal_d274c5f8076656880513e2b15bf72158b0307f1bf8d8247106299b5a99a5773b_prof);

        
        $__internal_c17c4b616f0c510db4c0ef7d99656250a06bf1bbf5b28ad12f824d93c4bd165c->leave($__internal_c17c4b616f0c510db4c0ef7d99656250a06bf1bbf5b28ad12f824d93c4bd165c_prof);

    }

    // line 13
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_43c4e4c1846b6ee2be3acc32fbfe78f097d414194a2be839c6c355c0cfdd17ee = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_43c4e4c1846b6ee2be3acc32fbfe78f097d414194a2be839c6c355c0cfdd17ee->enter($__internal_43c4e4c1846b6ee2be3acc32fbfe78f097d414194a2be839c6c355c0cfdd17ee_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_30a92e1591c7a6088e2dd9e4bd942ac98c8eda94b8b67d68f634641262f2f7a3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_30a92e1591c7a6088e2dd9e4bd942ac98c8eda94b8b67d68f634641262f2f7a3->enter($__internal_30a92e1591c7a6088e2dd9e4bd942ac98c8eda94b8b67d68f634641262f2f7a3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 14
        echo "            <!-- Bootstrap Core CSS -->
            <link href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("plugins/bower_components/bootstrap-rtl-master/dist/css/bootstrap-rtl.min.css"), "html", null, true);
        echo "\"
                  rel=\"stylesheet\">
            <!-- Menu CSS -->
            <link href=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
            <!-- vector map CSS -->
            <link href=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("plugins/bower_components/vectormap/jquery-jvectormap-2.0.2.css"), "html", null, true);
        echo "\"
                  rel=\"stylesheet\"/>
            <link href=\"";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("plugins/bower_components/css-chart/css-chart.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
            <!-- animation CSS -->
            <link href=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/animate.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
            <!-- Custom CSS -->
            <link href=\"";
        // line 26
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/style.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
            <!-- color CSS -->
            <link href=\"";
        // line 28
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/colors/blue.css"), "html", null, true);
        echo "\" id=\"theme\" rel=\"stylesheet\">
            <link rel=\"stylesheet\" href=\"";
        // line 29
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/style_business.css"), "html", null, true);
        echo "\">
            ";
        // line 31
        echo "            ";
        // line 32
        echo "            ";
        $this->displayBlock('stylesh', $context, $blocks);
        // line 34
        echo "            <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
            <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
            <!--[if lt IE 9]>
            <script src=\"https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js\"></script>
            <script src=\"https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js\"></script>
            <![endif]-->
        ";
        
        $__internal_30a92e1591c7a6088e2dd9e4bd942ac98c8eda94b8b67d68f634641262f2f7a3->leave($__internal_30a92e1591c7a6088e2dd9e4bd942ac98c8eda94b8b67d68f634641262f2f7a3_prof);

        
        $__internal_43c4e4c1846b6ee2be3acc32fbfe78f097d414194a2be839c6c355c0cfdd17ee->leave($__internal_43c4e4c1846b6ee2be3acc32fbfe78f097d414194a2be839c6c355c0cfdd17ee_prof);

    }

    // line 32
    public function block_stylesh($context, array $blocks = array())
    {
        $__internal_3819e2f30255e80cc43b5f5bdeeea13d3aeefd913c5cfdb2c77d3f872ce2b86b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3819e2f30255e80cc43b5f5bdeeea13d3aeefd913c5cfdb2c77d3f872ce2b86b->enter($__internal_3819e2f30255e80cc43b5f5bdeeea13d3aeefd913c5cfdb2c77d3f872ce2b86b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesh"));

        $__internal_4777666af29a75244e4febe7d40ee9a0d1cae9d7da7369e1176390785b8b3c78 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4777666af29a75244e4febe7d40ee9a0d1cae9d7da7369e1176390785b8b3c78->enter($__internal_4777666af29a75244e4febe7d40ee9a0d1cae9d7da7369e1176390785b8b3c78_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesh"));

        // line 33
        echo "                ";
        
        $__internal_4777666af29a75244e4febe7d40ee9a0d1cae9d7da7369e1176390785b8b3c78->leave($__internal_4777666af29a75244e4febe7d40ee9a0d1cae9d7da7369e1176390785b8b3c78_prof);

        
        $__internal_3819e2f30255e80cc43b5f5bdeeea13d3aeefd913c5cfdb2c77d3f872ce2b86b->leave($__internal_3819e2f30255e80cc43b5f5bdeeea13d3aeefd913c5cfdb2c77d3f872ce2b86b_prof);

    }

    // line 44
    public function block_navbarHeader($context, array $blocks = array())
    {
        $__internal_281d836b3b74e43e9472b87547184cd4bffbfc478f9ab036af1a4788b6948612 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_281d836b3b74e43e9472b87547184cd4bffbfc478f9ab036af1a4788b6948612->enter($__internal_281d836b3b74e43e9472b87547184cd4bffbfc478f9ab036af1a4788b6948612_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "navbarHeader"));

        $__internal_d97afed7a93decf68ccc0761d94caba8ba8f1d14e4e5750a5c5197dec9ba37cd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d97afed7a93decf68ccc0761d94caba8ba8f1d14e4e5750a5c5197dec9ba37cd->enter($__internal_d97afed7a93decf68ccc0761d94caba8ba8f1d14e4e5750a5c5197dec9ba37cd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "navbarHeader"));

        // line 45
        echo "    <!-- navigation -->
    <nav class=\"navbar navbar-default navbar-static-top m-b-0\">
        <div class=\"navbar-header\"><a class=\"navbar-toggle hidden-sm hidden-md hidden-lg \" href=\"javascript:void(0)\"
                                      data-toggle=\"collapse\" data-target=\".navbar-collapse\"><i class=\"ti-menu\"></i></a>
            <div class=\"top-left-part\"><a class=\"logo\" href=\"";
        // line 49
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("doctors_admin_homepage");
        echo "\"><b><i class=\"fa fa-2x fa-user-md\" aria-hidden=\"true\"></i>
                    </b><span
                            class=\"hidden-xs arabe\">الأطباء</span></a></div>
            <ul class=\"nav navbar-top-links navbar-left hidden-xs\">
                <li><a href=\"javascript:void(0)\" class=\"open-close hidden-xs waves-effect waves-light\"><i
                                class=\"icon-arrow-right-circle ti-menu\"></i></a></li>

            </ul>
            <ul class=\"nav navbar-top-links navbar-right pull-right\">
                <li class=\"dropdown\"><a class=\"dropdown-toggle profile-pic\" data-toggle=\"dropdown\" href=\"#\"> <img
                                src=\"";
        // line 59
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("plugins/images/users/avatar.png"), "html", null, true);
        echo "\" alt=\"user-img\" width=\"36\"
                                class=\"img-circle\"><b class=\"hidden-xs\">";
        // line 60
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "username", array()), "html", null, true);
        echo "</b> </a>
                    <ul class=\"dropdown-menu dropdown-user animated flipInY\">
                        <li><a href=\"";
        // line 62
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_profile_edit");
        echo "\" class=\"arabe\"><i class=\"ti-settings\"></i> اعدادات الحساب </a></li>
                        <li role=\"separator\" class=\"divider\"></li>
                        <li><a href=\"";
        // line 64
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_change_password");
        echo "\" class=\"arabe\"><i class=\"ti-settings\"></i> تغيير كلمة العبور</a></li>
                        <li role=\"separator\" class=\"divider\"></li>
                        <li>";
        // line 66
        if ($this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("IS_AUTHENTICATED_REMEMBERED")) {
            // line 67
            echo "                            <a href=\"";
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_security_logout");
            echo "\" class=\"arabe\"><i class=\"fa fa-sign-out\" aria-hidden=\"true\"></i>

                                تسجيل الخروج للأدمين
                            </a>
                        ";
        } else {
            // line 72
            echo "                            <a href=\"";
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_security_login");
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("layout.login", array(), "FOSUserBundle"), "html", null, true);
            echo "</a>
                            ";
        }
        // line 73
        echo "</li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- .Megamenu -->

                <!-- /.dropdown -->
            </ul>
        </div>
        <!-- /.navbar-header -->
        <!-- /.navbar-top-links -->
        <!-- /.navbar-static-side -->
    </nav>
    <!-- Left navbar-header -->
    <div class=\"navbar-default sidebar\" role=\"navigation\">
        <div class=\"sidebar-nav navbar-collapse slimscrollsidebar\">
            <ul class=\"nav\" id=\"side-menu\">
                <li class=\"sidebar-search hidden-sm hidden-md hidden-lg\">
                    <!-- input-group -->
                    <div class=\"input-group custom-search-form\">
                        <input type=\"text\" class=\"form-control\" placeholder=\"بحث...\">
                        <span class=\"input-group-btn\">
            <button class=\"btn btn-default\" type=\"button\"> <i class=\"fa fa-search\"></i> </button>
            </span></div>
                    <!-- /input-group -->
                </li>
                <li class=\"user-pro\"><a href=\"";
        // line 99
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("doctors_admin_homepage");
        echo "\" class=\"waves-effect\"><img
                                src=\"";
        // line 100
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("plugins/images/users/avatar.png"), "html", null, true);
        echo "\"
                                alt=\"user-img\" class=\"img-circle\"> <span
                                class=\"hide-menu arabe\"> الادارة</span></a>
                </li>
                <li class=\"nav-small-cap m-t-10\">--- القائمة الرئيسية ---</li>
                <li><a href=\"";
        // line 105
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("doctors_admin_homepage");
        echo "\" class=\"waves-effect active\"><i
                                class=\"linea-icon linea-basic fa-fw\"
                                data-icon=\"v\"></i> <span
                                class=\"hide-menu arabe\"> الاحصائيات </span></span>
                    </a>
                </li>
                <li><a href=\"#\" class=\"waves-effect\"><i class=\"fa fa-stethoscope p-r-10\" aria-hidden=\"true\"></i><span class=\"hide-menu arabe\"> ادارة الاختصاصات <span
                                    class=\"fa arrow\"></span></span></a>
                    <ul class=\"nav nav-second-level\">
                        <li><a href=\"";
        // line 114
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("doctorSpeciality_new");
        echo "\" class=\"arabe\"><i class=\"fa fa-plus p-r-10\"> </i> اضافة اختصاص</a></li>
                        <li><a href=\"";
        // line 115
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("doctorSpeciality_index");
        echo "\" class=\"arabe\"><i class=\"fa fa-list p-r-10\"> </i> قائمة الاختصاصات</a></li>
                    </ul>
                </li>
                <li><a href=\"javascript:void(0);\" class=\"waves-effect\"><i class=\"fa fa-user-md p-r-10\"></i> <span
                                class=\"hide-menu arabe\"> ادارة الأطباء <span class=\"fa arrow\"></span></span></a>
                    <ul class=\"nav nav-second-level\">
                        <li><a href=\"";
        // line 121
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("doctors_admin_addDoctors");
        echo "\" class=\"arabe\"><i class=\"fa fa-plus p-r-10\"> </i> اضافة حساب طبيب </a></li>
                        <li><a href=\"";
        // line 122
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("doctors_admin_addDoctors");
        echo "\" class=\"arabe\"><i class=\"fa fa-plus p-r-10\"> </i> اضافة طبيب  <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(بيانات مكتملة)</a></li>
                        <li><a href=\"";
        // line 123
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("doctors_admin_doctors");
        echo "\" class=\"arabe\"><i class=\"fa fa-list p-r-10\"> </i> قائمة الأطباء</a></li>
                        ";
        // line 125
        echo "                    </ul>
                </li>
                <li><a href=\"#\" class=\"waves-effect\"><i class=\"icon-people p-r-10\"></i> <span class=\"hide-menu arabe\"> ادارة المستخدمين <span
                                    class=\"fa arrow\"></span></span></a>
                    <ul class=\"nav nav-second-level\">
                        <li><a href=\"";
        // line 130
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("doctors_admin_addusers");
        echo "\" class=\"arabe\"><i class=\"fa fa-plus p-r-10\"> </i> اضافة مستخدم</a></li>
                        <li><a href=\"";
        // line 131
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("doctors_admin_users");
        echo "\" class=\"arabe\"><i class=\"fa fa-list p-r-10\"></i> قائمة المستخدمين</a></li>
                    </ul>
                </li>
                <li><a href=\"#\" class=\"waves-effect\"><i class=\"fa fa-medkit p-r-10\"></i> <span class=\"hide-menu arabe\"> ادارة المواعيد <span
                                    class=\"fa arrow\"></span></span></a>
                    <ul class=\"nav nav-second-level\">
                        <li><a href=\"";
        // line 137
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("doctors_admin_addAppointment");
        echo "\" class=\"arabe\"><i class=\"fa fa-plus p-r-10\"> </i> اضافة موعد</a></li>
                        <li><a href=\"";
        // line 138
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("doctors_admin_appointment");
        echo "\" class=\"arabe\"><i class=\"fa fa-list p-r-10\"> </i> قائمة المواعيد</a></li>

                    </ul>
                </li>
                <li><a href=\"#\" class=\"waves-effect\"><i class=\"fa fa-comment p-r-10\"></i> <span class=\"hide-menu arabe\"> ادارة التقييمات <span
                                    class=\"fa arrow\"></span></span></a>
                    <ul class=\"nav nav-second-level\">
                        <li><a href=\"";
        // line 145
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("doctors_admin_addEvaluation");
        echo "\" class=\"arabe\"><i class=\"fa fa-plus p-r-10\"> </i> اضافة تقييم</a></li>
                        <li><a href=\"";
        // line 146
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("doctors_admin_evaluation");
        echo "\" class=\"arabe\"><i class=\"fa fa-list p-r-10\"> </i> قائمة التقييمات</a></li>
                    </ul>
                </li>
                ";
        // line 157
        echo "            </ul>
        </div>
    </div>
    <!-- Left navbar-header end -->
";
        
        $__internal_d97afed7a93decf68ccc0761d94caba8ba8f1d14e4e5750a5c5197dec9ba37cd->leave($__internal_d97afed7a93decf68ccc0761d94caba8ba8f1d14e4e5750a5c5197dec9ba37cd_prof);

        
        $__internal_281d836b3b74e43e9472b87547184cd4bffbfc478f9ab036af1a4788b6948612->leave($__internal_281d836b3b74e43e9472b87547184cd4bffbfc478f9ab036af1a4788b6948612_prof);

    }

    // line 162
    public function block_body($context, array $blocks = array())
    {
        $__internal_33c560a3c2ab12f4dc1de49167c6661bd10381b6154cb2aa2ec536eefbb454ab = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_33c560a3c2ab12f4dc1de49167c6661bd10381b6154cb2aa2ec536eefbb454ab->enter($__internal_33c560a3c2ab12f4dc1de49167c6661bd10381b6154cb2aa2ec536eefbb454ab_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_ae7027930a4d88d92ba7ba117d01bb3396f9fdf47f85e255ab3312901f417da6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ae7027930a4d88d92ba7ba117d01bb3396f9fdf47f85e255ab3312901f417da6->enter($__internal_ae7027930a4d88d92ba7ba117d01bb3396f9fdf47f85e255ab3312901f417da6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_ae7027930a4d88d92ba7ba117d01bb3396f9fdf47f85e255ab3312901f417da6->leave($__internal_ae7027930a4d88d92ba7ba117d01bb3396f9fdf47f85e255ab3312901f417da6_prof);

        
        $__internal_33c560a3c2ab12f4dc1de49167c6661bd10381b6154cb2aa2ec536eefbb454ab->leave($__internal_33c560a3c2ab12f4dc1de49167c6661bd10381b6154cb2aa2ec536eefbb454ab_prof);

    }

    // line 163
    public function block_footer($context, array $blocks = array())
    {
        $__internal_2099945397e80128141ac7006768ba0c4b005de3fb06b986c5f00688b901bb44 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2099945397e80128141ac7006768ba0c4b005de3fb06b986c5f00688b901bb44->enter($__internal_2099945397e80128141ac7006768ba0c4b005de3fb06b986c5f00688b901bb44_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "footer"));

        $__internal_1aaad70b0ac9d02f5af782bc410ccbee873372d28c85c68b83734719eb569ab7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1aaad70b0ac9d02f5af782bc410ccbee873372d28c85c68b83734719eb569ab7->enter($__internal_1aaad70b0ac9d02f5af782bc410ccbee873372d28c85c68b83734719eb569ab7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "footer"));

        // line 164
        echo "    <footer class=\"footer text-center\"> ";
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, "now", "Y"), "html", null, true);
        echo " &copy; </footer>
";
        
        $__internal_1aaad70b0ac9d02f5af782bc410ccbee873372d28c85c68b83734719eb569ab7->leave($__internal_1aaad70b0ac9d02f5af782bc410ccbee873372d28c85c68b83734719eb569ab7_prof);

        
        $__internal_2099945397e80128141ac7006768ba0c4b005de3fb06b986c5f00688b901bb44->leave($__internal_2099945397e80128141ac7006768ba0c4b005de3fb06b986c5f00688b901bb44_prof);

    }

    // line 167
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_3146c862f986b4fcdb484edad2e61f5a4fb7d814f18565d782afc3a96aef7bd9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3146c862f986b4fcdb484edad2e61f5a4fb7d814f18565d782afc3a96aef7bd9->enter($__internal_3146c862f986b4fcdb484edad2e61f5a4fb7d814f18565d782afc3a96aef7bd9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_5fe17d3c9f37fbbfc793d3b20f86af43d34bdd309df7f572fe2b9ea20bdddb80 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5fe17d3c9f37fbbfc793d3b20f86af43d34bdd309df7f572fe2b9ea20bdddb80->enter($__internal_5fe17d3c9f37fbbfc793d3b20f86af43d34bdd309df7f572fe2b9ea20bdddb80_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 168
        echo "    <!-- jQuery -->
    <script src=\"";
        // line 169
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("plugins/bower_components/jquery/dist/jquery.min.js"), "html", null, true);
        echo "\"></script>
    ";
        // line 171
        echo "   ";
        // line 179
        echo "    ";
        // line 180
        echo "
    <!-- Bootstrap Core JavaScript -->
    ";
        // line 183
        echo "    <!-- Menu Plugin JavaScript -->
    <script src=\"";
        // line 184
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"), "html", null, true);
        echo "\"></script>
    <!--slimscroll JavaScript -->
    <script src=\"";
        // line 186
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/jquery.slimscroll.js"), "html", null, true);
        echo "\"></script>
    <!--Wave Effects -->
    <script src=\"";
        // line 188
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/waves.js"), "html", null, true);
        echo "\"></script>
    <!-- Flot Charts JavaScript -->
    ";
        // line 192
        echo "    <!-- google maps api -->
    ";
        // line 195
        echo "    <!-- Sparkline charts -->
    <script src=\"";
        // line 196
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js"), "html", null, true);
        echo "\"></script>
    <!-- EASY PIE CHART JS -->
    <script src=\"";
        // line 198
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("plugins/bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 199
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("plugins/bower_components/jquery.easy-pie-chart/easy-pie-chart.init.js"), "html", null, true);
        echo "\"></script>
    <!-- Custom Theme JavaScript -->
    <script src=\"";
        // line 201
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/custom.min.js"), "html", null, true);
        echo "\"></script>
    ";
        // line 203
        echo "    <!--Style Switcher -->
    <script src=\"";
        // line 204
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("plugins/bower_components/styleswitcher/jQuery.style.switcher.js"), "html", null, true);
        echo "\"></script>
";
        
        $__internal_5fe17d3c9f37fbbfc793d3b20f86af43d34bdd309df7f572fe2b9ea20bdddb80->leave($__internal_5fe17d3c9f37fbbfc793d3b20f86af43d34bdd309df7f572fe2b9ea20bdddb80_prof);

        
        $__internal_3146c862f986b4fcdb484edad2e61f5a4fb7d814f18565d782afc3a96aef7bd9->leave($__internal_3146c862f986b4fcdb484edad2e61f5a4fb7d814f18565d782afc3a96aef7bd9_prof);

    }

    public function getTemplateName()
    {
        return "::layoutAdmin.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  496 => 204,  493 => 203,  489 => 201,  484 => 199,  480 => 198,  475 => 196,  472 => 195,  469 => 192,  464 => 188,  459 => 186,  454 => 184,  451 => 183,  447 => 180,  445 => 179,  443 => 171,  439 => 169,  436 => 168,  427 => 167,  414 => 164,  405 => 163,  388 => 162,  374 => 157,  368 => 146,  364 => 145,  354 => 138,  350 => 137,  341 => 131,  337 => 130,  330 => 125,  326 => 123,  322 => 122,  318 => 121,  309 => 115,  305 => 114,  293 => 105,  285 => 100,  281 => 99,  253 => 73,  245 => 72,  236 => 67,  234 => 66,  229 => 64,  224 => 62,  219 => 60,  215 => 59,  202 => 49,  196 => 45,  187 => 44,  177 => 33,  168 => 32,  152 => 34,  149 => 32,  147 => 31,  143 => 29,  139 => 28,  134 => 26,  129 => 24,  124 => 22,  119 => 20,  114 => 18,  108 => 15,  105 => 14,  96 => 13,  78 => 12,  65 => 206,  63 => 167,  61 => 163,  59 => 162,  57 => 44,  53 => 42,  51 => 41,  49 => 13,  45 => 12,  32 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<head>
    <meta charset=\"utf-8\">
    <!DOCTYPE html>
    <html lang=\"en\" dir=\"rtl\">
    <head>
        <meta charset=\"UTF-8\"/>
        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
        <meta name=\"description\" content=\"\">
        <meta name=\"author\" content=\"\">
        <title>{% block title %}Doctors{% endblock %}</title>
        {% block stylesheets %}
            <!-- Bootstrap Core CSS -->
            <link href=\"{{ asset('plugins/bower_components/bootstrap-rtl-master/dist/css/bootstrap-rtl.min.css') }}\"
                  rel=\"stylesheet\">
            <!-- Menu CSS -->
            <link href=\"{{ asset('plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css') }}\" rel=\"stylesheet\">
            <!-- vector map CSS -->
            <link href=\"{{ asset('plugins/bower_components/vectormap/jquery-jvectormap-2.0.2.css') }}\"
                  rel=\"stylesheet\"/>
            <link href=\"{{ asset('plugins/bower_components/css-chart/css-chart.css') }}\" rel=\"stylesheet\">
            <!-- animation CSS -->
            <link href=\"{{ asset('css/animate.css') }}\" rel=\"stylesheet\">
            <!-- Custom CSS -->
            <link href=\"{{ asset('css/style.css') }}\" rel=\"stylesheet\">
            <!-- color CSS -->
            <link href=\"{{ asset('css/colors/blue.css') }}\" id=\"theme\" rel=\"stylesheet\">
            <link rel=\"stylesheet\" href=\"{{ asset('css/style_business.css') }}\">
            {#<link rel=\"stylesheet\" href=\"{{ asset('bootstrap-datetimepicker-master/build/css/bootstrap-datetimepicker.min.css') }}\">#}
            {#<link rel=\"stylesheet\" href=\"{{ asset('datetimepicker-master/jquery.datetimepicker.css') }}\">#}
            {% block stylesh %}
                {% endblock %}
            <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
            <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
            <!--[if lt IE 9]>
            <script src=\"https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js\"></script>
            <script src=\"https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js\"></script>
            <![endif]-->
        {% endblock %}
        {# <link rel=\"icon\" type=\"image/x-icon\" href=\"{{ asset('favicon.ico') }}\" />#}
    </head>
<body>
{% block navbarHeader %}
    <!-- navigation -->
    <nav class=\"navbar navbar-default navbar-static-top m-b-0\">
        <div class=\"navbar-header\"><a class=\"navbar-toggle hidden-sm hidden-md hidden-lg \" href=\"javascript:void(0)\"
                                      data-toggle=\"collapse\" data-target=\".navbar-collapse\"><i class=\"ti-menu\"></i></a>
            <div class=\"top-left-part\"><a class=\"logo\" href=\"{{ path('doctors_admin_homepage') }}\"><b><i class=\"fa fa-2x fa-user-md\" aria-hidden=\"true\"></i>
                    </b><span
                            class=\"hidden-xs arabe\">الأطباء</span></a></div>
            <ul class=\"nav navbar-top-links navbar-left hidden-xs\">
                <li><a href=\"javascript:void(0)\" class=\"open-close hidden-xs waves-effect waves-light\"><i
                                class=\"icon-arrow-right-circle ti-menu\"></i></a></li>

            </ul>
            <ul class=\"nav navbar-top-links navbar-right pull-right\">
                <li class=\"dropdown\"><a class=\"dropdown-toggle profile-pic\" data-toggle=\"dropdown\" href=\"#\"> <img
                                src=\"{{ asset('plugins/images/users/avatar.png') }}\" alt=\"user-img\" width=\"36\"
                                class=\"img-circle\"><b class=\"hidden-xs\">{{ app.user.username }}</b> </a>
                    <ul class=\"dropdown-menu dropdown-user animated flipInY\">
                        <li><a href=\"{{ path('fos_user_profile_edit') }}\" class=\"arabe\"><i class=\"ti-settings\"></i> اعدادات الحساب </a></li>
                        <li role=\"separator\" class=\"divider\"></li>
                        <li><a href=\"{{ path('fos_user_change_password') }}\" class=\"arabe\"><i class=\"ti-settings\"></i> تغيير كلمة العبور</a></li>
                        <li role=\"separator\" class=\"divider\"></li>
                        <li>{% if is_granted(\"IS_AUTHENTICATED_REMEMBERED\") %}
                            <a href=\"{{ path('fos_user_security_logout') }}\" class=\"arabe\"><i class=\"fa fa-sign-out\" aria-hidden=\"true\"></i>

                                تسجيل الخروج للأدمين
                            </a>
                        {% else %}
                            <a href=\"{{ path('fos_user_security_login') }}\">{{ 'layout.login'|trans({}, 'FOSUserBundle') }}</a>
                            {% endif %}</li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- .Megamenu -->

                <!-- /.dropdown -->
            </ul>
        </div>
        <!-- /.navbar-header -->
        <!-- /.navbar-top-links -->
        <!-- /.navbar-static-side -->
    </nav>
    <!-- Left navbar-header -->
    <div class=\"navbar-default sidebar\" role=\"navigation\">
        <div class=\"sidebar-nav navbar-collapse slimscrollsidebar\">
            <ul class=\"nav\" id=\"side-menu\">
                <li class=\"sidebar-search hidden-sm hidden-md hidden-lg\">
                    <!-- input-group -->
                    <div class=\"input-group custom-search-form\">
                        <input type=\"text\" class=\"form-control\" placeholder=\"بحث...\">
                        <span class=\"input-group-btn\">
            <button class=\"btn btn-default\" type=\"button\"> <i class=\"fa fa-search\"></i> </button>
            </span></div>
                    <!-- /input-group -->
                </li>
                <li class=\"user-pro\"><a href=\"{{ path('doctors_admin_homepage') }}\" class=\"waves-effect\"><img
                                src=\"{{ asset('plugins/images/users/avatar.png') }}\"
                                alt=\"user-img\" class=\"img-circle\"> <span
                                class=\"hide-menu arabe\"> الادارة</span></a>
                </li>
                <li class=\"nav-small-cap m-t-10\">--- القائمة الرئيسية ---</li>
                <li><a href=\"{{ path('doctors_admin_homepage') }}\" class=\"waves-effect active\"><i
                                class=\"linea-icon linea-basic fa-fw\"
                                data-icon=\"v\"></i> <span
                                class=\"hide-menu arabe\"> الاحصائيات </span></span>
                    </a>
                </li>
                <li><a href=\"#\" class=\"waves-effect\"><i class=\"fa fa-stethoscope p-r-10\" aria-hidden=\"true\"></i><span class=\"hide-menu arabe\"> ادارة الاختصاصات <span
                                    class=\"fa arrow\"></span></span></a>
                    <ul class=\"nav nav-second-level\">
                        <li><a href=\"{{ path('doctorSpeciality_new') }}\" class=\"arabe\"><i class=\"fa fa-plus p-r-10\"> </i> اضافة اختصاص</a></li>
                        <li><a href=\"{{ path('doctorSpeciality_index') }}\" class=\"arabe\"><i class=\"fa fa-list p-r-10\"> </i> قائمة الاختصاصات</a></li>
                    </ul>
                </li>
                <li><a href=\"javascript:void(0);\" class=\"waves-effect\"><i class=\"fa fa-user-md p-r-10\"></i> <span
                                class=\"hide-menu arabe\"> ادارة الأطباء <span class=\"fa arrow\"></span></span></a>
                    <ul class=\"nav nav-second-level\">
                        <li><a href=\"{{ path('doctors_admin_addDoctors') }}\" class=\"arabe\"><i class=\"fa fa-plus p-r-10\"> </i> اضافة حساب طبيب </a></li>
                        <li><a href=\"{{ path('doctors_admin_addDoctors') }}\" class=\"arabe\"><i class=\"fa fa-plus p-r-10\"> </i> اضافة طبيب  <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(بيانات مكتملة)</a></li>
                        <li><a href=\"{{ path('doctors_admin_doctors') }}\" class=\"arabe\"><i class=\"fa fa-list p-r-10\"> </i> قائمة الأطباء</a></li>
                        {#<li><a href=\"{{ path('doctors_admin_doctors') }}\">تحديث معلومات الطبيب</a></li>#}
                    </ul>
                </li>
                <li><a href=\"#\" class=\"waves-effect\"><i class=\"icon-people p-r-10\"></i> <span class=\"hide-menu arabe\"> ادارة المستخدمين <span
                                    class=\"fa arrow\"></span></span></a>
                    <ul class=\"nav nav-second-level\">
                        <li><a href=\"{{ path('doctors_admin_addusers') }}\" class=\"arabe\"><i class=\"fa fa-plus p-r-10\"> </i> اضافة مستخدم</a></li>
                        <li><a href=\"{{ path('doctors_admin_users') }}\" class=\"arabe\"><i class=\"fa fa-list p-r-10\"></i> قائمة المستخدمين</a></li>
                    </ul>
                </li>
                <li><a href=\"#\" class=\"waves-effect\"><i class=\"fa fa-medkit p-r-10\"></i> <span class=\"hide-menu arabe\"> ادارة المواعيد <span
                                    class=\"fa arrow\"></span></span></a>
                    <ul class=\"nav nav-second-level\">
                        <li><a href=\"{{ path('doctors_admin_addAppointment') }}\" class=\"arabe\"><i class=\"fa fa-plus p-r-10\"> </i> اضافة موعد</a></li>
                        <li><a href=\"{{ path('doctors_admin_appointment') }}\" class=\"arabe\"><i class=\"fa fa-list p-r-10\"> </i> قائمة المواعيد</a></li>

                    </ul>
                </li>
                <li><a href=\"#\" class=\"waves-effect\"><i class=\"fa fa-comment p-r-10\"></i> <span class=\"hide-menu arabe\"> ادارة التقييمات <span
                                    class=\"fa arrow\"></span></span></a>
                    <ul class=\"nav nav-second-level\">
                        <li><a href=\"{{ path('doctors_admin_addEvaluation') }}\" class=\"arabe\"><i class=\"fa fa-plus p-r-10\"> </i> اضافة تقييم</a></li>
                        <li><a href=\"{{ path('doctors_admin_evaluation') }}\" class=\"arabe\"><i class=\"fa fa-list p-r-10\"> </i> قائمة التقييمات</a></li>
                    </ul>
                </li>
                {#<li><a href=\"javascript:void(0)\" class=\"waves-effect\"><i class=\"fa fa-comments p-r-10\"></i> <span
                                class=\"hide-menu arabe\"> اتصالات </span></a>
                    <ul class=\"nav nav-second-level\">
                        <li><a href=\"contact.html\">Contact1</a></li>
                        <li><a href=\"contact2.html\">Contact2</a></li>
                        <li><a href=\"contact-detail.html\">Contact Detail</a></li>
                    </ul>
                </li>#}
            </ul>
        </div>
    </div>
    <!-- Left navbar-header end -->
{% endblock %}
{% block body %}{% endblock %}
{% block footer %}
    <footer class=\"footer text-center\"> {{ 'now'|date('Y') }} &copy; </footer>
{% endblock %}
{#<footer class=\"footer text-center\"> 2017 &copy; Businessology </footer>#}
{% block javascripts %}
    <!-- jQuery -->
    <script src=\"{{ asset('plugins/bower_components/jquery/dist/jquery.min.js') }}\"></script>
    {#<script  src=\"{{ asset('plugins/bower_components/bootstrap-rtl-master/dist/js/bootstrap-rtl.min.js') }}\"></script>#}
   {# <script src=\"{{ asset('js/jquery_19.min.js') }}\"></script>

    <script src=\"{{ asset('js/moment.js') }}\"></script>
    #}{#<script type=\"text/javascript\" src=\"/path/to/bootstrap/js/transition.js\"></script>#}{#
    <script  src=\"{{ asset('collapse.js') }}\"></script>

    <script  src=\"{{ asset('bootstrap-datetimepicker-master/build/js/bootstrap-datetimepicker.js') }}\"></script>
    #}
    {#<script type=\"text/javascript\" src=\"/path/to/bootstrap-datetimepicker.min.js\"></script>#}

    <!-- Bootstrap Core JavaScript -->
    {#<script src=\"{{ asset('plugins/bower_components/bootstrap-rtl-master/dist/js/bootstrap-rtl.min.js') }}\"></script>#}
    <!-- Menu Plugin JavaScript -->
    <script src=\"{{ asset('plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js') }}\"></script>
    <!--slimscroll JavaScript -->
    <script src=\"{{ asset('js/jquery.slimscroll.js') }}\"></script>
    <!--Wave Effects -->
    <script src=\"{{ asset('js/waves.js') }}\"></script>
    <!-- Flot Charts JavaScript -->
    {#<script src=\"{{ asset('plugins/bower_components/flot/jquery.flot.js') }}\"></script>
    <script src=\"{{ asset('plugins/bower_components/flot.tooltip/js/jquery.flot.tooltip.min.js') }}\"></script>#}
    <!-- google maps api -->
    {#<script src=\"{{ asset('plugins/bower_components/vectormap/jquery-jvectormap-2.0.2.min.js') }}\"></script>
    <script src=\"{{ asset('plugins/bower_components/vectormap/jquery-jvectormap-world-mill-en.js') }}\"></script>#}
    <!-- Sparkline charts -->
    <script src=\"{{ asset('plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js') }}\"></script>
    <!-- EASY PIE CHART JS -->
    <script src=\"{{ asset('plugins/bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js') }}\"></script>
    <script src=\"{{ asset('plugins/bower_components/jquery.easy-pie-chart/easy-pie-chart.init.js') }}\"></script>
    <!-- Custom Theme JavaScript -->
    <script src=\"{{ asset('js/custom.min.js') }}\"></script>
    {#<script src=\"{{ asset('js/dashboard2.js') }}\"></script>#}
    <!--Style Switcher -->
    <script src=\"{{ asset('plugins/bower_components/styleswitcher/jQuery.style.switcher.js') }}\"></script>
{% endblock %}

</body>
</html>
", "::layoutAdmin.html.twig", "C:\\wamp64\\www\\doctorAdmin\\app/Resources\\views/layoutAdmin.html.twig");
    }
}
