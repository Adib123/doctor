<?php

/* @FOSUser/layout.html.twig */
class __TwigTemplate_20cdcdc71119a5a1b50f8337447c3d9047e483c58360d8bb35b8e7c21748594e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d7fd867b0a71e3652b293b4832a5ebdce7e061d16f5e097fc64195fcc24cabcd = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d7fd867b0a71e3652b293b4832a5ebdce7e061d16f5e097fc64195fcc24cabcd->enter($__internal_d7fd867b0a71e3652b293b4832a5ebdce7e061d16f5e097fc64195fcc24cabcd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/layout.html.twig"));

        $__internal_2a43672bf5e168b820f31638abefa1719f47dd7308400ef34eb6e9c14f14600a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2a43672bf5e168b820f31638abefa1719f47dd7308400ef34eb6e9c14f14600a->enter($__internal_2a43672bf5e168b820f31638abefa1719f47dd7308400ef34eb6e9c14f14600a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/layout.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <meta charset=\"utf-8\">
        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
        <meta name=\"description\" content=\"\">
        <meta name=\"author\" content=\"\">
        <title>Authentification</title>
        <!-- Bootstrap Core CSS -->
        <link href=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("plugins/bower_components/bootstrap-rtl-master/dist/css/bootstrap-rtl.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
        <!-- animation CSS -->
        <link href=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/animate.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
        <!-- Custom CSS -->
        <link href=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/style.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
        <!-- color CSS -->
        <link href=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/colors/blue.css"), "html", null, true);
        echo "\" id=\"theme\"  rel=\"stylesheet\">
        <link rel=\"stylesheet\" href=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/style_business.css"), "html", null, true);
        echo "\">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src=\"https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js\"></script>
        <script src=\"https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js\"></script>
        <![endif]-->
    </head>
    <body>
        ";
        // line 38
        echo "
        ";
        // line 39
        if ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "hasPreviousSession", array())) {
            // line 40
            echo "            ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashbag", array()), "all", array(), "method"));
            foreach ($context['_seq'] as $context["type"] => $context["messages"]) {
                // line 41
                echo "                ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($context["messages"]);
                foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
                    // line 42
                    echo "                    <div class=\"flash-";
                    echo twig_escape_filter($this->env, $context["type"], "html", null, true);
                    echo "\">
                        ";
                    // line 43
                    echo twig_escape_filter($this->env, $context["message"], "html", null, true);
                    echo "
                    </div>
                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 46
                echo "            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['type'], $context['messages'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 47
            echo "        ";
        }
        // line 48
        echo "
        <div>
            ";
        // line 50
        $this->displayBlock('fos_user_content', $context, $blocks);
        // line 52
        echo "        </div>

        <script src=\"";
        // line 54
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("plugins/bower_components/jquery/dist/jquery.min.js"), "html", null, true);
        echo "\"></script>
        <!-- Bootstrap Core JavaScript -->
        <script src=\"";
        // line 56
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("plugins/bower_components/bootstrap-rtl-master/dist/js/bootstrap-rtl.min.js"), "html", null, true);
        echo "\"></script>
        <!-- Menu Plugin JavaScript -->
        <script src=\"";
        // line 58
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"), "html", null, true);
        echo "\"></script>

        <!--slimscroll JavaScript -->
        <script src=\"";
        // line 61
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/jquery.slimscroll.js"), "html", null, true);
        echo "\"></script>
        <!--Wave Effects -->
        <script src=\"";
        // line 63
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/waves.js"), "html", null, true);
        echo "\"></script>
        <!-- Custom Theme JavaScript -->
        <script src=\"";
        // line 65
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/custom.min.js"), "html", null, true);
        echo "\"></script>
        <!--Style Switcher -->
        <script src=\"";
        // line 67
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("plugins/bower_components/styleswitcher/jQuery.style.switcher.js"), "html", null, true);
        echo "\"></script>
    </body>
</html>
";
        
        $__internal_d7fd867b0a71e3652b293b4832a5ebdce7e061d16f5e097fc64195fcc24cabcd->leave($__internal_d7fd867b0a71e3652b293b4832a5ebdce7e061d16f5e097fc64195fcc24cabcd_prof);

        
        $__internal_2a43672bf5e168b820f31638abefa1719f47dd7308400ef34eb6e9c14f14600a->leave($__internal_2a43672bf5e168b820f31638abefa1719f47dd7308400ef34eb6e9c14f14600a_prof);

    }

    // line 50
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_314a1f18857278c53265bb804c46e32805a00cdaa08d2e0baaf77e571bdfb975 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_314a1f18857278c53265bb804c46e32805a00cdaa08d2e0baaf77e571bdfb975->enter($__internal_314a1f18857278c53265bb804c46e32805a00cdaa08d2e0baaf77e571bdfb975_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_6c4c60e96e62ee63f288e3426f79f08bf45604479175d592b5651c4eb5406ee0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6c4c60e96e62ee63f288e3426f79f08bf45604479175d592b5651c4eb5406ee0->enter($__internal_6c4c60e96e62ee63f288e3426f79f08bf45604479175d592b5651c4eb5406ee0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 51
        echo "            ";
        
        $__internal_6c4c60e96e62ee63f288e3426f79f08bf45604479175d592b5651c4eb5406ee0->leave($__internal_6c4c60e96e62ee63f288e3426f79f08bf45604479175d592b5651c4eb5406ee0_prof);

        
        $__internal_314a1f18857278c53265bb804c46e32805a00cdaa08d2e0baaf77e571bdfb975->leave($__internal_314a1f18857278c53265bb804c46e32805a00cdaa08d2e0baaf77e571bdfb975_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  172 => 51,  163 => 50,  149 => 67,  144 => 65,  139 => 63,  134 => 61,  128 => 58,  123 => 56,  118 => 54,  114 => 52,  112 => 50,  108 => 48,  105 => 47,  99 => 46,  90 => 43,  85 => 42,  80 => 41,  75 => 40,  73 => 39,  70 => 38,  58 => 19,  54 => 18,  49 => 16,  44 => 14,  39 => 12,  26 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <meta charset=\"utf-8\">
        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
        <meta name=\"description\" content=\"\">
        <meta name=\"author\" content=\"\">
        <title>Authentification</title>
        <!-- Bootstrap Core CSS -->
        <link href=\"{{ asset('plugins/bower_components/bootstrap-rtl-master/dist/css/bootstrap-rtl.min.css') }}\" rel=\"stylesheet\">
        <!-- animation CSS -->
        <link href=\"{{ asset('css/animate.css') }}\" rel=\"stylesheet\">
        <!-- Custom CSS -->
        <link href=\"{{ asset('css/style.css') }}\" rel=\"stylesheet\">
        <!-- color CSS -->
        <link href=\"{{ asset('css/colors/blue.css') }}\" id=\"theme\"  rel=\"stylesheet\">
        <link rel=\"stylesheet\" href=\"{{ asset('css/style_business.css') }}\">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src=\"https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js\"></script>
        <script src=\"https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js\"></script>
        <![endif]-->
    </head>
    <body>
        {#<div>
            {% if is_granted(\"IS_AUTHENTICATED_REMEMBERED\") %}
                {{ 'layout.logged_in_as'|trans({'%username%': app.user.username}, 'FOSUserBundle') }} |
                <a href=\"{{ path('fos_user_security_logout') }}\">
                    {{ 'layout.logout'|trans({}, 'FOSUserBundle') }}
                </a>
            {% else %}
                <a href=\"{{ path('fos_user_security_login') }}\">{{ 'layout.login'|trans({}, 'FOSUserBundle') }}</a>
            {% endif %}
        </div>#}

        {% if app.request.hasPreviousSession %}
            {% for type, messages in app.session.flashbag.all() %}
                {% for message in messages %}
                    <div class=\"flash-{{ type }}\">
                        {{ message }}
                    </div>
                {% endfor %}
            {% endfor %}
        {% endif %}

        <div>
            {% block fos_user_content %}
            {% endblock fos_user_content %}
        </div>

        <script src=\"{{ asset('plugins/bower_components/jquery/dist/jquery.min.js') }}\"></script>
        <!-- Bootstrap Core JavaScript -->
        <script src=\"{{ asset('plugins/bower_components/bootstrap-rtl-master/dist/js/bootstrap-rtl.min.js') }}\"></script>
        <!-- Menu Plugin JavaScript -->
        <script src=\"{{ asset('plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js') }}\"></script>

        <!--slimscroll JavaScript -->
        <script src=\"{{ asset('js/jquery.slimscroll.js') }}\"></script>
        <!--Wave Effects -->
        <script src=\"{{ asset('js/waves.js') }}\"></script>
        <!-- Custom Theme JavaScript -->
        <script src=\"{{ asset('js/custom.min.js') }}\"></script>
        <!--Style Switcher -->
        <script src=\"{{ asset('plugins/bower_components/styleswitcher/jQuery.style.switcher.js') }}\"></script>
    </body>
</html>
", "@FOSUser/layout.html.twig", "C:\\wamp64\\www\\doctorAdmin\\app\\Resources\\FOSUserBundle\\views\\layout.html.twig");
    }
}
