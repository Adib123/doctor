<?php

/* @WebProfiler/Collector/exception.html.twig */
class __TwigTemplate_d0ea04b0681df5be4f4372891dbf57cd265f736c45c771d50e480f083206011b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/exception.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_40be5854e7d5c750e154c81d871663ebf165fba3c8e43dbfdf707e1f85d6a22a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_40be5854e7d5c750e154c81d871663ebf165fba3c8e43dbfdf707e1f85d6a22a->enter($__internal_40be5854e7d5c750e154c81d871663ebf165fba3c8e43dbfdf707e1f85d6a22a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/exception.html.twig"));

        $__internal_8dc7e8dcc5997fd1f9112fabc253ca832ab13e3acfec41e4f818ac3649996e61 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8dc7e8dcc5997fd1f9112fabc253ca832ab13e3acfec41e4f818ac3649996e61->enter($__internal_8dc7e8dcc5997fd1f9112fabc253ca832ab13e3acfec41e4f818ac3649996e61_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/exception.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_40be5854e7d5c750e154c81d871663ebf165fba3c8e43dbfdf707e1f85d6a22a->leave($__internal_40be5854e7d5c750e154c81d871663ebf165fba3c8e43dbfdf707e1f85d6a22a_prof);

        
        $__internal_8dc7e8dcc5997fd1f9112fabc253ca832ab13e3acfec41e4f818ac3649996e61->leave($__internal_8dc7e8dcc5997fd1f9112fabc253ca832ab13e3acfec41e4f818ac3649996e61_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_d53736be126f2ab833a23322ee7fe4401eb4fda4fb519ef8f44208712b557695 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d53736be126f2ab833a23322ee7fe4401eb4fda4fb519ef8f44208712b557695->enter($__internal_d53736be126f2ab833a23322ee7fe4401eb4fda4fb519ef8f44208712b557695_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_6023511eeb4774e04a0aaa586ed1ea04f5e799b728122205f97dfcdf4b5ca539 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6023511eeb4774e04a0aaa586ed1ea04f5e799b728122205f97dfcdf4b5ca539->enter($__internal_6023511eeb4774e04a0aaa586ed1ea04f5e799b728122205f97dfcdf4b5ca539_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    ";
        if ($this->getAttribute((isset($context["collector"]) ? $context["collector"] : $this->getContext($context, "collector")), "hasexception", array())) {
            // line 5
            echo "        <style>
            ";
            // line 6
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception_css", array("token" => (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")))));
            echo "
        </style>
    ";
        }
        // line 9
        echo "    ";
        $this->displayParentBlock("head", $context, $blocks);
        echo "
";
        
        $__internal_6023511eeb4774e04a0aaa586ed1ea04f5e799b728122205f97dfcdf4b5ca539->leave($__internal_6023511eeb4774e04a0aaa586ed1ea04f5e799b728122205f97dfcdf4b5ca539_prof);

        
        $__internal_d53736be126f2ab833a23322ee7fe4401eb4fda4fb519ef8f44208712b557695->leave($__internal_d53736be126f2ab833a23322ee7fe4401eb4fda4fb519ef8f44208712b557695_prof);

    }

    // line 12
    public function block_menu($context, array $blocks = array())
    {
        $__internal_a03df34fc0df7ddb3a6e87736afa6f2d327779b08766da2ff8b7e904c8597d9d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a03df34fc0df7ddb3a6e87736afa6f2d327779b08766da2ff8b7e904c8597d9d->enter($__internal_a03df34fc0df7ddb3a6e87736afa6f2d327779b08766da2ff8b7e904c8597d9d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_78438d4fcb62213bc7765dfcced3e6e8ef4242f45de2c45bafcf435040e024a5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_78438d4fcb62213bc7765dfcced3e6e8ef4242f45de2c45bafcf435040e024a5->enter($__internal_78438d4fcb62213bc7765dfcced3e6e8ef4242f45de2c45bafcf435040e024a5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 13
        echo "    <span class=\"label ";
        echo (($this->getAttribute((isset($context["collector"]) ? $context["collector"] : $this->getContext($context, "collector")), "hasexception", array())) ? ("label-status-error") : ("disabled"));
        echo "\">
        <span class=\"icon\">";
        // line 14
        echo twig_include($this->env, $context, "@WebProfiler/Icon/exception.svg");
        echo "</span>
        <strong>Exception</strong>
        ";
        // line 16
        if ($this->getAttribute((isset($context["collector"]) ? $context["collector"] : $this->getContext($context, "collector")), "hasexception", array())) {
            // line 17
            echo "            <span class=\"count\">
                <span>1</span>
            </span>
        ";
        }
        // line 21
        echo "    </span>
";
        
        $__internal_78438d4fcb62213bc7765dfcced3e6e8ef4242f45de2c45bafcf435040e024a5->leave($__internal_78438d4fcb62213bc7765dfcced3e6e8ef4242f45de2c45bafcf435040e024a5_prof);

        
        $__internal_a03df34fc0df7ddb3a6e87736afa6f2d327779b08766da2ff8b7e904c8597d9d->leave($__internal_a03df34fc0df7ddb3a6e87736afa6f2d327779b08766da2ff8b7e904c8597d9d_prof);

    }

    // line 24
    public function block_panel($context, array $blocks = array())
    {
        $__internal_abe28b999b027ac7dd20862801fa6c23f040aed9c2cf9dfe054a011bdaf0c204 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_abe28b999b027ac7dd20862801fa6c23f040aed9c2cf9dfe054a011bdaf0c204->enter($__internal_abe28b999b027ac7dd20862801fa6c23f040aed9c2cf9dfe054a011bdaf0c204_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_62bb51c06ef21c01397aa615b3bb1e6cf85821ad62501203c162300dae023811 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_62bb51c06ef21c01397aa615b3bb1e6cf85821ad62501203c162300dae023811->enter($__internal_62bb51c06ef21c01397aa615b3bb1e6cf85821ad62501203c162300dae023811_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 25
        echo "    <h2>Exceptions</h2>

    ";
        // line 27
        if ( !$this->getAttribute((isset($context["collector"]) ? $context["collector"] : $this->getContext($context, "collector")), "hasexception", array())) {
            // line 28
            echo "        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    ";
        } else {
            // line 32
            echo "        <div class=\"sf-reset\">
            ";
            // line 33
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception", array("token" => (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")))));
            echo "
        </div>
    ";
        }
        
        $__internal_62bb51c06ef21c01397aa615b3bb1e6cf85821ad62501203c162300dae023811->leave($__internal_62bb51c06ef21c01397aa615b3bb1e6cf85821ad62501203c162300dae023811_prof);

        
        $__internal_abe28b999b027ac7dd20862801fa6c23f040aed9c2cf9dfe054a011bdaf0c204->leave($__internal_abe28b999b027ac7dd20862801fa6c23f040aed9c2cf9dfe054a011bdaf0c204_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/exception.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  138 => 33,  135 => 32,  129 => 28,  127 => 27,  123 => 25,  114 => 24,  103 => 21,  97 => 17,  95 => 16,  90 => 14,  85 => 13,  76 => 12,  63 => 9,  57 => 6,  54 => 5,  51 => 4,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block head %}
    {% if collector.hasexception %}
        <style>
            {{ render(path('_profiler_exception_css', { token: token })) }}
        </style>
    {% endif %}
    {{ parent() }}
{% endblock %}

{% block menu %}
    <span class=\"label {{ collector.hasexception ? 'label-status-error' : 'disabled' }}\">
        <span class=\"icon\">{{ include('@WebProfiler/Icon/exception.svg') }}</span>
        <strong>Exception</strong>
        {% if collector.hasexception %}
            <span class=\"count\">
                <span>1</span>
            </span>
        {% endif %}
    </span>
{% endblock %}

{% block panel %}
    <h2>Exceptions</h2>

    {% if not collector.hasexception %}
        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    {% else %}
        <div class=\"sf-reset\">
            {{ render(path('_profiler_exception', { token: token })) }}
        </div>
    {% endif %}
{% endblock %}
", "@WebProfiler/Collector/exception.html.twig", "C:\\wamp64\\www\\doctorAdmin\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle\\Resources\\views\\Collector\\exception.html.twig");
    }
}
