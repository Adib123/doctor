<?php

/* @FOSUser/Security/login.html.twig */
class __TwigTemplate_03e9fb3a88e3c8a3ddd0164717f7a9a616e1b071534a6e38a35b68fba6b20a37 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "@FOSUser/Security/login.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_eba9a3c3433eeaf999a179a10d16f2e1895fa6bdc67be64a2522013a61dd55de = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_eba9a3c3433eeaf999a179a10d16f2e1895fa6bdc67be64a2522013a61dd55de->enter($__internal_eba9a3c3433eeaf999a179a10d16f2e1895fa6bdc67be64a2522013a61dd55de_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Security/login.html.twig"));

        $__internal_5892240201417e0f93bd6d9f845efb6f747da38d25ec3a62bf0df5b34359f718 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5892240201417e0f93bd6d9f845efb6f747da38d25ec3a62bf0df5b34359f718->enter($__internal_5892240201417e0f93bd6d9f845efb6f747da38d25ec3a62bf0df5b34359f718_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Security/login.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_eba9a3c3433eeaf999a179a10d16f2e1895fa6bdc67be64a2522013a61dd55de->leave($__internal_eba9a3c3433eeaf999a179a10d16f2e1895fa6bdc67be64a2522013a61dd55de_prof);

        
        $__internal_5892240201417e0f93bd6d9f845efb6f747da38d25ec3a62bf0df5b34359f718->leave($__internal_5892240201417e0f93bd6d9f845efb6f747da38d25ec3a62bf0df5b34359f718_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_a6af026d2cc25e73d434931a1cc648ab67cd56ceaa5358303289424ed2a5884a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a6af026d2cc25e73d434931a1cc648ab67cd56ceaa5358303289424ed2a5884a->enter($__internal_a6af026d2cc25e73d434931a1cc648ab67cd56ceaa5358303289424ed2a5884a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_84e15a3f65bf1e42aae7d7dcb7cfd782fc56e1325d5bac0a250346bbbd324bec = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_84e15a3f65bf1e42aae7d7dcb7cfd782fc56e1325d5bac0a250346bbbd324bec->enter($__internal_84e15a3f65bf1e42aae7d7dcb7cfd782fc56e1325d5bac0a250346bbbd324bec_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        echo "    ";
        echo twig_include($this->env, $context, "@FOSUser/Security/login_content.html.twig");
        echo "
";
        
        $__internal_84e15a3f65bf1e42aae7d7dcb7cfd782fc56e1325d5bac0a250346bbbd324bec->leave($__internal_84e15a3f65bf1e42aae7d7dcb7cfd782fc56e1325d5bac0a250346bbbd324bec_prof);

        
        $__internal_a6af026d2cc25e73d434931a1cc648ab67cd56ceaa5358303289424ed2a5884a->leave($__internal_a6af026d2cc25e73d434931a1cc648ab67cd56ceaa5358303289424ed2a5884a_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Security/login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
    {{ include('@FOSUser/Security/login_content.html.twig') }}
{% endblock fos_user_content %}
", "@FOSUser/Security/login.html.twig", "C:\\wamp64\\www\\doctorAdmin\\app\\Resources\\FOSUserBundle\\views\\Security\\login.html.twig");
    }
}
