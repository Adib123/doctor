API Doctors
===================

----------


###User:

 1. **Add account**

 

HTTP Verb :
POST       

URL :
```json
/user_api_add
```  

data :
```json
  {
    "phoneNumber": 26715671,
    "password" : 123456,
    "token" : "56476540zdfs6f8408"
  }
```  
  
  
Return :
```json
HTTP/1.1 200 OK
[
  {
    "success": 1,
    "id": 14
  }
]
```  

 2. **VerifiedAccount**

  


HTTP Verb :
POST       

URL :
```json
/user_verify_api/{id}
```  

data :
```json
  {
    code: 3871,
  }
```  
  
  
Return :
```json
HTTP/1.1 200 OK
[
  {
   "verification":1,
   "id":"14"
  }
]
```  

 3. **Update User**

  


HTTP Verb :
POST       

URL :
```json
edit_user_api/{id}
```  

data :
```json
  {
    name : "akrem",
    country : "السعودية",
    town : "مكة الكرمة",
    city : "حي السلامة",
    email : "akrem.boussaha@gmail.com",
  }
```  
  
  
Return :
```json
HTTP/1.1 200 OK
[
  {
    "succcess" : 1, 
    "id" : 14
  }
]
``` 
4. **Complete user profile**


HTTP Verb :
POST       

URL :
```json
user_profile_complete_api/14
```  

data :
```json
  {
    "name": "akrem",
    "country" : "السعودية",
    "town" : "المدينة المنورة",
    "city" : "حي السلامة",
    "email" : "akrem.boussaha@gmail.com"
  }
```  
  
  
Return :
```json
HTTP/1.1 200 OK
[
  {
    "succcess" : 1, 
    "id" : 1,
  }
]
```  


5. **Show User Profile** 


HTTP Verb :
GET       

URL :
```json
user_show_profile_api/{id}
```  

data :
```json
  {
    "id": "1",
  }
```  
  
  
Return :
```json
HTTP/1.1 200 OK
[
  {
  "success": 1,
  "data": {
    "userId": 1,
    "name": "Akrem",
    "phoneNumber": 55956566,
    "country": "Tunisia",
    "town": "Jendouba",
    "city": "Ferdaous",
    "photo": "/uploads/users/6448a630205ddec1f67d971b97ef92e6.png",
    "email": "akrem.boussaha@gmail.com",
    "verified": 0
  }
}
]
```   
6.**Reset password User**


HTTP Verb :
POST       

URL :
```json
/user_reset_password_api
```  

data :
```json
  {
    "phoneNumber": 97956214 ,
  }
```  
  
  
Return :
```json
HTTP/1.1 200 OK
[
  {
    "succcess" : 1, 
    "id" : 1,
  }
]
```  

7.**Delete User**


HTTP Verb :
DELETE     

URL :
```json
/api_delete_user/14
```  

data :
```json
  {
  }
```  
  
  
Return :
```json
HTTP/1.1 200 OK
[
  {
  "success": 1,
  "id": "14"
  }
]
```

8  .**Doctor list**


HTTP Verb :
GET  

URL :
```json
/list_doctors
```  

data :
```json
  {
  }
```  
  
  
Return :
```json
HTTP/1.1 200 OK
[
  {
	  "success": 1,
	  "data": [
	    {
	      "doctorId": 14,
	      "nameDoctor": "أديب عوادي",
	      "nationality": "تمنسية",
	      "phoneNumberDoctor": 96321478,
	      "emailDoctor": "aouadi_adib@gmail.com",
	      "workplaceName": "مستشفى النور",
	      "workplace": "مكة المكرمة",
	      "countryDoctor": "السعودية",
	      "photoDoctor": "uploads/doctors/e3450133a2d5c2463c7e9b164efa5912.jpeg",
	      "townDoctor": "مكة المكرمة",
	      "cityDoctor": "حي السلامة",
	      "speciality": "الأسنان",
	      "longitude": null,
	      "lattitude": null,
	      "profile_completed": false
	    },
	    {
	      "doctorId": 12,
	      "nameDoctor": "Hamza",
	      "nationality": "Tunisienne",
	      "phoneNumberDoctor": 71321456,
	      "emailDoctor": "hamza@gmail.com",
	      "workplaceName": null,
	      "workplace": null,
	      "countryDoctor": "Tunisia",
	      "photoDoctor": "uploads/doctors/6eb7d4adc09a8030caea19287b4fe3cb.jpeg",
	      "townDoctor": "Jendouba",
	      "cityDoctor": "Elferdaous",
	      "speciality": "القلب",
	      "longitude": "10.37109375",
	      "lattitude": "32.217448573031014",
	      "profile_completed": true
	    },
	    {
	      "doctorId": 11,
	      "nameDoctor": "سمير سلطاني",
	      "nationality": "سعودية",
	      "phoneNumberDoctor": 97854658,
	      "emailDoctor": "akjdsej@edflekjdf.com",
	      "workplaceName": "مستشفى اللك سلمان",
	      "workplace": "المدينة",
	      "countryDoctor": "السعودية",
	      "photoDoctor": "uploads/doctors/d048018adb2b6b2aa7a6614429fb6f37.jpeg",
	      "townDoctor": "المدينة المنورة",
	      "cityDoctor": "حي السلام",
	      "speciality": "القلب",
	      "longitude": "10.4534912109375",
	      "lattitude": "32.939538898778416",
	      "profile_completed": false
	    },
	    {
	      "doctorId": 9,
	      "nameDoctor": "بتصثناق",
	      "nationality": "ستنيبصثل",
	      "phoneNumberDoctor": 565585648,
	      "emailDoctor": "sdklfjsdj@dfkdh.com",
	      "workplaceName": "بجحصثخعبهص",
	      "workplace": "بصثلا",
	      "countryDoctor": "السعودية",
	      "photoDoctor": "uploads/doctors/60fd47f96c8f0342670d55e0a785e01f.jpeg",
	      "townDoctor": "سيبمسيب",
	      "cityDoctor": "يبصثتنلبصثفا",
	      "speciality": "العيون",
	      "longitude": "10.61279296875",
	      "lattitude": "36.43896124085945\n",
	      "profile_completed": false
	    },
	    {
	      "doctorId": 7,
	      "nameDoctor": "صالح",
	      "nationality": "جزائرية",
	      "phoneNumberDoctor": 21365475,
	      "emailDoctor": "saleh@gmail.com",
	      "workplaceName": "المستشفى الجهوي بجندوبة",
	      "workplace": "جندوبة",
	      "countryDoctor": "الجزائر",
	      "photoDoctor": "uploads/doctors/f4964ee427f3f368599b546a5b79132c.jpeg",
	      "townDoctor": "وهران",
	      "cityDoctor": "الفردوس",
	      "speciality": "الأسنان",
	      "longitude": "9.86572265625",
	      "lattitude": "37.2456348218214",
	      "profile_completed": false
	    },
	    {
	      "doctorId": 2,
	      "nameDoctor": "محمد",
	      "nationality": "تونسية",
	      "phoneNumberDoctor": null,
	      "emailDoctor": "med@gmail.com",
	      "workplaceName": "مستشفى النور",
	      "workplace": "مكة المكرمة",
	      "countryDoctor": "السعودية",
	      "photoDoctor": "uploads/doctors/f0a254ed923ccd732720db90f6a385e8.jpeg",
	      "townDoctor": "مكة المكرمة",
	      "cityDoctor": "النور",
	      "speciality": "العيون",
	      "longitude": "8.7176513671875",
	      "lattitude": "36.52288052805137",
	      "profile_completed": false
	    },
	    {
	      "doctorId": 1,
	      "nameDoctor": "عماد سلطاني",
	      "nationality": "تونسية",
	      "phoneNumberDoctor": null,
	      "emailDoctor": "akrem.boussaha@gmail.com",
	      "workplaceName": "BUSINESSOLOGY PLUS",
	      "workplace": "شارع الطيب المهيري",
	      "countryDoctor": "تونس",
	      "photoDoctor": "uploads/doctors/4629d7cfea51478b976a7a713095e774.jpeg",
	      "townDoctor": "جندوبة",
	      "cityDoctor": "السواني",
	      "speciality": "العيون",
	      "longitude": "8.7176513671875",
	      "lattitude": "36.52288052805137",
	      "profile_completed": false
	    }
  ]
}
]
```
9.**Complete doctor profile **


HTTP Verb :
POST   

URL :
```json
/doctor_profile_complete_api/13
```  

data :
```json
  {
    "nameDoctor" : "mohamed Ali",
    "nationality" : "Tunisienne",
    "countryDoctor" : "Saudi",
    "townDoctor" : "mekkah",
    "cityDoctor" : "cité Salem",
    "photoDoctor" : "sks.jpg",
    "emailDoctor" : "dali@gmail.com",
    "longitude" : "9.684604897894",
    "lattitude" : "35.68547542554",
    "workplace" : "Jendouba",
    "workplaceName" : "hopital regional de jendouba",
    "tokenDoctor" : "sdfdsf587sd4542142c54vcd5458"
  }
```  
  
  
Return :
```json
HTTP/1.1 200 OK
[
  {
  "success": 1,
  "id": 13
  }
]
```
10.**Search Doctor**


HTTP Verb :
POST     

URL :
```json
/search_doctor
```  

data :
```json
  {
   "countryDoctor" : "Saudi",
   "townDoctor" : "mekkah",
   "cityDoctor" : "cité Salem",
   "speciality_id" : 2,
  }
```  
  
  
Return :
```json
HTTP/1.1 200 OK
[
  {
    "doctorId": 1,
    "nameDoctor": "عماد سلطاني",
    "photoDoctor": "/uploads/doctors/4629d7cfea51478b976a7a713095e774.jpeg",
    "countryDoctor": "تونس",
    "townDoctor": "جندوبة",
    "workplaceDoctor": "شارع الطيب المهيري",
    "workplaceNameDoctor": "BUSINESSOLOGY PLUS",
    "speciality": "العيون",
    "longitude": "8.7176513671875",
    "lattitude": "36.52288052805137"
  },
  {
    "doctorId": 2,
    "nameDoctor": "محمد",
    "photoDoctor": "/uploads/doctors/5936aa26e3872.png",
    "countryDoctor": "السعودية",
    "townDoctor": "مكة المكرمة",
    "workplaceDoctor": "مكة المكرمة",
    "workplaceNameDoctor": "مستشفى النور",
    "speciality": "العيون",
    "longitude": "8.7176513671875",
    "lattitude": "36.52288052805137"
  },
  {
    "doctorId": 7,
    "nameDoctor": "صالح",
    "photoDoctor": "/uploads/doctors/f4964ee427f3f368599b546a5b79132c.jpeg",
    "countryDoctor": "الجزائر",
    "townDoctor": "وهران",
    "workplaceDoctor": "جندوبة",
    "workplaceNameDoctor": "المستشفى الجهوي بجندوبة",
    "speciality": "الأسنان",
    "longitude": "9.86572265625",
    "lattitude": "37.2456348218214"
  }
]
```
11.**Calculate Distance**


HTTP Verb :
POST     

URL :
```json
/doctor_distance
```  

data :
```json
  {
   "lattitude" : "35.68547542554",
   "longitude" : "9.7844514451214",
  }
```  
  
  
Return :
```json
HTTP/1.1 200 OK
[
  {
    "id": 1,
    "nameDoctor": "عماد سلطاني",
    "photoDoctor": "/uploads/doctors/4629d7cfea51478b976a7a713095e774.jpeg",
    "countryDoctor": "تونس",
    "townDoctor": "جندوبة",
    "workplaceDoctor": "شارع الطيب المهيري",
    "workplaceNameDoctor": "BUSINESSOLOGY PLUS",
    "speciality": "العيون",
    "longitude": "8.7176513671875",
    "lattitude": "36.52288052805137"
  },
  {
    "id": 2,
    "nameDoctor": "محمد",
    "photoDoctor": "/uploads/doctors/5936b46488e34.png",
    "countryDoctor": "السعودية",
    "townDoctor": "مكة المكرمة",
    "workplaceDoctor": "مكة المكرمة",
    "workplaceNameDoctor": "مستشفى النور",
    "speciality": "العيون",
    "longitude": "8.7176513671875",
    "lattitude": "36.52288052805137"
  },
  {
    "id": 7,
    "nameDoctor": "صالح",
    "photoDoctor": "/uploads/doctors/f4964ee427f3f368599b546a5b79132c.jpeg",
    "countryDoctor": "الجزائر",
    "townDoctor": "وهران",
    "workplaceDoctor": "جندوبة",
    "workplaceNameDoctor": "المستشفى الجهوي بجندوبة",
    "speciality": "الأسنان",
    "longitude": "9.86572265625",
    "lattitude": "37.2456348218214"
  },
]
```

12.**Update Doctor**


HTTP Verb :
POST     

URL :
```json
/doctor_update_api/{id}
```  

data :
```json
  {
   "phoneNumberDoctor" : "53475456",
   "passwordDoctor" : "sdgrgerg87j567m53a",
   "nationality" : "Tunisienne",
   "nameDoctor" : "Akrem Boussaha",
   "countryDoctor" : "Saudi",
   "townDoctor" : "Mekkah",
   "cityDoctor" : "Salum city",
   "emailDoctor" : "akrem.boussaha@gmail.com",
   "workplace" : "Mekka Mukuarram",
   "workplaceName" : "El nour Hospital",
   "startTimeWork" : "08:00:00",
   "endTimeWork" : "17:00:00",
   "longitude" : "35.5472876645",
   "lattitude" : "9.5876468734",
   "speciality_id" : "2",
  }
```  
  
  
Return :
```json
HTTP/1.1 200 OK
[
  {
  "success": 1,
  "id": 13
  }
]
```

13.**Reset Doctor's password**


HTTP Verb :
POST     

URL :
```json
/doctor_reset_password_api
```  

data :
```json
  {
   "phoneNumberDoctor" : "53475456",
  }
```  
  
  
Return :
```json
HTTP/1.1 200 OK
[
  {
  "success": 1,
  "id": 13
  }
]
```
14.**Login User / Doctor**


HTTP Verb :
POST     

URL :
```json
/login_api
```  

data :
```json
  {
   "phoneNumber" : "78451231",
   "password" : "123456"
  }
```  
  
  
Return :
```json
HTTP/1.1 200 OK
[
  {
    "login": 1,
    "isDoctor": 0,
    "isUser": 1,
    "user_id": 12
 }
]
```


15.**Delete doctor**


HTTP Verb :
DELETE    

URL :
```json
api_delete_doctor/13
```  

data :
```json
  {
  }
```  
  
  
Return :
```json
HTTP/1.1 200 OK
[
  {
  "success": 1,
  "id": "13"
  }
]
```

16.**GET Evaluation by doctor**


HTTP Verb :
GET   

URL :
```json
/get_evaluation_doctor/11
```  

data :
```json
  {   
  }
```  
  
  
Return :
```json
HTTP/1.1 200 OK
{
  "success": 1,
  "data": [
    {
      "evaluation_id": 8,
      "evaluation": 3,
      "evaluation_value": "ممتاز",
      "feedback": "hola",
      "doctor_id": 1,
      "name_doctor": "عماد سلطاني",
      "photo_doctor": "uploads/doctors/4629d7cfea51478b976a7a713095e774.jpeg",
      "user_id": 1,
      "username": "Akrem",
      "photo_user": "uploads/users/6448a630205ddec1f67d971b97ef92e6.png",
      "appointment_id": 54
    }
  ]
}
```

17.**GET Evaluation by User**


HTTP Verb :
GET   

URL :
```json
/get_evaluation_user/1
```  

data :
```json
  {   
  }
```  
  
  
Return :
```json
HTTP/1.1 200 OK
{
	 {
	  "success": 1,
	  "data": [
	    {
	      "evaluationId": 10,
	      "evaluation": 3,
	      "evaluation_value": "ممتاز",
	      "feedback": "bonne consultation",
	      "doctor_id": 11,
	      "name_doctor": "سمير سلطاني",
	      "photo_doctor": "uploads/doctors/d048018adb2b6b2aa7a6614429fb6f37.jpeg",
	      "user_id": 1,
	      "username": "Akrem",
	      "photo_user": "uploads/users/6448a630205ddec1f67d971b97ef92e6.png",
	      "appointment_id": 64
	    },
	    {
	      "evaluationId": 8,
	      "evaluation": 3,
	      "evaluation_value": "ممتاز",
	      "feedback": "hola",
	      "doctor_id": 1,
	      "name_doctor": "عماد سلطاني",
	      "photo_doctor": "uploads/doctors/4629d7cfea51478b976a7a713095e774.jpeg",
	      "user_id": 1,
	      "username": "Akrem",
	      "photo_user": "uploads/users/6448a630205ddec1f67d971b97ef92e6.png",
	      "appointment_id": 54
	    }
	  ]
	  }
}
  
```

18.**Update evaluation **


HTTP Verb :
POST 

URL :
```json
/evaluation_update_api/12
```  

data :
```json
  {
    "evaluation" : "سيء",
    "feedback" : "مردود سيء يضلع"   
  }
```  
  
  
Return :
```json
HTTP/1.1 200 OK
[
  {
    "success": 1,
    "id": 12
  }
]
```
19.**Add evaluation **


HTTP Verb :
POST

URL :
```json
/add_evaluation_api
```  

data :
```json
  {   
    "evaluationName" : "ممتاز"
    "feedback" : "نمتمتتمختههخ"
    "appointment_id" : 65
  }
```  
  
  
Return :
```json
HTTP/1.1 200 OK
[
  {
    "success": 1,
    "id": "6"
  }
]
```


20.**Delete evaluation **


HTTP Verb :
DELETE

URL :
```json
/delete_evaluation_api/12
```  

data :
```json
  {   
  }
```  
  
  
Return :
```json
HTTP/1.1 200 OK
[
  {
    "success": 1,
    "id": "12"
  }
]
```
21.**Load Hours work of doctor in the date appointment **


HTTP Verb :
POST

URL :
```json
/load_hour_work
```  

data :
```json
  {
    "dateAppointment" : "30-05-2017",
    "doctor_id" : 11   
  }
```  
  
  
Return :
```json
HTTP/1.1 200 OK
[
  {
    "date": "2017-05-30 08:00",
    "disponible": 1
  },
  {
    "date": "2017-05-30 08:30",
    "disponible": 1
  },
  {
    "date": "2017-05-30 09:00",
    "disponible": 1
  },
  {
    "date": "2017-05-30 09:30",
    "disponible": 0
  },
  {
    "date": "2017-05-30 10:00",
    "disponible": 0
  },
  {
    "date": "2017-05-30 10:30",
    "disponible": 1
  },
  {
    "date": "2017-05-30 11:00",
    "disponible": 0
  },
  {
    "date": "2017-05-30 11:30",
    "disponible": 1
  },
  {
    "date": "2017-05-30 12:00",
    "disponible": 0
  }
]
```
22.**Add appointment**


HTTP Verb :
POST

URL :
```json
/add_appointment
```  

data :
```json
  {
    "date_appointment" : "30-05-2017",
    "timeOfAppt" : "08:30:00",
    "doctor_id" : 11,
    "user_id" : 11   
  }
```  
  
  
Return :
```json
HTTP/1.1 200 OK
[
  {
    "success": 1,
    "id": 65
  }
]
```

23.**Update status appointment**


HTTP Verb :
POST

URL :
```json
/update_status_appointment/65
```  

data :
```json
  {
    "status" : "مؤكد", 
  }
```  
  
  
Return :
```json
HTTP/1.1 200 OK
[
  {
    "success": 1,
    "id": 65
  }
]
```

24.**List appointment by status**


HTTP Verb :
POST

URL :
```json
/list_appointment_confirmed
```  

data :
```json
  {
    "status" : "مؤكد", 
  }
```  
  
  
Return :
```json
HTTP/1.1 200 OK
[
  {
    "success": 1,
    "id": 65
  }
]
```

25.**List appointment by doctor**


HTTP Verb :
GET

URL :
```json
/list_appointment_by_doctor/11
```  

data :
```json
  {
  }
```  
  
  
Return :
```json
HTTP/1.1 200 OK
[
	   {
	  "success": 1,
	  "data": [
	    {
	      "appointmentId": 65,
	      "statusAppointment": "مؤكد",
	      "date": "2017-05-30",
	      "time": "08:30",
	      "doctor": 11,
	      "user": 11
	    },
	    {
	      "appointmentId": 63,
	      "statusAppointment": "معلق",
	      "date": "2017-05-30",
	      "time": "11:00",
	      "doctor": 11,
	      "user": 4
	    },
	    {
	      "appointmentId": 62,
	      "statusAppointment": "مؤكد",
	      "date": "2017-05-30",
	      "time": "14:30",
	      "doctor": 11,
	      "user": 3
	    },
	    {
	      "appointmentId": 61,
	      "statusAppointment": "معلق",
	      "date": "2017-05-30",
	      "time": "13:00",
	      "doctor": 11,
	      "user": 4
	    },
	    {
	      "appointmentId": 60,
	      "statusAppointment": "معلق",
	      "date": "2017-05-30",
	      "time": "12:00",
	      "doctor": 11,
	      "user": 4
	    },
	    {
	      "appointmentId": 59,
	      "statusAppointment": "معلق",
	      "date": "2017-05-30",
	      "time": "10:00",
	      "doctor": 11,
	      "user": 4
	    },
	    {
	      "appointmentId": 58,
	      "statusAppointment": "معلق",
	      "date": "2017-05-30",
	      "time": "09:30",
	      "doctor": 11,
	      "user": 1
	    },
	    {
	      "appointmentId": 40,
	      "statusAppointment": "معلق",
	      "date": "2017-05-03",
	      "time": "08:41",
	      "doctor": 11,
	      "user": 1
	    }
	  ]
	}
]
```
26.**send notification**


HTTP Verb :
GET

URL :
```json
/send_notification
```  

data :
```json
  {
    "status" : "مؤكد", 
  }
```  
  
  
Return :
```json
HTTP/1.1 200 OK
[
   {
    "appointmentId": 65,
    "statusAppointment": "مؤكد",
    "appointment": {
      "date": "2017-05-30 08:30:00.000000",
      "timezone_type": 3,
      "timezone": "UTC"
    },
    "doctor": 11,
    "user": 11
  },
  {
    "appointmentId": 64,
    "statusAppointment": "معلق",
    "appointment": {
      "date": "2017-05-26 09:30:00.000000",
      "timezone_type": 3,
      "timezone": "UTC"
    },
    "doctor": 11,
    "user": 1
  },
  {
    "appointmentId": 63,
    "statusAppointment": "معلق",
    "appointment": {
      "date": "2017-05-30 11:00:00.000000",
      "timezone_type": 3,
      "timezone": "UTC"
    },
    "doctor": 11,
    "user": 4
  },
]
```

27.**Delete appointment**


HTTP Verb :
DELETE

URL :
```json
/api_delete_appointment/64
```  

data :
```json
  {
  }
```  
  
  
Return :
```json
HTTP/1.1 200 OK
[
   {
    "success": 1,
    "id": "64"
   }
]
```
28.**List of Specialities**


HTTP Verb :
GET

URL :
```json
/list_speciality
```  

data :
```json
  {
  }
```  
  
  
Return :
```json
HTTP/1.1 200 OK
[
   {
	  "success": 1,
	  "data": [
	    {
	      "id": 3,
	      "nameSpeciality": "الأسنان"
	    },
	    {
	      "id": 2,
	      "nameSpeciality": "القلب"
	    },
	    {
	      "id": 1,
	      "nameSpeciality": "العيون"
	    }
	  ]
   }
]
```
29.**Add user token**


HTTP Verb :
POST

URL :
```json
/add_user_token
```  

data :
```json
  {
    user_id: 8
  }
```  
  
  
Return :
```json
HTTP/1.1 200 OK
[
   {
    "success": 1,
    "token_id": "1"
   }
]
```

30.**Add doctor token**


HTTP Verb :
POST

URL :
```json
/add_doctor_token
```  

data :
```json
  {
    doctor_id: 8
  }
```  
  
  
Return :
```json
HTTP/1.1 200 OK
[
   {
    "success": 1,
    "token_id": "1"
   }
]
```
31.**List doctors per speciality**


HTTP Verb :
GET

URL :
```json
/list_doctor_speciality/1
```  

data :
```json
  {
  }
```  
  
  
Return :
```json
HTTP/1.1 200 OK
[
   {
	  "success": 1,
	  "data": [
	    {
	      "doctorId": 9,
	      "nameDoctor": "بتصثناق",
	      "phoneNumberDoctor": 565585648,
	      "workplaceName": "بجحصثخعبهص",
	      "countryDoctor": "السعودية",
	      "townDoctor": "سيبمسيب",
	      "cityDoctor": "يبصثتنلبصثفا",
	      "speciality": "العيون",
	      "longitude": "10.61279296875",
	      "lattitude": "36.43896124085945\n"
	    },
	    {
	      "doctorId": 2,
	      "nameDoctor": "محمد",
	      "phoneNumberDoctor": null,
	      "workplaceName": "مستشفى النور",
	      "countryDoctor": "السعودية",
	      "townDoctor": "مكة المكرمة",
	      "cityDoctor": "النور",
	      "speciality": "العيون",
	      "longitude": "8.7176513671875",
	      "lattitude": "36.52288052805137"
	    },
	    {
	      "doctorId": 1,
	      "nameDoctor": "عماد سلطاني",
	      "phoneNumberDoctor": null,
	      "workplaceName": "BUSINESSOLOGY PLUS",
	      "countryDoctor": "تونس",
	      "townDoctor": "جندوبة",
	      "cityDoctor": "السواني",
	      "speciality": "العيون",
	      "longitude": "8.7176513671875",
	      "lattitude": "36.52288052805137"
	    }
	  ]
	}
]
```
32.**List appointment by user**


HTTP Verb :
GET

URL :
```json
list_appointment_by_user/1
```  

data :
```json
  {
  }
```  
  
  
Return :
```json
HTTP/1.1 200 OK
[
   {
  "success": 1,
  "data": [
	    {
	      "appointmentId": 58,
	      "statusAppointment": "معلق",
	      "date": "2017-05-30",
	      "time": "09:30",
	      "doctor": 11,
	      "user": 1
	    },
	    {
	      "appointmentId": 56,
	      "statusAppointment": "مرفوض",
	      "date": "2015-04-15",
	      "time": "05:03",
	      "doctor": 7,
	      "user": 1
	    },
	    {
	      "appointmentId": 55,
	      "statusAppointment": "منته",
	      "date": "2015-04-15",
	      "time": "05:03",
	      "doctor": 1,
	      "user": 1
	    },
	    {
	      "appointmentId": 54,
	      "statusAppointment": "مؤكد",
	      "date": "2017-05-03",
	      "time": "08:41",
	      "doctor": 1,
	      "user": 1
	    },
	    {
	      "appointmentId": 53,
	      "statusAppointment": "معلق",
	      "date": "2017-05-03",
	      "time": "08:41",
	      "doctor": 1,
	      "user": 1
	    },
	    {
	      "appointmentId": 51,
	      "statusAppointment": "معلق",
	      "date": "2017-05-03",
	      "time": "08:41",
	      "doctor": 1,
	      "user": 1
	    },
	    {
	      "appointmentId": 50,
	      "statusAppointment": "مؤكد",
	      "date": "2015-04-15",
	      "time": "05:03",
	      "doctor": 1,
	      "user": 1
	    },
	    {
	      "appointmentId": 49,
	      "statusAppointment": "مرفوض",
	      "date": "2015-04-15",
	      "time": "05:03",
	      "doctor": 1,
	      "user": 1
	    },
	    {
	      "appointmentId": 48,
	      "statusAppointment": "معلق",
	      "date": "2017-05-03",
	      "time": "08:41",
	      "doctor": 1,
	      "user": 1
	    },
	    {
	      "appointmentId": 47,
	      "statusAppointment": "معلق",
	      "date": "2017-05-03",
	      "time": "08:41",
	      "doctor": 1,
	      "user": 1
	    },
	    {
	      "appointmentId": 46,
	      "statusAppointment": "معلق",
	      "date": "2017-05-03",
	      "time": "08:41",
	      "doctor": 1,
	      "user": 1
	    },
	    {
	      "appointmentId": 45,
	      "statusAppointment": "معلق",
	      "date": "2017-05-03",
	      "time": "08:41",
	      "doctor": 1,
	      "user": 1
	    },
	    {
	      "appointmentId": 44,
	      "statusAppointment": "معلق",
	      "date": "2017-05-03",
	      "time": "08:41",
	      "doctor": 1,
	      "user": 1
	    },
	    {
	      "appointmentId": 43,
	      "statusAppointment": "معلق",
	      "date": "2017-05-03",
	      "time": "08:41",
	      "doctor": 1,
	      "user": 1
	    },
	    {
	      "appointmentId": 42,
	      "statusAppointment": "معلق",
	      "date": "2017-05-03",
	      "time": "08:41",
	      "doctor": 1,
	      "user": 1
	    },
	    {
	      "appointmentId": 41,
	      "statusAppointment": "معلق",
	      "date": "2017-05-03",
	      "time": "08:41",
	      "doctor": 1,
	      "user": 1
	    },
	    {
	      "appointmentId": 40,
	      "statusAppointment": "معلق",
	      "date": "2017-05-03",
	      "time": "08:41",
	      "doctor": 11,
	      "user": 1
	    },
	    {
	      "appointmentId": 39,
	      "statusAppointment": "معلق",
	      "date": "2017-05-03",
	      "time": "08:41",
	      "doctor": 1,
	      "user": 1
	    },
	    {
	      "appointmentId": 38,
	      "statusAppointment": "معلق",
	      "date": "2017-05-03",
	      "time": "08:41",
	      "doctor": 1,
	      "user": 1
	    },
	    {
	      "appointmentId": 37,
	      "statusAppointment": "معلق",
	      "date": "2017-05-03",
	      "time": "08:41",
	      "doctor": 1,
	      "user": 1
	    },
	    {
	      "appointmentId": 36,
	      "statusAppointment": "معلق",
	      "date": "2017-05-03",
	      "time": "08:41",
	      "doctor": 1,
	      "user": 1
	    },
	    {
	      "appointmentId": 35,
	      "statusAppointment": "معلق",
	      "date": "2017-05-03",
	      "time": "08:41",
	      "doctor": 1,
	      "user": 1
	    },
	    {
	      "appointmentId": 34,
	      "statusAppointment": "معلق",
	      "date": "2017-05-03",
	      "time": "08:41",
	      "doctor": 1,
	      "user": 1
	    },
	    {
	      "appointmentId": 33,
	      "statusAppointment": "معلق",
	      "date": "2017-05-03",
	      "time": "08:41",
	      "doctor": 1,
	      "user": 1
	    },
	    {
	      "appointmentId": 32,
	      "statusAppointment": "معلق",
	      "date": "2017-05-03",
	      "time": "08:41",
	      "doctor": 1,
	      "user": 1
	    },
	    {
	      "appointmentId": 31,
	      "statusAppointment": "معلق",
	      "date": "2017-05-03",
	      "time": "08:41",
	      "doctor": 1,
	      "user": 1
	    },
	    {
	      "appointmentId": 30,
	      "statusAppointment": "معلق",
	      "date": "2017-05-03",
	      "time": "08:41",
	      "doctor": 1,
	      "user": 1
	    },
	    {
	      "appointmentId": 29,
	      "statusAppointment": "معلق",
	      "date": "2017-05-03",
	      "time": "08:41",
	      "doctor": 1,
	      "user": 1
	    },
	    {
	      "appointmentId": 28,
	      "statusAppointment": "معلق",
	      "date": "2017-05-03",
	      "time": "08:41",
	      "doctor": 1,
	      "user": 1
	    },
	    {
	      "appointmentId": 27,
	      "statusAppointment": "معلق",
	      "date": "2017-05-03",
	      "time": "08:41",
	      "doctor": 1,
	      "user": 1
	    },
	    {
	      "appointmentId": 26,
	      "statusAppointment": "معلق",
	      "date": "2017-05-03",
	      "time": "08:41",
	      "doctor": 1,
	      "user": 1
	    },
	    {
	      "appointmentId": 25,
	      "statusAppointment": "معلق",
	      "date": "2017-05-03",
	      "time": "08:41",
	      "doctor": 1,
	      "user": 1
	    },
	    {
	      "appointmentId": 24,
	      "statusAppointment": "معلق",
	      "date": "2017-05-03",
	      "time": "08:41",
	      "doctor": 1,
	      "user": 1
	    },
	    {
	      "appointmentId": 23,
	      "statusAppointment": "معلق",
	      "date": "2017-05-03",
	      "time": "08:41",
	      "doctor": 1,
	      "user": 1
	    },
	    {
	      "appointmentId": 22,
	      "statusAppointment": "معلق",
	      "date": "2017-05-03",
	      "time": "08:41",
	      "doctor": 1,
	      "user": 1
	    },
	    {
	      "appointmentId": 21,
	      "statusAppointment": "معلق",
	      "date": "2017-05-03",
	      "time": "08:41",
	      "doctor": 1,
	      "user": 1
	    },
	    {
	      "appointmentId": 20,
	      "statusAppointment": "معلق",
	      "date": "2017-05-03",
	      "time": "08:41",
	      "doctor": 1,
	      "user": 1
	    },
	    {
	      "appointmentId": 19,
	      "statusAppointment": "معلق",
	      "date": "2017-05-03",
	      "time": "08:41",
	      "doctor": 1,
	      "user": 1
	    },
	    {
	      "appointmentId": 18,
	      "statusAppointment": "معلق",
	      "date": "2017-05-03",
	      "time": "08:41",
	      "doctor": 1,
	      "user": 1
	    },
	    {
	      "appointmentId": 17,
	      "statusAppointment": "معلق",
	      "date": "2017-05-03",
	      "time": "08:41",
	      "doctor": 1,
	      "user": 1
	    },
	    {
	      "appointmentId": 16,
	      "statusAppointment": "معلق",
	      "date": "2017-05-03",
	      "time": "08:41",
	      "doctor": 1,
	      "user": 1
	    },
	    {
	      "appointmentId": 9,
	      "statusAppointment": "معلق",
	      "date": "2012-01-01",
	      "time": "00:00",
	      "doctor": 5,
	      "user": 1
	    },
	    {
	      "appointmentId": 7,
	      "statusAppointment": "منته",
	      "date": "2012-01-01",
	      "time": "00:00",
	      "doctor": 5,
	      "user": 1
	    }
	  ]
	}
]
```
33.**Update photo doctor**


HTTP Verb :
POST

URL :
```json
doctor_update_picture_api/2
```  

data :
```json
  {
    "photo": "R0lGODlhPQBEAPeoAJosM//AwO/AwHVYZ/z595kzAP/s7P+goOXMv8+fhw/v739/f+8PD98fH/8mJl+fn/9ZWb8/PzWlwv///6wWGbImAPgTEMImIN9gUFCEm/gDALULDN8PAD6atYdCTX9gUNKlj8wZAKUsAOzZz+UMAOsJAP/Z2ccMDA8PD/95eX5NWvsJCOVNQPtfX/8zM8+QePLl38MGBr8JCP+zs9myn/8GBqwpAP/GxgwJCPny78lzYLgjAJ8vAP9fX/+MjMUcAN8zM/9wcM8ZGcATEL+QePdZWf/29uc/P9cmJu9MTDImIN+/r7+/vz8/P8VNQGNugV8AAF9fX8swMNgTAFlDOICAgPNSUnNWSMQ5MBAQEJE3QPIGAM9AQMqGcG9vb6MhJsEdGM8vLx8fH98AANIWAMuQeL8fABkTEPPQ0OM5OSYdGFl5jo+Pj/+pqcsTE78wMFNGQLYmID4dGPvd3UBAQJmTkP+8vH9QUK+vr8ZWSHpzcJMmILdwcLOGcHRQUHxwcK9PT9DQ0O/v70w5MLypoG8wKOuwsP/g4P/Q0IcwKEswKMl8aJ9fX2xjdOtGRs/Pz+Dg4GImIP8gIH0sKEAwKKmTiKZ8aB/f39Wsl+LFt8dgUE9PT5x5aHBwcP+AgP+WltdgYMyZfyywz78AAAAAAAD///8AAP9mZv///wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH5BAEAAKgALAAAAAA9AEQAAAj/AFEJHEiwoMGDCBMqXMiwocAbBww4nEhxoYkUpzJGrMixogkfGUNqlNixJEIDB0SqHGmyJSojM1bKZOmyop0gM3Oe2liTISKMOoPy7GnwY9CjIYcSRYm0aVKSLmE6nfq05QycVLPuhDrxBlCtYJUqNAq2bNWEBj6ZXRuyxZyDRtqwnXvkhACDV+euTeJm1Ki7A73qNWtFiF+/gA95Gly2CJLDhwEHMOUAAuOpLYDEgBxZ4GRTlC1fDnpkM+fOqD6DDj1aZpITp0dtGCDhr+fVuCu3zlg49ijaokTZTo27uG7Gjn2P+hI8+PDPERoUB318bWbfAJ5sUNFcuGRTYUqV/3ogfXp1rWlMc6awJjiAAd2fm4ogXjz56aypOoIde4OE5u/F9x199dlXnnGiHZWEYbGpsAEA3QXYnHwEFliKAgswgJ8LPeiUXGwedCAKABACCN+EA1pYIIYaFlcDhytd51sGAJbo3onOpajiihlO92KHGaUXGwWjUBChjSPiWJuOO/LYIm4v1tXfE6J4gCSJEZ7YgRYUNrkji9P55sF/ogxw5ZkSqIDaZBV6aSGYq/lGZplndkckZ98xoICbTcIJGQAZcNmdmUc210hs35nCyJ58fgmIKX5RQGOZowxaZwYA+JaoKQwswGijBV4C6SiTUmpphMspJx9unX4KaimjDv9aaXOEBteBqmuuxgEHoLX6Kqx+yXqqBANsgCtit4FWQAEkrNbpq7HSOmtwag5w57GrmlJBASEU18ADjUYb3ADTinIttsgSB1oJFfA63bduimuqKB1keqwUhoCSK374wbujvOSu4QG6UvxBRydcpKsav++Ca6G8A6Pr1x2kVMyHwsVxUALDq/krnrhPSOzXG1lUTIoffqGR7Goi2MAxbv6O2kEG56I7CSlRsEFKFVyovDJoIRTg7sugNRDGqCJzJgcKE0ywc0ELm6KBCCJo8DIPFeCWNGcyqNFE06ToAfV0HBRgxsvLThHn1oddQMrXj5DyAQgjEHSAJMWZwS3HPxT/QMbabI/iBCliMLEJKX2EEkomBAUCxRi42VDADxyTYDVogV+wSChqmKxEKCDAYFDFj4OmwbY7bDGdBhtrnTQYOigeChUmc1K3QTnAUfEgGFgAWt88hKA6aCRIXhxnQ1yg3BCayK44EWdkUQcBByEQChFXfCB776aQsG0BIlQgQgE8qO26X1h8cEUep8ngRBnOy74E9QgRgEAC8SvOfQkh7FDBDmS43PmGoIiKUUEGkMEC/PJHgxw0xH74yx/3XnaYRJgMB8obxQW6kL9QYEJ0FIFgByfIL7/IQAlvQwEpnAC7DtLNJCKUoO/w45c44GwCXiAFB/OXAATQryUxdN4LfFiwgjCNYg+kYMIEFkCKDs6PKAIJouyGWMS1FSKJOMRB/BoIxYJIUXFUxNwoIkEKPAgCBZSQHQ1A2EWDfDEUVLyADj5AChSIQW6gu10bE/JG2VnCZGfo4R4d0sdQoBAHhPjhIB94v/wRoRKQWGRHgrhGSQJxCS+0pCZbEhAAOw=="
  }
```  
  
  
Return :
```json
HTTP/1.1 200 OK
[
	{
	  "success": 1,
	  "name": "5936aed2938d9.png"
	}
]
```
34.**Update photo user**


HTTP Verb :
POST

URL :
```json
user_update_picture_api/3
```  

data :
```json
  {
    "photo": "R0lGODlhPQBEAPeoAJosM//AwO/AwHVYZ/z595kzAP/s7P+goOXMv8+fhw/v739/f+8PD98fH/8mJl+fn/9ZWb8/PzWlwv///6wWGbImAPgTEMImIN9gUFCEm/gDALULDN8PAD6atYdCTX9gUNKlj8wZAKUsAOzZz+UMAOsJAP/Z2ccMDA8PD/95eX5NWvsJCOVNQPtfX/8zM8+QePLl38MGBr8JCP+zs9myn/8GBqwpAP/GxgwJCPny78lzYLgjAJ8vAP9fX/+MjMUcAN8zM/9wcM8ZGcATEL+QePdZWf/29uc/P9cmJu9MTDImIN+/r7+/vz8/P8VNQGNugV8AAF9fX8swMNgTAFlDOICAgPNSUnNWSMQ5MBAQEJE3QPIGAM9AQMqGcG9vb6MhJsEdGM8vLx8fH98AANIWAMuQeL8fABkTEPPQ0OM5OSYdGFl5jo+Pj/+pqcsTE78wMFNGQLYmID4dGPvd3UBAQJmTkP+8vH9QUK+vr8ZWSHpzcJMmILdwcLOGcHRQUHxwcK9PT9DQ0O/v70w5MLypoG8wKOuwsP/g4P/Q0IcwKEswKMl8aJ9fX2xjdOtGRs/Pz+Dg4GImIP8gIH0sKEAwKKmTiKZ8aB/f39Wsl+LFt8dgUE9PT5x5aHBwcP+AgP+WltdgYMyZfyywz78AAAAAAAD///8AAP9mZv///wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH5BAEAAKgALAAAAAA9AEQAAAj/AFEJHEiwoMGDCBMqXMiwocAbBww4nEhxoYkUpzJGrMixogkfGUNqlNixJEIDB0SqHGmyJSojM1bKZOmyop0gM3Oe2liTISKMOoPy7GnwY9CjIYcSRYm0aVKSLmE6nfq05QycVLPuhDrxBlCtYJUqNAq2bNWEBj6ZXRuyxZyDRtqwnXvkhACDV+euTeJm1Ki7A73qNWtFiF+/gA95Gly2CJLDhwEHMOUAAuOpLYDEgBxZ4GRTlC1fDnpkM+fOqD6DDj1aZpITp0dtGCDhr+fVuCu3zlg49ijaokTZTo27uG7Gjn2P+hI8+PDPERoUB318bWbfAJ5sUNFcuGRTYUqV/3ogfXp1rWlMc6awJjiAAd2fm4ogXjz56aypOoIde4OE5u/F9x199dlXnnGiHZWEYbGpsAEA3QXYnHwEFliKAgswgJ8LPeiUXGwedCAKABACCN+EA1pYIIYaFlcDhytd51sGAJbo3onOpajiihlO92KHGaUXGwWjUBChjSPiWJuOO/LYIm4v1tXfE6J4gCSJEZ7YgRYUNrkji9P55sF/ogxw5ZkSqIDaZBV6aSGYq/lGZplndkckZ98xoICbTcIJGQAZcNmdmUc210hs35nCyJ58fgmIKX5RQGOZowxaZwYA+JaoKQwswGijBV4C6SiTUmpphMspJx9unX4KaimjDv9aaXOEBteBqmuuxgEHoLX6Kqx+yXqqBANsgCtit4FWQAEkrNbpq7HSOmtwag5w57GrmlJBASEU18ADjUYb3ADTinIttsgSB1oJFfA63bduimuqKB1keqwUhoCSK374wbujvOSu4QG6UvxBRydcpKsav++Ca6G8A6Pr1x2kVMyHwsVxUALDq/krnrhPSOzXG1lUTIoffqGR7Goi2MAxbv6O2kEG56I7CSlRsEFKFVyovDJoIRTg7sugNRDGqCJzJgcKE0ywc0ELm6KBCCJo8DIPFeCWNGcyqNFE06ToAfV0HBRgxsvLThHn1oddQMrXj5DyAQgjEHSAJMWZwS3HPxT/QMbabI/iBCliMLEJKX2EEkomBAUCxRi42VDADxyTYDVogV+wSChqmKxEKCDAYFDFj4OmwbY7bDGdBhtrnTQYOigeChUmc1K3QTnAUfEgGFgAWt88hKA6aCRIXhxnQ1yg3BCayK44EWdkUQcBByEQChFXfCB776aQsG0BIlQgQgE8qO26X1h8cEUep8ngRBnOy74E9QgRgEAC8SvOfQkh7FDBDmS43PmGoIiKUUEGkMEC/PJHgxw0xH74yx/3XnaYRJgMB8obxQW6kL9QYEJ0FIFgByfIL7/IQAlvQwEpnAC7DtLNJCKUoO/w45c44GwCXiAFB/OXAATQryUxdN4LfFiwgjCNYg+kYMIEFkCKDs6PKAIJouyGWMS1FSKJOMRB/BoIxYJIUXFUxNwoIkEKPAgCBZSQHQ1A2EWDfDEUVLyADj5AChSIQW6gu10bE/JG2VnCZGfo4R4d0sdQoBAHhPjhIB94v/wRoRKQWGRHgrhGSQJxCS+0pCZbEhAAOw=="
  }
```  
  
  
Return :
```json
HTTP/1.1 200 OK
[
	{
	  "success": 1,
	  "name": "5936afb55a1b4.png"
	}
]
```
35.**add photo user**


HTTP Verb :
POST

URL :
```json
user_add_picture_api/3
```  

data :
```json
  {
    "photo": "R0lGODlhPQBEAPeoAJosM//AwO/AwHVYZ/z595kzAP/s7P+goOXMv8+fhw/v739/f+8PD98fH/8mJl+fn/9ZWb8/PzWlwv///6wWGbImAPgTEMImIN9gUFCEm/gDALULDN8PAD6atYdCTX9gUNKlj8wZAKUsAOzZz+UMAOsJAP/Z2ccMDA8PD/95eX5NWvsJCOVNQPtfX/8zM8+QePLl38MGBr8JCP+zs9myn/8GBqwpAP/GxgwJCPny78lzYLgjAJ8vAP9fX/+MjMUcAN8zM/9wcM8ZGcATEL+QePdZWf/29uc/P9cmJu9MTDImIN+/r7+/vz8/P8VNQGNugV8AAF9fX8swMNgTAFlDOICAgPNSUnNWSMQ5MBAQEJE3QPIGAM9AQMqGcG9vb6MhJsEdGM8vLx8fH98AANIWAMuQeL8fABkTEPPQ0OM5OSYdGFl5jo+Pj/+pqcsTE78wMFNGQLYmID4dGPvd3UBAQJmTkP+8vH9QUK+vr8ZWSHpzcJMmILdwcLOGcHRQUHxwcK9PT9DQ0O/v70w5MLypoG8wKOuwsP/g4P/Q0IcwKEswKMl8aJ9fX2xjdOtGRs/Pz+Dg4GImIP8gIH0sKEAwKKmTiKZ8aB/f39Wsl+LFt8dgUE9PT5x5aHBwcP+AgP+WltdgYMyZfyywz78AAAAAAAD///8AAP9mZv///wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH5BAEAAKgALAAAAAA9AEQAAAj/AFEJHEiwoMGDCBMqXMiwocAbBww4nEhxoYkUpzJGrMixogkfGUNqlNixJEIDB0SqHGmyJSojM1bKZOmyop0gM3Oe2liTISKMOoPy7GnwY9CjIYcSRYm0aVKSLmE6nfq05QycVLPuhDrxBlCtYJUqNAq2bNWEBj6ZXRuyxZyDRtqwnXvkhACDV+euTeJm1Ki7A73qNWtFiF+/gA95Gly2CJLDhwEHMOUAAuOpLYDEgBxZ4GRTlC1fDnpkM+fOqD6DDj1aZpITp0dtGCDhr+fVuCu3zlg49ijaokTZTo27uG7Gjn2P+hI8+PDPERoUB318bWbfAJ5sUNFcuGRTYUqV/3ogfXp1rWlMc6awJjiAAd2fm4ogXjz56aypOoIde4OE5u/F9x199dlXnnGiHZWEYbGpsAEA3QXYnHwEFliKAgswgJ8LPeiUXGwedCAKABACCN+EA1pYIIYaFlcDhytd51sGAJbo3onOpajiihlO92KHGaUXGwWjUBChjSPiWJuOO/LYIm4v1tXfE6J4gCSJEZ7YgRYUNrkji9P55sF/ogxw5ZkSqIDaZBV6aSGYq/lGZplndkckZ98xoICbTcIJGQAZcNmdmUc210hs35nCyJ58fgmIKX5RQGOZowxaZwYA+JaoKQwswGijBV4C6SiTUmpphMspJx9unX4KaimjDv9aaXOEBteBqmuuxgEHoLX6Kqx+yXqqBANsgCtit4FWQAEkrNbpq7HSOmtwag5w57GrmlJBASEU18ADjUYb3ADTinIttsgSB1oJFfA63bduimuqKB1keqwUhoCSK374wbujvOSu4QG6UvxBRydcpKsav++Ca6G8A6Pr1x2kVMyHwsVxUALDq/krnrhPSOzXG1lUTIoffqGR7Goi2MAxbv6O2kEG56I7CSlRsEFKFVyovDJoIRTg7sugNRDGqCJzJgcKE0ywc0ELm6KBCCJo8DIPFeCWNGcyqNFE06ToAfV0HBRgxsvLThHn1oddQMrXj5DyAQgjEHSAJMWZwS3HPxT/QMbabI/iBCliMLEJKX2EEkomBAUCxRi42VDADxyTYDVogV+wSChqmKxEKCDAYFDFj4OmwbY7bDGdBhtrnTQYOigeChUmc1K3QTnAUfEgGFgAWt88hKA6aCRIXhxnQ1yg3BCayK44EWdkUQcBByEQChFXfCB776aQsG0BIlQgQgE8qO26X1h8cEUep8ngRBnOy74E9QgRgEAC8SvOfQkh7FDBDmS43PmGoIiKUUEGkMEC/PJHgxw0xH74yx/3XnaYRJgMB8obxQW6kL9QYEJ0FIFgByfIL7/IQAlvQwEpnAC7DtLNJCKUoO/w45c44GwCXiAFB/OXAATQryUxdN4LfFiwgjCNYg+kYMIEFkCKDs6PKAIJouyGWMS1FSKJOMRB/BoIxYJIUXFUxNwoIkEKPAgCBZSQHQ1A2EWDfDEUVLyADj5AChSIQW6gu10bE/JG2VnCZGfo4R4d0sdQoBAHhPjhIB94v/wRoRKQWGRHgrhGSQJxCS+0pCZbEhAAOw=="
  }
```  
  
  
Return :
```json
HTTP/1.1 200 OK
[
	{
	  "success": 1,
	  "name": "5936b395e339d.png"
	}
]
```

36.**add photo doctor**


HTTP Verb :
POST

URL :
```json
doctor_add_picture_api/2
```  

data :
```json
  {
    "photo": "R0lGODlhPQBEAPeoAJosM//AwO/AwHVYZ/z595kzAP/s7P+goOXMv8+fhw/v739/f+8PD98fH/8mJl+fn/9ZWb8/PzWlwv///6wWGbImAPgTEMImIN9gUFCEm/gDALULDN8PAD6atYdCTX9gUNKlj8wZAKUsAOzZz+UMAOsJAP/Z2ccMDA8PD/95eX5NWvsJCOVNQPtfX/8zM8+QePLl38MGBr8JCP+zs9myn/8GBqwpAP/GxgwJCPny78lzYLgjAJ8vAP9fX/+MjMUcAN8zM/9wcM8ZGcATEL+QePdZWf/29uc/P9cmJu9MTDImIN+/r7+/vz8/P8VNQGNugV8AAF9fX8swMNgTAFlDOICAgPNSUnNWSMQ5MBAQEJE3QPIGAM9AQMqGcG9vb6MhJsEdGM8vLx8fH98AANIWAMuQeL8fABkTEPPQ0OM5OSYdGFl5jo+Pj/+pqcsTE78wMFNGQLYmID4dGPvd3UBAQJmTkP+8vH9QUK+vr8ZWSHpzcJMmILdwcLOGcHRQUHxwcK9PT9DQ0O/v70w5MLypoG8wKOuwsP/g4P/Q0IcwKEswKMl8aJ9fX2xjdOtGRs/Pz+Dg4GImIP8gIH0sKEAwKKmTiKZ8aB/f39Wsl+LFt8dgUE9PT5x5aHBwcP+AgP+WltdgYMyZfyywz78AAAAAAAD///8AAP9mZv///wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH5BAEAAKgALAAAAAA9AEQAAAj/AFEJHEiwoMGDCBMqXMiwocAbBww4nEhxoYkUpzJGrMixogkfGUNqlNixJEIDB0SqHGmyJSojM1bKZOmyop0gM3Oe2liTISKMOoPy7GnwY9CjIYcSRYm0aVKSLmE6nfq05QycVLPuhDrxBlCtYJUqNAq2bNWEBj6ZXRuyxZyDRtqwnXvkhACDV+euTeJm1Ki7A73qNWtFiF+/gA95Gly2CJLDhwEHMOUAAuOpLYDEgBxZ4GRTlC1fDnpkM+fOqD6DDj1aZpITp0dtGCDhr+fVuCu3zlg49ijaokTZTo27uG7Gjn2P+hI8+PDPERoUB318bWbfAJ5sUNFcuGRTYUqV/3ogfXp1rWlMc6awJjiAAd2fm4ogXjz56aypOoIde4OE5u/F9x199dlXnnGiHZWEYbGpsAEA3QXYnHwEFliKAgswgJ8LPeiUXGwedCAKABACCN+EA1pYIIYaFlcDhytd51sGAJbo3onOpajiihlO92KHGaUXGwWjUBChjSPiWJuOO/LYIm4v1tXfE6J4gCSJEZ7YgRYUNrkji9P55sF/ogxw5ZkSqIDaZBV6aSGYq/lGZplndkckZ98xoICbTcIJGQAZcNmdmUc210hs35nCyJ58fgmIKX5RQGOZowxaZwYA+JaoKQwswGijBV4C6SiTUmpphMspJx9unX4KaimjDv9aaXOEBteBqmuuxgEHoLX6Kqx+yXqqBANsgCtit4FWQAEkrNbpq7HSOmtwag5w57GrmlJBASEU18ADjUYb3ADTinIttsgSB1oJFfA63bduimuqKB1keqwUhoCSK374wbujvOSu4QG6UvxBRydcpKsav++Ca6G8A6Pr1x2kVMyHwsVxUALDq/krnrhPSOzXG1lUTIoffqGR7Goi2MAxbv6O2kEG56I7CSlRsEFKFVyovDJoIRTg7sugNRDGqCJzJgcKE0ywc0ELm6KBCCJo8DIPFeCWNGcyqNFE06ToAfV0HBRgxsvLThHn1oddQMrXj5DyAQgjEHSAJMWZwS3HPxT/QMbabI/iBCliMLEJKX2EEkomBAUCxRi42VDADxyTYDVogV+wSChqmKxEKCDAYFDFj4OmwbY7bDGdBhtrnTQYOigeChUmc1K3QTnAUfEgGFgAWt88hKA6aCRIXhxnQ1yg3BCayK44EWdkUQcBByEQChFXfCB776aQsG0BIlQgQgE8qO26X1h8cEUep8ngRBnOy74E9QgRgEAC8SvOfQkh7FDBDmS43PmGoIiKUUEGkMEC/PJHgxw0xH74yx/3XnaYRJgMB8obxQW6kL9QYEJ0FIFgByfIL7/IQAlvQwEpnAC7DtLNJCKUoO/w45c44GwCXiAFB/OXAATQryUxdN4LfFiwgjCNYg+kYMIEFkCKDs6PKAIJouyGWMS1FSKJOMRB/BoIxYJIUXFUxNwoIkEKPAgCBZSQHQ1A2EWDfDEUVLyADj5AChSIQW6gu10bE/JG2VnCZGfo4R4d0sdQoBAHhPjhIB94v/wRoRKQWGRHgrhGSQJxCS+0pCZbEhAAOw=="
  }
```  
  
  
Return :
```json
HTTP/1.1 200 OK
[
	{
	  "success": 1,
	  "name": "5936b46488e34.png"
	}
]
```
37.**Show profile doctor**


HTTP Verb :
GET

URL :
```json
profile_doctor/12
```  

data :
```json
  {
  }
```  
  
  
Return :
```json
HTTP/1.1 200 OK
[
	{
	  "success": 1,
	  "data": {
	    "doctorId": 12,
	    "nameDoctor": "Hamza",
	    "phoneNumberDoctor": "71321456",
	    "countryDoctor": "Tunisia",
	    "townDoctor": "Jendouba",
	    "cityDoctor": "Elferdaous",
	    "photoDoctor": "/uploads/doctors/6eb7d4adc09a8030caea19287b4fe3cb.jpeg",
	    "emailDoctor": "hamza@gmail.com",
	    "completed": 1
	  }
	}
]
```
38.**Get notification**


HTTP Verb :
GET

URL :
```json
get_notification
```  

data :
```json
  {
    "isDoctor": 1,
    "identifiant": 13
  }
```  
  
  
Return :
```json
HTTP/1.1 200 OK
[
    {
        "notificationId": 4,
        "notificationMessage": "sdlkfjsd usdhfusdfh ksdfh ksdhf",
        "notificationType": "appt",
        "notificationDate": "2017-06-22",
        "notificationTime": "07:00",
        "notificationSeen": 0,
        "notificationIsDoctor": 1
    },
    {
        "notificationId": 1,
        "notificationMessage": "قام المستخدمfat7iبحجز موعد جديد",
        "notificationType": "appointment",
        "notificationDate": "2017-06-21",
        "notificationTime": "19:42",
        "notificationSeen": 0,
        "notificationIsDoctor": 1
    }
]
```
39.**update notification**


HTTP Verb :
GET

URL :
```json
update_notification
```  

data :
```json
  {
    "isDoctor": 1,
    "identifiant": 13
  }
```  
  
  
Return :
```json
HTTP/1.1 200 OK

   {
    "success": 1
   }

```
