<?php

namespace Doctors\EndpointBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('DoctorsEndpointBundle:Default:index.html.twig');
    }
}
