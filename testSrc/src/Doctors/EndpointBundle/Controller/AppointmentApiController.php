<?php

namespace Doctors\EndpointBundle\Controller;

use Doctors\AdminBundle\Entity\Appointment;
use Doctors\AdminBundle\Entity\TokenDoctor;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

class AppointmentApiController extends Controller
{
    public function addAppointmentAction(Request $request)
    {
        $key = $request->headers->get('key');
        if ($this->auth_key($key)) {
            $em = $this->getDoctrine()->getManager();
            $doctor_id = $request->request->get('doctor_id');
            $doctor = $em->getRepository('DoctorsAdminBundle:Doctor')->find($doctor_id);
            $dateAppointmt = $request->request->get('date_appointment');
            $dateAppointmtdate = substr($dateAppointmt, 0, 2);
            $dateAppointmtMonth = substr($dateAppointmt, 3, 2);
            $dateAppointmtYear = substr($dateAppointmt, 6, 4);
            $dateAppt = $request->request->get('timeOfAppt');
            $dateApptHour = substr($dateAppt, 0, 2);
            $dateApptMinute = substr($dateAppt, 3, 2);
            $dateApptSeconds = substr($dateAppt, 6, 2);
            $dateFormattedAppointment = $dateAppointmtYear . '-' . $dateAppointmtMonth . '-' . $dateAppointmtdate . ' ' . $dateApptHour . ':' . $dateApptMinute . ':' . $dateApptSeconds . '00';
            /*var_dump($dateFormattedAppointment);*/
            $dateAppointment = new \DateTime($dateFormattedAppointment);
            $appointment = $em->getRepository('DoctorsAdminBundle:Appointment')->findBy(
                array('appointment' => $dateAppointment, 'doctor' => $doctor)
            );
            /*var_dump($appointment);*/
            if (empty($appointment)) {
                $newAppointment = new Appointment();
                $newAppointment->setIsActiveAppointment(1);
                $newAppointment->setCreatedAtAppointment(new \Datetime());
                $newAppointment->setStatus('معلق');
                /*var_dump($doctor_id);*/
                $user_id = $request->request->get('user_id');
                /*var_dump($user_id);*/
                $user = $em->getRepository('DoctorsAdminBundle:User')->find($user_id);
                $newAppointment->setDoctor($doctor);
                $newAppointment->setUser($user);
                $newAppointment->setAppointment($dateAppointment);
                $newAppointment->setEval(0);
                $em->persist($newAppointment);
                $em->flush();
                $tokens = $em->getRepository('DoctorsAdminBundle:TokenDoctor')->findBy(
                    array('doctor' => $doctor)
                );
                /*var_dump($tokens);*/
                if (!empty($tokens)) {
                    $tab = array();
                    foreach ($tokens as $token) {
                        $tab[] = $token->getTokenDoctor();
                    }
                }
                $tab_token = $tab;
                $title = "الأطباء ";
                $text = 'قام المستخدم' . $newAppointment->getUser()->getName() . 'بحجز موعد جديد';
                $this->sendNotificationAppointment($tab_token, $title, $text);
                $result = array(
                    'success' => 1,
                    'id' => $newAppointment->getId()
                );
            } else {
                $result = array(
                    'success' => 0,
                    'exist' => 1
                );
            }
            return new JsonResponse($result);
        } else {
            $tab = array('auth' => 0);
            return new JsonResponse($tab);
        }
    }

    public function loadDataHourWorkAction(Request $request)
    {
        $key = $request->headers->get('key');
        if ($this->auth_key($key)) {
            $dateRDV = $request->request->get('dateAppointment');
            $dateRDV = new \DateTime($dateRDV);
            $em = $this->getDoctrine()->getManager();
            /*var_dump('_____________dateAppointment__________');
            var_dump($dateRDV);*/
            $dateRDVEnd = clone $dateRDV;
            $doctor_id = $request->request->get('doctor_id');
            /*var_dump('______doctor_id_______________');
            var_dump($doctor_id);*/
            $user_id = $request->request->get('user_id');
            $doctor = $em->getRepository('DoctorsAdminBundle:Doctor')->find($doctor_id);
            /*var_dump('________Doctor who have the doctor_id________');*/
            if (!empty($doctor)) {
                $dateRDVDay = $dateRDV->format('d');
                $dateRDVMonth = $dateRDV->format('m');
                $dateRDVYear = $dateRDV->format('Y');
                $dateRDVFormatted = $dateRDVYear . '-' . $dateRDVMonth . '-' . $dateRDVDay;
                $appointments = $em->getRepository('DoctorsAdminBundle:Appointment')->findCorrespendingAppointment($dateRDVFormatted, $doctor->getId());
                $em = $this->getDoctrine()->getManager();
                $doctor = $em->getRepository('DoctorsAdminBundle:Doctor')->find($doctor_id);
                $startTimeWork = $doctor->getStartTimeWork();
                $endTimeWork = $doctor->getEndTimeWork();
                $startWorkTimeHours = $startTimeWork->format('H');
                $startWorkTimeMinutes = $startTimeWork->format('i');
                $startWorkTimeSeconds = $startTimeWork->format('s');
                /* add hours and minutes for the selected day */
                $dateRDV->add(new \DateInterval('PT' . $startWorkTimeHours . 'H'));
                $dateRDV->add(new \DateInterval('PT' . $startWorkTimeMinutes . 'M'));
                $interval = '30';
                $timesWorkday = array();
                $timesWorkday[] = $dateRDV;
                $dateStarted = clone $dateRDV;
                $endTimeWorkHours = $endTimeWork->format('H');
                $endTimeWorkMinutes = $endTimeWork->format('i');
                $endTimeWorkSeconds = $endTimeWork->format('s');
                $dateRDVEnd->add(new \DateInterval('PT' . $endTimeWorkHours . 'H'));
                $dateRDVEnd->add(new \DateInterval('PT' . $endTimeWorkMinutes . 'M'));
                $dateEnded = clone $dateRDVEnd;
                $test = true;
                do {
                    $dateStarted = $this->addMinutes($dateStarted, $interval, $timesWorkday);
                    $timesWorkday[] = $dateStarted;
                    $dateStartedHour = $dateStarted->format('H');
                    $dateStartedMinute = $dateStarted->format('i');
                    $dateEndedHour = $dateEnded->format('H');
                    $dateEndedMinute = $dateEnded->format('i');
                    if ($dateEndedHour == $dateStartedHour) {
                        $test = false;
                    }
                    $dateStarted = clone $dateStarted;
                } while ($test);
                $appnmt = array();
                foreach ($appointments as $appointment) {
                    foreach ($appointment as $apt) {
                        $appnmt[] = $apt;
                    }
                }
                foreach ($timesWorkday as $time) {
                    foreach ($appnmt as $appt) {
                        if ($time == $appt) {
                            $time->disponible = 0;
                            $time = clone $time;
                        } else {
                            $time->disponible = 1;
                        }
                    }
                }
                $tab = array();
                foreach ($timesWorkday as $key => $value) {
                    /*var_dump($value);*/
                    $tab[$key]["date"] = $value->format('Y-m-d');
                    $tab[$key]["time"] = $value->format('H:i');
                    $tab[$key]["disponible"] = $value->disponible;
                }
                $result = array();
                $result['success'] = 1;
                $result['data'] = $tab;
                return new JsonResponse($result);
            } else {
                $result = array(
                    'success' => 0
                );
                return new JsonResponse($result);
            }
        } else {
            $tab = array('auth' => 0);
            return new JsonResponse($tab);
        }
    }

    public function addMinutes(\DateTime $startTimeWorked, $interval, array $timesWorkday)
    {
        $startTimeWorked->add(new \DateInterval('PT' . $interval . 'M'));
        return $startTimeWorked;
    }

    public function api_appointment_list_confirmedAction(Request $request)
    {
        $key = $request->headers->get('key');
        if ($this->auth_key($key)) {
            $em = $this->getDoctrine()->getManager();
            $status = $request->request->get('status');
            $appointments = $em->getRepository('DoctorsAdminBundle:Appointment')->findBy(
                array(
                    'status' => $status
                ),
                array('id' => 'desc')
            );
            $i = 0;
            $appointmentArray = array();
            foreach ($appointments as $appointment) {
                $appointmentArray[$i]["appointmentId"] = $appointment->getId();
                $appointmentArray[$i]["statusAppointment"] = $appointment->getStatus();
                $appointmentArray[$i]["appointment"] = $appointment->getAppointment();
                $appointmentArray[$i]['doctor'] = $appointment->getDoctor()->getId();
                $appointmentArray[$i]['user'] = $appointment->getUser()->getId();
                $i++;
            }
            return new JsonResponse($appointmentArray);
        } else {
            $tab = array('auth' => 0);
            return new JsonResponse($tab);
        }
    }

    public function api_delete_appointmentAction(Request $request, $appointment_id)
    {
        $em = $this->getDoctrine()->getManager();
        $key = $request->headers->get('key');
        if ($this->auth_key($key)) {
            $appointment = $em->getRepository('DoctorsAdminBundle:Appointment')->find($appointment_id);
            if (!empty($appointment)) {
                $appointment->setIsActiveAppointment(0);
                $em->merge($appointment);
                $em->flush();
                $result = array(
                    'success' => 1,
                    'id' => $appointment_id
                );
            } else {
                $result = array(
                    'exist' => 0
                );
            }
            return new JsonResponse($result);
        } else {
            $tab = array('auth' => 0);
            return new JsonResponse($tab);
        }
    }

    public function api_get_appointment_by_doctorAction(Request $request, $doctor_id)
    {
        $em = $this->getDoctrine()->getManager();
        $key = $request->headers->get('key');
        if ($this->auth_key($key)) {
            $doctor = $em->getRepository('DoctorsAdminBundle:Doctor')->find($doctor_id);
            $appointments = $em->getRepository('DoctorsAdminBundle:Appointment')->findBy(
                array('doctor' => $doctor, 'isActiveAppointment' => 1),
                array('id' => 'desc')
            );
            $i = 0;
            $appointmentArray = array();
            $tab_return = array();
            if (empty($appointments)) {
                $tab_return['success'] = 0;
                $tab_return['data'] = array();
            } else {
                foreach ($appointments as $appointment) {
                    $appointmentArray[$i]["appointmentId"] = $appointment->getId();
                    $appointmentArray[$i]["statusAppointment"] = $appointment->getStatus();
                    $appointmentArray[$i]["appointment"] = $appointment->getAppointment();
                    $appointmentArray[$i]["date"] = $appointment->getAppointment()->format('Y-m-d');
                    $appointmentArray[$i]["time"] = $appointment->getAppointment()->format('H:i');
                    $appointmentArray[$i]['doctor'] = $appointment->getDoctor()->getId();
                    $appointmentArray[$i]['namedoctor'] = $appointment->getDoctor()->getNameDoctor();
                    $appointmentArray[$i]['user'] = $appointment->getUser()->getId();
                    $appointmentArray[$i]['userName'] = $appointment->getUser()->getName();
                    $i++;
                }
                $tab_return['success'] = 1;
                $tab_return['data'] = $appointmentArray;
            }

            return new JsonResponse($tab_return);
        } else {
            $tab = array('auth' => 0);
            return new JsonResponse($tab);
        }
    }

    public function api_get_appointment_by_userAction(Request $request, $user_id)
    {
        $em = $this->getDoctrine()->getManager();
        $key = $request->headers->get('key');
        if ($this->auth_key($key)) {
            $user = $em->getRepository('DoctorsAdminBundle:User')->find($user_id);
            $appointments = $em->getRepository('DoctorsAdminBundle:Appointment')->findBy(
                array('user' => $user, 'isActiveAppointment' => 1),
                array('id' => 'desc')
            );
            $i = 0;
            $appointmentArray = array();
            $tab_return = array();
            if (empty($appointments)) {
                $tab_return['success'] = 0;
                $tab_return['data'] = array();
            } else {
                foreach ($appointments as $appointment) {
                    $appointmentArray[$i]["appointmentId"] = $appointment->getId();
                    $appointmentArray[$i]["statusAppointment"] = $appointment->getStatus();
                    $appointmentArray[$i]["date"] = $appointment->getAppointment()->format('Y-m-d');
                    $appointmentArray[$i]["time"] = $appointment->getAppointment()->format('H:i');
                    $appointmentArray[$i]['doctor'] = $appointment->getDoctor()->getId();
                    $appointmentArray[$i]['doctor'] = $appointment->getDoctor()->getNameDoctor();
                    $appointmentArray[$i]['user'] = $appointment->getUser()->getId();
                    $appointmentArray[$i]['userName'] = $appointment->getUser()->getName();
                    $appointmentArray[$i]['reason'] = $appointment->getReason();
                    if ($appointment->getEval() == false) {
                        $appointmentArray[$i]['eval'] = 0;
                        $appointmentArray[$i]['id'] = null;
                        $appointmentArray[$i]['evaluation'] = null;
                        $appointmentArray[$i]['feedback'] = null;
                    } else {
                        $evaluations = $em->getRepository('DoctorsAdminBundle:Evaluation')->findAll();
                        foreach ($evaluations as $evaluationVal) {
                            if ($evaluationVal->getAppointment() == $appointment) {
                                if ($evaluationVal->getStatusEvaluation() == false) {
                                    $appointmentArray[$i]['eval'] = 1;
                                    $appointmentArray[$i]['id'] = $evaluationVal->getId();
                                    $appointmentArray[$i]['evaluation'] = $evaluationVal->getEvaluation();
                                    $appointmentArray[$i]['feedback'] = $evaluationVal->getFeedback();
                                } else {
                                    $appointmentArray[$i]['eval'] = 2;
                                    $appointmentArray[$i]['id'] = $evaluationVal->getId();
                                    $appointmentArray[$i]['evaluation'] = $evaluationVal->getEvaluation();
                                    $appointmentArray[$i]['feedback'] = $evaluationVal->getFeedback();
                                }
                            }
                        }
                    }

                    if ($appointmentArray[$i]['eval'] == true) {
                        /*$appointmentArray[$i][]*/
                    }
                    /*var_dump($appointmentArray[$i]['eval']);*/
                    $i++;
                }
                $tab_return['success'] = 1;
                $tab_return['data'] = $appointmentArray;
            }
            return new JsonResponse($tab_return);
        } else {
            $tab = array('auth' => 0);
            return new JsonResponse($tab);
        }
    }

    public function api_update_status_appointmentAction(Request $request, $appointment_id)
    {
        $em = $this->getDoctrine()->getManager();
        $key = $request->headers->get('key');
        if ($this->auth_key($key)) {
            $status = $request->request->get('status');
            if (isset($status)) {
                $appointment = $em->getRepository('DoctorsAdminBundle:Appointment')->find($appointment_id);
               // var_dump($appointment);
                if (!empty($appointment)) {
                    if ($status == "مرفوض") {
                        $reason = $request->request->get('reason');
                       // var_dump($reason);
                        $appointment->setReason($reason);
                    }
                    $appointment->setStatus($status);
                    $em->merge($appointment);
                    $em->flush();
                    $user = $appointment->getUser();
                    $tokens = $em->getRepository('DoctorsAdminBundle:TokenUser')->findBy(
                        array('user' => $user)
                    );
                    if (!empty($tokens)) {
                        $tab = array();
                        foreach ($tokens as $token) {
                            $tab[] = $token->getToken();
                        }
                    }
                    $tab_token = $tab;
                    $title = "الأطباء ";
                    if ($appointment->getStatus() == "مؤكد") {
                        $text = 'تمت الموافقة على الموعد';
                    }
                    if ($appointment->getStatus() == "مرفوض") {
                        $text = 'تم رفض الموعد مع الطبيب';
                    }
                    $this->sendNotificationAppointment($tab_token, $title, $text);
                    $result = array(
                        'success' => 1,
                        'id' => $appointment_id
                    );
                } else {
                    $result = array(
                        'exist' => 0
                    );
                }
                return new JsonResponse($result);
            }

        } else {
            $tab = array('auth' => 0);
            return new JsonResponse($tab);
        }
    }


    public function sendNotificationAppointment($token, $title, $text)
    {
        $url = 'https://fcm.googleapis.com/fcm/send';
        $path_to_firebase_cm = 'https://fcm.googleapis.com/fcm/send';
        $fields = array(
            'to' => $token,
            'notification' => array('title' => $title, 'body' => $text),
            'data' => array('message' => 'adiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiib ya m3alemmmmmmmm')
        );
        $headers = array(
            'Authorization: key=AAAA_hUBCYE:APA91bHsJRk0F6sty7Vz1L6HyUoWqHp1wxKLNKTn4N2BT3sEoONGI5I1HTwl3HlswJ-_1JW-OnArf__NaSBikXBnIa-XMeTsKl8m2hcuU2S5SvrlyRzNOJ23bvUO_aIdhMts1ocg9LjY',
            'Content-Type: application/json'
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $path_to_firebase_cm);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        curl_close($ch);
        return true;
    }

    public function api_send_notification_corn_jobsAction(Request $request)
    {
        $key = $request->headers->get('key');
        if ($this->auth_key($key)) {
            $em = $this->getDoctrine()->getManager();
            $nowDate = date('Y-m-d');
            $appointments = $em->getRepository('DoctorsAdminBundle:Appointment')->findAppointmentsDate($nowDate);
            //var_dump($appointments);
            if (!empty($appointments)) {
                $appt = array();
                $i = 0;
                foreach ($appointments as $appointment) {

                    $minutes = 60;
                    $appt[$i]['appointment'] = $appointment['appointment']->add(new \DateInterval('PT' . $minutes . 'M'));
                    $appt[$i]['user_id'] = $appointment[1];
                    $i++;
                }
                foreach ($appt as $app) {
                    $test = false;
                    foreach ($app as $appointmentPlusHour) {
                        $dattte = new \DateTime();
                        if ($appointmentPlusHour == $dattte) {
                            $user = $em->getRepository('DoctorsAdminBundle:User')->find($app['user_id']);
                            $tokens = $em->getRepository('DoctorsAdminBundle:TokenUser')->findBy(
                                array('user' => $user)
                            );
                            if (!empty($tokens)) {
                                $tab = array();
                                foreach ($tokens as $token) {
                                    $tab[] = $token->getTokenUser();
                                }
                            }
                            $tab_token = $tab;
                            $title = "الأطباء ";
                            $text = 'يمكنك تقييم موعدك';
                            $this->sendNotificationAppointment($tab_token, $title, $text);
                        }
                        if ($test) {

                        }
                    }
                }
            }
            return true;
        }
        return false;
    }

    public function auth_key($key)
    {
        $em = $this->getDoctrine()->getManager();
        $codes = $em->getRepository('DoctorsEndpointBundle:Code')->findAll();
        $val = "";
        foreach ($codes as $code) {
            $val = $code->getNumber();
        }
        if ($val == md5($key)) {
            return true;
        } else {
            return false;
        }
    }
}